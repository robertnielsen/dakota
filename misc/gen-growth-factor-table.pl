#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-

use strict;
use warnings;

use POSIX; # ceil

my $nl = "\n";

sub print_tbl {
  my ($growth_factor_n, $growth_factor_d, $init_size) = @_;
  if (! defined $init_size) {
    $init_size = 64;
  }
  my $new_hole = 0;
  my $cur_hole = $new_hole;
  my $cur_size = $init_size;
  my $max_i = 100;
  for (my $i = 1; $i <= $max_i; $i++) {
    my $new_size = &ceil(&ceil($growth_factor_n * $cur_size) / $growth_factor_d);
    $new_hole += $cur_size;
    $cur_size = $new_size;
    my $line = sprintf("%6i / %5i = %.6f: [%3i]",
                      $growth_factor_n, $growth_factor_d, $growth_factor_n/$growth_factor_d, $i);
    #$line .= sprintf(" (old-hole %7i) %7i -> %7i (new-hole = %7i)", $cur_hole, $cur_size, $new_size, $new_hole);
    if ($cur_hole >= $cur_size) {
      printf("# %s\n", $line);
      return;
    } else {
      #printf("  %s\n", $line);
    }
    $cur_hole = $new_hole;
  }
  printf("no solution\n");
  exit(1);
}


sub ratios {
  my ($denominator) = @_;
  #(128 + 64 + 8 + 4 + 2 + 1)/128
  my $i = $denominator;
  my $numerator = $denominator;
  while (1) {
    if (($numerator + ($i/2))/$denominator < 1.618) {
      $numerator += $i/2;
    }
    $i = $i/2;

    if ($i == 1) {
      #printf("%4i / %4i = %.4f\n", $numerator, $denominator, $numerator/$denominator);
      return $numerator;
    }
  }
}

# best growth factor:
# must be less than golden ratio (1.618...)
# must be expressed as fraction of two whole number
# numerator: sum of base-2 numbers
# denominator: base-2 number

# (128 + 64 + 8 + 4 + 2 + 1)/128
# 207/128
# 1.6171875
sub start {
  my ($argv) = @_;
  for (my $i = 3; $i <= 16; $i++) {
    my $d = 2**$i;
    my $numerator = &ratios($d);
    &print_tbl($numerator, $d);
  }
}
unless (caller) {
  &start(\@ARGV);
}
1;
