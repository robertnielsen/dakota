// -*- mode: c++; c-basic-offset: 2; tab-width: 2; indent-tabs-mode: nil -*-

// Copyright (C) 2007 - 2022 Robert Nielsen <robert@dakota.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

# include <fcntl.h>
# include <getopt.h> // getopt_long()
# include <sys/param.h> // MAXPATHLEN
# include <unistd.h>

# include <cassert>
# include <cerrno>
# include <cstdio>
# include <cstring>
# include <sys/wait.h>

# include "dakota-dso.h"
# include "dummy.h"
# include "dakota-other.inc" // NUL, non_exit_fail_with_msg(), exit_failue_with_msg(), setenv_int()

const str_t dev_null = "/dev/null";
enum {
  DAKOTA_CATALOG_HELP = 256,
  DAKOTA_CATALOG_EXPORTED_ONLY,
  DAKOTA_CATALOG_ONLY,
  DAKOTA_CATALOG_OUTPUT,
  DAKOTA_CATALOG_OUTPUT_DIRECTORY,
  DAKOTA_CATALOG_RECURSIVE,
  DAKOTA_CATALOG_JSON,
};
struct opts_t {
  bool  exported_only;
  char* only; // full or partial (prefix) klass name
  char* output; // path or "" for stdout
  char* output_directory; // path or "" for .
  bool  recursive;
  bool  json;
};
static opts_t opts = {};

static FUNC usage(str_t progname, option* options) -> int {
  str_t tmp_progname = strrchr(progname, '/');
  if (tmp_progname != nullptr)
    tmp_progname++;
  else
    tmp_progname = progname;
  fprintf(stdout, "usage: %s", tmp_progname);
  int result = 0;

  while (options->name != nullptr) {
    switch (options->has_arg) {
      case no_argument:
        fprintf(stdout, " [--%s]", options->name);
        break;
      case required_argument:
        fprintf(stdout, " [--%s <>]", options->name);
        break;
      case optional_argument:
        fprintf(stdout, " [--%s [<>]]", options->name);
        break;
      default:
        result = -1;
    }
    options++;
  }
  fprintf(stdout, " <shared-library> [<shared-library> ...]\n");
  return result;
}
static FUNC handle_opts(int* argc, char*** argv) -> void {
  str_t progname = *argv[0];
  int unrecognized_opt_cnt = 0;
  // options descriptor
  static struct option longopts[] = {
    { "help",             no_argument,       nullptr, DAKOTA_CATALOG_HELP },
    { "exported-only",    no_argument,       nullptr, DAKOTA_CATALOG_EXPORTED_ONLY },
    { "only",             required_argument, nullptr, DAKOTA_CATALOG_ONLY },
    { "output",           required_argument, nullptr, DAKOTA_CATALOG_OUTPUT },
    { "output-directory", required_argument, nullptr, DAKOTA_CATALOG_OUTPUT_DIRECTORY },
    { "recursive",        no_argument,       nullptr, DAKOTA_CATALOG_RECURSIVE },
    { "json",             no_argument,       nullptr, DAKOTA_CATALOG_JSON },
    { nullptr, 0, nullptr, 0 }
  };
  int opt;

  while ((opt = getopt_long(*argc, *argv, "", longopts, nullptr)) != -1) {
    switch (opt) {
      case DAKOTA_CATALOG_HELP:
        usage(progname, longopts);
        exit(EXIT_SUCCESS);
      case DAKOTA_CATALOG_EXPORTED_ONLY:
        opts.exported_only = true;
        break;
      case DAKOTA_CATALOG_ONLY:
        opts.only = optarg; // should be an array
        break;
      case DAKOTA_CATALOG_OUTPUT:
        opts.output = optarg;
        assert(opts.output[0] != NUL);
        break;
      case DAKOTA_CATALOG_OUTPUT_DIRECTORY:
        opts.output_directory = optarg;
        break;
      case DAKOTA_CATALOG_RECURSIVE:
        opts.recursive = true;
        break;
      case DAKOTA_CATALOG_JSON:
        opts.json = true;
        break;
      default:
        unrecognized_opt_cnt++;
    }
  }
  if (unrecognized_opt_cnt != 0 || (*argc - optind == 0)) {
    usage(progname, longopts);
    exit(EXIT_FAILURE);
  }
  *argc -= optind;
  *argv += optind;
  return;
}
static FUNC file_exists(str_t path, int flags = O_RDONLY) -> bool {
  bool state;
  int fd = open(path, flags);
  if (fd == -1) {
    state = false;
  } else {
    state = true;
    close(fd);
  }
  return state;
}
static FUNC compare_files(FILE* fp1, FILE* fp2) -> int {
  assert(fp1 != nullptr);
  assert(fp2 != nullptr);
  int result = 0;
  int ch1 = getc(fp1);
  int ch2 = getc(fp2);
  while ((ch1 != EOF) && (ch2 != EOF) && (ch1 == ch2)) {
    ch1 = getc(fp1);
    ch2 = getc(fp2);
  }
  result = DK_CMP(ch1, ch2);
  return result;
}
static FUNC compare_files(const char* fname1, const char* fname2) -> cmp_t {
  assert(fname1 != nullptr);
  assert(fname2 != nullptr);
  FILE* fp1 = fopen(fname1, "r");
  FILE* fp2 = fopen(fname2, "r");
  if (fp1 == nullptr || fp2 == nullptr)
    throw "oops!";
  cmp_t result = compare_files(fp1, fp2);
  fclose(fp1);
  fclose(fp2);
  return result;
}
static FUNC create_empty_file(str_t path) -> int_t {
  assert(path != nullptr);
  assert(path[0] != NUL);
  int fd = open(path, O_CREAT | O_TRUNC, 0644);
  if (fd == -1) exit_fail_with_msg("error: %s: \"%s\"\n", path, strerror(errno));
  int n = close(fd);
  if (n == -1) exit_fail_with_msg("error: %s: \"%s\"\n", path, strerror(errno));
  return 0;
}
FUNC main(int argc, char** argv) -> int {
  handle_opts(&argc, &argv);
  int exit_value = 0;
  char tmp_output_buffer[MAXPATHLEN] = "";
  char* tmp_output = tmp_output_buffer;
  int overwrite;

  if (opts.output != nullptr) {
    if (strcmp(opts.output, dev_null) == 0) {
      tmp_output = opts.output;
    } else {
      snprintf(tmp_output_buffer, sizeof(tmp_output_buffer), "%s-%i", opts.output, getpid());
      create_empty_file(tmp_output);
    }
  }
  if (opts.exported_only)
    setenv_int("DAKOTA_CATALOG_EXPORTED_ONLY", opts.exported_only);

  setenv("DAKOTA_CATALOG_OUTPUT", tmp_output, overwrite = 1); // may be empty-string ("")

  if (opts.output_directory != nullptr)
    setenv("DAKOTA_CATALOG_OUTPUT_DIRECTORY", opts.output_directory, overwrite = 1);
  if (opts.only != nullptr)
    setenv("DAKOTA_CATALOG_ONLY", opts.only, overwrite = 1); // should be regex
  if (opts.recursive)
    setenv_int("DAKOTA_CATALOG_RECURSIVE", opts.recursive);
  if (opts.json)
    setenv_int("DAKOTA_CATALOG_JSON", opts.json);
  if (!opts.json)
    setenv("DKT_NO_INIT_RUNTIME_CORE",  "1", overwrite = 1);
  for (int i = 0; i < argc; i++) {
    str_t arg = argv[i];
    int lib_input_num = i + 1;
    setenv_int("DAKOTA_CATALOG_LIB_INPUT_NUM", lib_input_num, overwrite = 1);
    ptr_t handle = dso_dlopen(arg, RTLD_LAZY | RTLD_GLOBAL);

    // if the shared library is not found in the search path
    // and the argument *could* be relative to the cwd
    if (handle == nullptr && strchr(arg, '/') == nullptr) {
      char rel_arg[MAXPATHLEN] = "";
      strcat(rel_arg, "./");
      strcat(rel_arg, arg);
      if (file_exists(rel_arg))
        handle = dso_dlopen(rel_arg, RTLD_LAZY | RTLD_GLOBAL);
    }
  }
  typealias dk_initialize_runtime_t = FUNC (*)() -> void;
  dk_initialize_runtime_t dk_initialize_runtime = cast(dk_initialize_runtime_t)dso_dlsym(RTLD_DEFAULT, "dk_initialize_runtime");
  if (dk_initialize_runtime)
    dk_initialize_runtime();

  if (opts.output != nullptr) {
    if (strcmp(opts.output, tmp_output) != 0) {
      assert(tmp_output[0] != NUL);
      if (file_exists(opts.output) &&
          compare_files(opts.output, tmp_output) == 0) {
        // preserve timestamp when possible
        int n = unlink(tmp_output);
        if (n == -1) exit_value = non_exit_fail_with_msg("error: %s: \"%s\"\n", tmp_output, strerror(errno));
      } else {
        int n = rename(tmp_output, opts.output);
        if (n == -1) exit_value = non_exit_fail_with_msg("error: %s: \"%s\"\n", opts.output, strerror(errno));
      }
    }
  }
  dk_initialize_runtime_t dk_finalize_runtime = cast(dk_initialize_runtime_t)dso_dlsym(RTLD_DEFAULT, "dk_finalize_runtime");
  if (dk_finalize_runtime)
    dk_finalize_runtime();
  return exit_value;
}
