#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-

# Copyright (C) 2007 - 2022 Robert Nielsen <robert@dakota.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

use strict;
use warnings;

my $nl = "\n";

&gen_tbl($ARGV[0], $ARGV[1], $ARGV[2]);

sub gen_tbl {
  my ($header, $prefix, $ns_name) = @_;
  my $bname = $header =~ s/\.h$//r;
  my $err_set = {};
  my $r_err_tbl = {};
  my $exclude = { 'SIGRTMIN' => 1,
                  'SIGRTMAX' => 1 };
  my $paths = [];
  my $include_dirs = [ '/usr/include',
                       '/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include' ];
  foreach my $include_dir (@$include_dirs) {
    if (-d $include_dir) {
      $paths = [split /\s/, `find $include_dir -name "*$bname*.h" -print`];
    }
  }

  foreach my $path (@$paths) {
    open(my $in, "<", $path) or die "cannot open < $path: $!";

    while (<$in>) {
      if (m/\#\s*define\s+($prefix[A-Z0-9]+)\s+($prefix[A-Z0-9]+|\d+)/) {
        my $err = $1;
        my $val = $2;
        next if $$exclude{$err};
        $$err_set{$err} = undef;
        if ($val =~ /^\d+$/) {
          if (!exists $$r_err_tbl{$val}) {
            $$r_err_tbl{$val} = {};
          }
          $$r_err_tbl{$val}{$err} = undef;
        }
      }
    }
    close($in);
  }
  my $keys = [sort keys %$err_set];
  print "// -*- mode: c++; c-basic-offset: 2; tab-width: 2; indent-tabs-mode: nil -*-\n";
  print "\n";
  print "# include <$header>\n";
  print "\n";
  print "namespace $ns_name {\n";
  print "  static str_t names\[] = {\n";
  for (my $i = 0; $i < scalar keys %$r_err_tbl; $i++) {
    my $errs = join "|", sort keys %{$$r_err_tbl{$i} || {}};
    print "    \"$i\", // $errs\n";
  }
  print "    nullptr\n";
  print "  };\n";
  print "  static FUNC set_names(str_t* _names, ssize_t _num_names) -> void {\n";
  for (my $i = 0; $i < scalar keys %$r_err_tbl; $i++) {
    my $keys = [sort keys %{$$r_err_tbl{$i} || {}}];
    foreach my $key (@$keys) {
      print "    # if defined $key\n";
      print "      set_name(_names, _num_names, $key, \"$key\");\n";
      print "    # endif\n";
    }
  }
  print "    return;\n";
  print "  }\n";
  print "  static __ddl_t ddl = __ddl_t{[](){set_names(names, scountof(names));}, [](){}};\n";
  print "}\n";
}
