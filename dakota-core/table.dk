// -*- mode: c++; mode: dakota; c-basic-offset: 2; tab-width: 2; indent-tabs-mode: nil -*-

// Copyright (C) 2007 - 2022 Robert Nielsen <robert@dakota.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

share dakota-core;

trait table {
  require set;
  trait   set-of-assocs;

  func initialize(object-t) -> void {
    assoc::klass();
    return;
  }
  method add(object-t self, object-t key, object-t item) -> object-t {
    object-t assoc = $at(super, key, nullptr);
    object-t current-item = assoc;

    if (assoc) {
      if (klass-of(assoc) == assoc::_klass_ || $instance-of?(assoc, assoc::_klass_))
        current-item = assoc::unbox(assoc).item;
      if (!$equals?(current-item, item))
        assoc::mutable-unbox(assoc).item = item;
    } else {
      current-item = item;
      assoc = assoc::box({key, current-item});
      $add(super, assoc);
    }
    return current-item;
  }
}
