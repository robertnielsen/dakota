// -*- mode: c++; mode: dakota; c-basic-offset: 2; tab-width: 2; indent-tabs-mode: nil -*-

// Copyright (C) 2007 - 2022 Robert Nielsen <robert@dakota.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

# include "safe-str.h"
# include "private.h"

share dakota-core;

klass str {
  superklass value;
  klass      value-klass;

  slots const char-t*;

  // causes make-format(object-t, str-t, ...) to be generated
  // method va::init-format(object-t self, str-t format, va-list-t args) -> object-t {
  //   self = $init(super);
  //   //
  //   return self;
  // }
  method length(object-t self) -> ssize-t {
    ssize-t result = cast(auto)safe-strlen(unbox(self));
    return result;
  }
  method intern(const slots-t s) -> symbol-t {
    symbol-t result = dk-intern-str(s);
    return result;
  }
  method dump(object-t self) -> object-t {
    $dump(super);
    fprintf(stderr, "%p { slots=\"%s\" }\n",
            cast(ptr-t)self, unbox(self));
    return self;
  }
  method hash(object-t self) -> hash-t {
    const slots-t s = unbox(self);
    assert(s != nullptr);
    hash-t result = dk-hash-str(s);
    return result;
  }
  method compare(const slots-t s, const slots-t other-s) -> cmp-t {
    assert(s != nullptr);
    assert(other-s != nullptr);
    cmp-t result = safe-strcmp(s, other-s);
    return result;
  }
}
