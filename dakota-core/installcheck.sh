#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
source_dir=$(cd $samedir/.. && pwd)
source "$source_dir/vars.sh"

source $source_dir/common.sh
source $source_dir/config.sh

# compile <>.o
rm -f installcheck-o.o installcheck-util-o.o
cat /dev/null > installcheck-o.dk
cat /dev/null > installcheck-util-o.dk
set -o verbose
$INSTALL_PREFIX/bin/dakota --compile installcheck-o.dk
$INSTALL_PREFIX/bin/dakota --compile installcheck-util-o.dk $INSTALL_PREFIX/lib/libdakota.$lib_suffix
set +o verbose
rm -f installcheck-o.dk installcheck-util-o.dk

# compile/link <>.so
rm -f installcheck-so.so installcheck-util-so.so
cat /dev/null > installcheck-so.dk
cat /dev/null > installcheck-util-so.dk
rm -f installcheck-so.so installcheck-util-so.so
set -o verbose
$INSTALL_PREFIX/bin/dakota --shared installcheck-so.dk
$INSTALL_PREFIX/bin/dakota --shared installcheck-util-so.dk $INSTALL_PREFIX/lib/libdakota.$lib_suffix
dakota-catalog installcheck-so.so
dakota-catalog installcheck-util-so.so
set +o verbose
rm -f installcheck-so.dk installcheck-util-so.dk

# compile/link <> executable
rm -f installcheck installcheck-util
src="func main(int-t, const str-t[]) -> int-t { EXIT(0); }" 
echo $src > installcheck.dk
echo $src > installcheck-util.dk
rm -f installcheck installcheck-util
set -o verbose
$INSTALL_PREFIX/bin/dakota installcheck.dk
$INSTALL_PREFIX/bin/dakota installcheck-util.dk $INSTALL_PREFIX/lib/libdakota.$lib_suffix
./installcheck
./installcheck-util
set +o verbose
rm -f installcheck.dk installcheck-util.dk
