#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
source_dir=$samedir
source "$source_dir/vars.sh"
source "$source_dir/stage-pre-built.sh"

function project-build() {
  # install everthing first and then use this partial installation to build
  # the remaining two exes and three libs
  stage-pre-built $stage_prefix_dir
  local secs=$SECONDS
  echo "# building ..." 1>&2
  ee $dakota_home/bin/dakota-project --build $build_dir "$@"
  secs=$(( $SECONDS - $secs ))
  echo "# building [duration=$(($secs / 60))m$(($secs % 60))s]" 1>&2
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  project-build "$@"
fi
