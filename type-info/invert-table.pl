#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-
# -*- tab-width: 2
# -*- indent-tabs-mode: nil

use strict;
use warnings;
use sort 'stable';

use Data::Dumper;
$Data::Dumper::Terse     = 1;
$Data::Dumper::Deepcopy  = 1;
$Data::Dumper::Purity    = 1;
$Data::Dumper::Quotekeys = 1;
$Data::Dumper::Indent    = 1; # default = 2

my $nl = "\n";

undef $/;

my $pri = {
	   'signed char' =>        'hhi',
	   'short' =>              'hi',
	   'int' =>                'i',
	   'long' =>               'li',
	   'long long' =>          'lli',

	   'unsigned char' =>      'hhu',
	   'unsigned short' =>     'hu',
	   'unsigned int' =>       'u',
	   'unsigned long' =>      'lu',
	   'unsigned long long' => 'llu',
	  };
# int literal suffix: i8,i16,i32,i64,u8,u16,u32,u64

my $instr = <STDIN>;
my $in = eval($instr) or die;

my $result = { 'types' => {}, 'sizes' => {}};

while (my ($type, $type_info) = each (%$in)) {
  # types
  if (! $$result{'types'}{$$type_info{'typeid-name'}}) {
    $$result{'types'}{$$type_info{'typeid-name'}} = {};
  }
  $$result{'types'}{$$type_info{'typeid-name'}}{$type} = 1;

  # sizes
  if (! $$result{'sizes'}{$$type_info{'size'}}) {
    $$result{'sizes'}{$$type_info{'size'}} = {};
  }
  $$result{'sizes'}{$$type_info{'size'}}{$type} = 1;
}
print &Dumper($result);
