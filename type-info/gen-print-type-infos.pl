#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-
# -*- tab-width: 2
# -*- indent-tabs-mode: nil

use strict;
use warnings;
use sort 'stable';

use Data::Dumper;
$Data::Dumper::Terse     = 1;
$Data::Dumper::Deepcopy  = 1;
$Data::Dumper::Purity    = 1;
$Data::Dumper::Quotekeys = 1;
$Data::Dumper::Indent    = 1; # default = 2

my $nl = "\n";

my $header_lines = '';
my $types = [];

while (<>) {
  if ($_ =~ /(#\s+include\s+<.+?>)/) {
    $header_lines .= $1 . $nl;
  }
  if ($_ =~ /(\w+?_t)/) {
    push @$types, $1;
  }
}
my $type_lines = '';
$type_lines .= 'fprintf(out, "{\n");' . $nl;
$type_lines .= $nl;
foreach my $type (@$types) {
  $type_lines .= "fprintf(out, INFO($type));" . $nl;
}
$type_lines .= $nl;
$type_lines .= 'fprintf(out, "}\n");' . $nl;
#print $header_lines;
print $type_lines;
