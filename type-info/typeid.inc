// -*- mode: c++ -*-

# include <csignal>
# include <cstddef>
# include <cstdint>
# include <cstring>
# include <cstdio>
# include <cwchar>
# include <sys/types.h>

# include <cxxabi.h>

# include <typeinfo>

# define FUNC auto
# define STR(s) #s
// # define cast(...) (__VA_ARGS__)

# define cast(t) (t)
# define INFO(type) \
  "  \"%s\"=>%-*s{ \"size\"=> %zu, \"typeid-name\"=> \"%s\" },\n", \
    STR(type), pad_len(16, STR(type)), "", sizeof(type), demangle(typeid(type).name())

using int_t =  int;
using str_t =  const char*;
using ptr_t =  void*;
using char_t = char;

inline FUNC demangle(str_t mangled_name, char_t* buffer = nullptr, size_t buffer_len = 0) -> str_t {
  int_t status = 0;
  str_t result = abi::__cxa_demangle(mangled_name, buffer, &buffer_len, &status); // must be free()ed if buffer is non-nullptr
  if (status != 0)
    result = mangled_name; // silent failure
  return result;
}
FUNC pad_len(int_t max, const char_t* type) -> int_t;
FUNC pad_len(int_t max, const char_t* type) -> int_t {
  int_t result = max - cast(int_t)strlen(type);
  int_t min = 1;
  if (result < min)
    result = min;
  return result;
}
