// -*- mode: c++ -*-

# include "typeid.inc"

auto main() -> int {
  FILE* out = stdout;

# include "print-lines.inc"

  return 0;
}
// clang++    -std=c++1z --warn-everything --warn-no-c++98-compat --warn-no-c++98-compat-pedantic --warn-no-old-style-cast --output typeid typeid.cc && ./typeid
// clang++5.0 -std=c++1z --warn-everything --warn-no-c++98-compat --warn-no-c++98-compat-pedantic --warn-no-old-style-cast --output typeid typeid.cc && ./typeid
