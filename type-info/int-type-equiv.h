// -*- mode: c++ -*-

# define  DKT_SIGNED_CHAR          1
# define  DKT_SHORT                2
# define  DKT_INT                  3
# define  DKT_LONG                 4
# define  DKT_LONG_LONG            5

# define  DKT_UNSIGNED_CHAR        6
# define  DKT_UNSIGNED_SHORT       7
# define  DKT_UNSIGNED_INT         8
# define  DKT_UNSIGNED_LONG        9
# define  DKT_UNSIGNED_LONG_LONG  10

# define  DK_INT8_T_EQUIV  DKT_SIGNED_CHAR





# if INT64_T_EQUIV != INTPTR_T_EQUIV
# endif
