#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-

use strict;
use warnings;
use sort 'stable';
use Cwd;
use File::Find;
use File::Spec;

my $gbl_prefix;
my $gbl_recipes;
my $gbl_cmd_info;
my $all_rules = [];
my $root_name;
my $nl = "\n";

sub dk_prefix {
  my ($path) = @_;
  $path =~ s|//+|/|;
  $path =~ s|/\./+|/|;
  $path =~ s|^./||;
  if (-d "$path/bin" && -d "$path/lib") {
    return $path
  } elsif ($path =~ s|^(.+?)/+[^/]+$|$1|) {
    &dk_prefix($path);
  } else {
    die "Could not determine \$prefix from executable path $0: $!" . $nl;
  }
}

BEGIN {
  $gbl_prefix = &dk_prefix($0);
  unshift @INC, "$gbl_prefix/lib";
};
use Carp; $SIG{ __DIE__ } = sub { Carp::confess( @_ ) };

use dakota::dakota;
use dakota::parse;
use dakota::util;

use Data::Dumper;
$Data::Dumper::Terse =     1;
$Data::Dumper::Deepcopy =  1;
$Data::Dumper::Purity =    1;
$Data::Dumper::Useqq =     1;
$Data::Dumper::Sortkeys =  1;
$Data::Dumper::Indent =    1;   # default = 2

use Getopt::Long qw(GetOptionsFromArray);
$Getopt::Long::ignorecase = 0;

my $jobs = &builder_jobs();

my $builder;
sub init_builder {
  my $builder_tbl = {
  'make' => {
    'opts' =>                [
      '-j', $jobs, # not using --jobs to emphasize its equivalency to ninja's -j
      '--no-builtin-rules',
      '--no-builtin-variables',
      '--warn-undefined-variables',
    ],
    'io-vars' =>             [ '$^', '$@' ],
    'path-from-cmk-path' =>  \&mk_path_from_cmk_path,
    'gen-builder' =>         \&gen_make,
    'gen-rules-builder' =>   \&gen_rules_make,
    'gen-body-builder'  =>   \&gen_body_make,
    'include-keyword' =>     'include',
    'ext' =>                 'mk',
    'keep-going' =>          '-k',
    'header-lines' => [
    ],
  },
  'ninja' => {
    'opts' =>                [
      '-j', $jobs, # ninja uses num-threads on darwin rather than num-core when it calculates default jobs
    ],
    'io-vars' =>             [ '${in}', '${out}' ],
    'path-from-cmk-path' =>  \&ninja_path_from_cmk_path,
    'gen-builder' =>         \&gen_ninja,
    'gen-rules-builder' =>   \&gen_rules_ninja,
    'gen-body-builder'  =>   \&gen_body_ninja,
    'include-keyword' =>     'subninja',
    'ext' =>                 'ninja',
    'keep-going' =>          '-k 0',
    'header-lines' => [
      "ninja_required_version = 1.8",
      "builddir =               " . &intmd_dir(),
    ],
  }
  };
  if (! $ENV{'builder'}) { $ENV{'builder'} = 'ninja'; }
  return $$builder_tbl{$ENV{'builder'}};
}
sub get_builder {
  my $intmd_dir = &intmd_dir();
  if (0) {
  } elsif (-f "$intmd_dir/build.ninja") {
    $ENV{'builder'} = 'ninja';
  } elsif (-f "$intmd_dir/build.mk") {
    $ENV{'builder'} = 'make';
  } else {
    die "Cannot file build file in $intmd_dir.";
  }
  return &init_builder();
}

sub get_opts_from_array {
  my $argv = shift @_;
  my $rest = \@_;

  my $root_cmd = {
    'opts' => {
    'var' => [],
    'clean-exclude' => [],
    }
  };
  &GetOptionsFromArray($argv, $$root_cmd{'opts'}, @$rest);
  $$root_cmd{'vars'} = {};
  foreach my $kv (@{$$root_cmd{'opts'}{'var'}}) {
    $kv =~ /^([\w-]+)=(.*)$/;
    my ($key, $val) = ($1, $2);
    $$root_cmd{'vars'}{$key} = $val;
  }

  my %tmp = map { $_ => 1 } @{$$root_cmd{'opts'}{'clean-exclude'}};
  $$root_cmd{'clean-excludes'} = \%tmp;
  return $root_cmd;
}
sub cmd_info_from_argv {
  my ($argv) = @_;
  my $cmd_info = &get_opts_from_array($argv,
                                      'path-only',
                                      'source-dir:s',
                                      'build-dir:s',
                                      'build:s',
                                      'clean=s',
                                      'clean-exclude=s',
                                      'keep-going',
                                      'var=s',
                                    );
  $$cmd_info{'inputs'} = [ @ARGV ];
  return $cmd_info;
}
sub gen_dot {
  my ($rules, $output_path) = @_;
  if (0) { return; }
  my $result = '';
  $result .= "digraph {" . $nl;
  $result .= "  graph [ rankdir = LR, nodesep = 0 ];" . $nl;
  $result .= "  node  [ shape = rect, style = rounded, height = 0, width = 0 ];" . $nl;
  $result .= $nl;
  $result .= &gen_dot_body($rules);
  $result .= "}" . $nl;
  if (1) { # hack to make the graph less noisy
    my $dir = $ENV{'HOME'};
    $result =~ s#"$dir/#"#g;
  }
  &_filestr_to_file($result, $output_path, '>');
}
sub gen_dot_body {
  my ($rules) = @_;
  my $result = '';
  $result .= &gen_dot_body_nodes($rules);
  $result .= $nl;
  $result .= &gen_dot_body_edges($rules);
  return $result;
}
sub gen_dot_body_nodes {
  my ($rules) = @_;
  my $result = '';
  my $nodes = {};
  my $path = $$rules[0][0][0];
  my $name = &basename($path);
  $result .= "  \"$path\" [ label = \"$name\", color = green ];" . $nl;
  $$nodes{$path} = 1;
  for (my $i = 0; $i < scalar @$rules; $i++) {
    foreach my $j (0, 2, 3) { # skip 1 because its the recipe name
      foreach $path (@{$$rules[$i][$j]}) {
        my $recipe = $$rules[$i][1];
        if (! $$nodes{$path}) {
          $$nodes{$path} = 1;
          $name = &basename($path);
          if (0) {
          } elsif (&is_so_path($path) ||
                   &is_exe_path($path)) {
            $result .= "  \"$path\" [ label = \"$name\", color = green ];" . $nl;
          } else {
            $result .= "  \"$path\" [ label = \"$name\", color = black ];" . $nl;
          }
        }
      }
    }
  }
  return $result;
}
sub gen_dot_body_edges {
  my ($rules) = @_;
  my $result = '';
  foreach my $rule (@$rules) {
    my $tgts =               $$rule[0];
    my $recipe =             $$rule[1];
    my $prereqs =            $$rule[2];
    my $order_only_prereqs = $$rule[3];
    foreach my $tgt (@$tgts) {
      foreach my $prereq (@$prereqs) {
        $result .= "  \"$tgt\" -> \"$prereq\"";
        if (0) {
        } elsif (&is_o_path($tgt)) {
          $result .= ' [ color = blue ]';
        } elsif ((&is_exe_path($tgt) || &is_so_path($tgt)) && (&is_exe_path($prereq) || &is_so_path($prereq))) {
          $result .= ' [ color = black ]';
        } elsif (&is_exe_path($tgt) || &is_so_path($tgt)) {
          $result .= ' [ color = green ]';
        } elsif ($tgt =~ m=/z/target\.(h|cc)$=) {
          $result .= ' [ color = magenta ]';
        } elsif (&is_ast_path($tgt) && &is_ast_path($prereq)) {
          $result .= ' [ color = gold ]';
        } elsif (&is_ast_path($tgt)) {
          $result .= ' [ color = violetred ]';
        } elsif (&is_ctlg_path($tgt)) {
          $result .= ' [ color = red ]';
        }
        $result .= ';' . $nl;
      }
      foreach my $prereq (@$order_only_prereqs) {
        $result .= "  \"$tgt\" -> \"$prereq\" [ color = gray, style = dashed ]";
        ###
        $result .= ';' . $nl;
      }
    }
  }
  return $result;
}
sub gen_rules_ninja {
  my ($recipes, $target, $root_tgt) = @_;
  my $result = '';

  foreach my $rule (sort keys %$recipes) {
    my $cmd = $$recipes{$rule};
    if (! $cmd) { die; }
    $result .=
      $nl .
      "rule $rule" . $nl .
      '  ' . "command = $$cmd[0]" . $nl;
  }
  return $result;
}
sub gen_rules_make {
  my ($recipes, $target, $root_tgt) = @_;
  my $result = '';
  my $dakota =         '${dakota_bin_dir}/dakota${exe_suffix}';
  my $dakota_catalog = '${dakota_bin_dir}/dakota-catalog${exe_suffix}';

  $result .=
    '# catalog'                                 . $nl .
    '%.ctlg :'                                  . $nl .
    "\t" . "\@mkdir -p \$(dir \$@)"             . $nl .
    "\t" . "\@$dakota_catalog --output \$@ \$<" . $nl .
    $nl .
    '# parse'                                          . $nl .
    '%.ctlg.ast :'                                     . $nl .
    "\t" . "\@mkdir -p \$(dir \$@)"                    . $nl .
    "\t" . "\@$dakota --action parse --output \$@ \$<" . $nl .
    $nl .
    '# parse'                                          . $nl .
    '%.dk.ast :'                                       . $nl .
    "\t" . "\@mkdir -p \$(dir \$@)"                    . $nl .
    "\t" . "\@$dakota --action parse --output \$@ \$<" . $nl .
    $nl .
    ".PHONY : all" . $nl .
    $nl .
    "all : $root_tgt" . $nl;
  return $result;
}
sub gen_header {
  my ($rules, $so_paths, $target) = @_;
  my $dakota_home = &dakota_home();
  my $source_dir = &source_dir();
  my $intmd_dir =  &intmd_dir();
  my $build_dir =  &build_dir();
  my $platform =   &var('platform');
  my $source_dir_bname = &basename($source_dir);
  my $stage_prefix_dir = $ENV{'stage_prefix_dir'};
  $stage_prefix_dir = &default_stage_prefix_dir_from_source_dir($source_dir) if ! $stage_prefix_dir;
  my $result = '';
  $result .=
    "dakota_home =        $dakota_home"            . $nl .
    "dakota_bin_dir =     \${dakota_home}/bin"     . $nl .
    "dakota_lib_dir =     \${dakota_home}/lib"     . $nl .
    "dakota_include_dir = \${dakota_home}/include" . $nl .
    $nl .
    "stage_prefix_dir =  $stage_prefix_dir"            . $nl .
    "stage_bin_dir =     \${stage_prefix_dir}/bin"     . $nl .
    "stage_lib_dir =     \${stage_prefix_dir}/lib"     . $nl .
    "stage_include_dir = \${stage_prefix_dir}/include" . $nl .
    $nl .
    "source_dir = $source_dir" . $nl .
    "intmd_dir =  $intmd_dir"  . $nl .
    "build_dir =  $build_dir"  . $nl .
    $nl .
    "lib_prefix =" . &pad_var('lib_prefix') . $nl .
    "lib_suffix =" . &pad_var('lib_suffix') . $nl .
    "exe_suffix =" . &pad_var('exe_suffix') . $nl;

  if ($ENV{'debug'}) {
    $result .=
      $nl .
      "cc_compiler_shared_library_extra_opts = \@\${dakota_lib_dir}/dakota/opts/compiler-shared-library-extra-${platform}.opts" . $nl .
      "cc_compiler_executable_extra_opts =     \@\${dakota_lib_dir}/dakota/opts/compiler-executable-extra-${platform}.opts"     . $nl .
      "cc_linker_shared_library_extra_opts =   \@\${dakota_lib_dir}/dakota/opts/linker-shared-library-extra-${platform}.opts"   . $nl .
      "cc_linker_executable_extra_opts =       \@\${dakota_lib_dir}/dakota/opts/linker-executable-extra-${platform}.opts"       . $nl .
      $nl .
      "compiler_shared_library_extra_opts =    \@\${dakota_lib_dir}/dakota/opts/compiler-shared-library-extra-${platform}.opts" . $nl .
      "compiler_executable_extra_opts =        \@\${dakota_lib_dir}/dakota/opts/compiler-executable-extra-${platform}.opts"     . $nl .
      "linker_shared_library_extra_opts =      \@\${dakota_lib_dir}/dakota/opts/linker-shared-library-extra-${platform}.opts"   . $nl .
      "linker_executable_extra_opts =          \@\${dakota_lib_dir}/dakota/opts/linker-executable-extra-${platform}.opts"       . $nl;
    } else {
    $result .=
      $nl .
      "cc_compiler_shared_library_extra_opts =" . $nl .
      "cc_compiler_executable_extra_opts ="     . $nl .
      "cc_linker_shared_library_extra_opts ="   . $nl .
      "cc_linker_executable_extra_opts ="       . $nl .
      $nl .
      "compiler_shared_library_extra_opts ="    . $nl .
      "compiler_executable_extra_opts ="        . $nl .
      "linker_shared_library_extra_opts ="      . $nl .
      "linker_executable_extra_opts ="          . $nl;
  }
  return $result;
}
sub gen_ninja {
  my ($rules, $so_paths, $target) = @_;
  my $cxx =                &var('cxx');
  my $source_dir =         &source_dir();
  my $intmd_dir =          &intmd_dir();
  my $build_dir =          &build_dir();
  my $current_source_dir = &current_source_dir();
  my $current_intmd_dir =  &current_intmd_dir();
  my $current_build_dir =  &current_build_dir();
  my $result = '';
  my $root_tgt = $$rules[0][0][0];
  my $target_hdr_path = &target_hdr_path();
  my $dirs = {};
  if (&is_so_path($root_tgt)) {
    my $dir = &dirname($root_tgt);
    $$dirs{$dir} = 1;
  }
  foreach my $so_path (@$so_paths) {
    my $dir = &dirname($so_path);
    $$dirs{$dir} = 1;
  }
  my $root_tgt_dir = &dirname($root_tgt);
  my $gen_rules_sub = $$builder{'gen-rules-builder'};
  my $gen_body_sub =  $$builder{'gen-body-builder'};
  $result .=
    &gen_header($rules, $so_paths, $target);

  if ($$gbl_cmd_info{'vars'} && scalar keys %{$$gbl_cmd_info{'vars'}}) {
    $result .= $nl;
    while (my ($k, $v) = each (%{$$gbl_cmd_info{'vars'}})) {
      $result .=
        "$k = $v" . $nl;
    }
  }
  $result .=
    $nl .
    "current_source_dir = $current_source_dir" . $nl .
    "current_intmd_dir =  $current_intmd_dir"  . $nl .
    "current_build_dir =  $current_build_dir"  . $nl .
    $nl .
    &$gen_rules_sub($gbl_recipes, $target, $root_tgt) .
    &$gen_body_sub($rules, $gbl_recipes) .
    $nl .
    "default $root_tgt" . $nl;
  return $result;
}
sub gen_make {
  my ($rules, $so_paths, $target) = @_;
  my $cxx =                &var('cxx');
  my $source_dir =         &source_dir();
  my $intmd_dir =          &intmd_dir();
  my $build_dir =          &build_dir();
  my $current_source_dir = &current_source_dir();
  my $current_intmd_dir =  &current_intmd_dir();
  my $current_build_dir =  &current_build_dir();
  my $result = '';
  my $root_tgt = $$rules[0][0][0];
  my $target_hdr_path = &target_hdr_path();
  my $dirs = {};
  if (&is_so_path($root_tgt)) {
    my $dir = &dirname($root_tgt);
    $$dirs{$dir} = 1;
  }
  foreach my $so_path (@$so_paths) {
    my $dir = &dirname($so_path);
    $$dirs{$dir} = 1;
  }
  my $current_target_x_intmd_dir = &current_target_x_intmd_dir();
  my $current_target_x_build_dir = &current_target_x_build_dir();
  my $root_tgt_dir = &dirname($root_tgt);
  my $gen_rules_sub = $$builder{'gen-rules-builder'};
  my $gen_body_sub =  $$builder{'gen-body-builder'};
  $result .=
    &gen_header($rules, $so_paths, $target);

  if ($$gbl_cmd_info{'vars'} && scalar keys %{$$gbl_cmd_info{'vars'}}) {
    $result .= $nl;
    while (my ($k, $v) = each (%{$$gbl_cmd_info{'vars'}})) {
      $result .=
        "$root_tgt : $k := $v" . $nl;
    }
  }
  $result .=
    $nl .
    "$root_tgt : current_source_dir := $current_source_dir" . $nl .
    "$root_tgt : current_intmd_dir :=  $current_intmd_dir"  . $nl .
    "$root_tgt : current_build_dir :=  $current_build_dir"  . $nl .
    $nl .
    "\$(shell mkdir -p $root_tgt_dir)"              . $nl .
    "\$(shell mkdir -p $current_target_x_intmd_dir) # current_target_x_intmd_dir"  . $nl .
    "\$(shell mkdir -p $current_target_x_build_dir) # current_target_x_build_dir"  . $nl .
    $nl .
    &$gen_rules_sub($gbl_recipes, $target, $root_tgt) .
    &$gen_body_sub($rules, $gbl_recipes);
  return $result;
}
sub gen_body_ninja {
  my ($rules, $all_recipes_tbl) = @_;
  my $result = '';
  my $exclude = { 'catalog' => 1, 'parse' => 1 };
  foreach my $rule (@$rules) {
    my $tgts =               $$rule[0];
    my $recipe =             $$rule[1];
    my $prereqs =            $$rule[2];
    my $order_only_prereqs = $$rule[3];
    my $cmd = $recipe;
    if (! $cmd) { die; }
    my $d = $nl;
    $result .= $nl . 'build';
    $d = " \$" . $nl;
    foreach my $tgt (@$tgts) {
      $result .= $d . '  ' . $tgt;
    }
    $result .= $d . '  ' . ': ' . $cmd;
    if (scalar @$prereqs) {
      foreach my $prereq (@$prereqs) {
        $result .= $d . '  ' . $prereq;
      }
    }
    if (scalar @$order_only_prereqs) {
      $result .= $d . '  ' . '||';
      foreach my $prereq (@$order_only_prereqs) {
        $result .= $d . '  ' . $prereq;
      }
    }
    $result .= $nl;
  }
  return $result;
}
sub gen_body_make {
  my ($rules, $all_recipes_tbl) = @_;
  my $result = '';
  my $exclude = { 'catalog' => 1, 'parse' => 1 };
  foreach my $rule (@$rules) {
    my $tgts =               $$rule[0];
    my $recipe =             $$rule[1];
    my $prereqs =            $$rule[2];
    my $order_only_prereqs = $$rule[3];
    my $d = $nl;
    foreach my $tgt (@$tgts) {
      $result .= $d . $tgt;
      $d = " \\" . $nl;
    }
    $result .= ' :';
    foreach my $prereq (@$prereqs) {
      $result .= $d . $prereq;
    }
    if (scalar @$order_only_prereqs) {
      $result .= $d . '|';
      foreach my $prereq (@$order_only_prereqs) {
        $result .= $d . $prereq;
      }
    }
    $result .= $nl;
    if (length $recipe && $$all_recipes_tbl{$recipe}) {
      if (! $$exclude{$recipe} ) {
        for my $line (@{$$all_recipes_tbl{$recipe}}) {
          $result .= "\t" . $line . $nl;
        }
      }
    }
  }
  return $result;
}
sub fully_qualify_path {
  my ($path) = @_;
  if (! is_abs($path) && $path !~ /^\$/) {
    $path = &current_source_dir() . '/' . $path;
  }
  $path =~ s#/.$##;
  $path =~ s#/[^/]+/\.\./#/#;
  return $path;
}
sub rpaths {
  my ($so_paths) = @_;
  my $result = '';
  my $dirs = {};
  foreach my $so_path (@$so_paths) {
    $so_path = &fully_qualify_path($so_path);
    my $dir = &dirname($so_path);
    if (! $$dirs{$dir}) {
      $$dirs{$dir} = 1;
      $result .= " -Wl,-rpath,$dir";
    }
  }
  return $result;
}
sub include_dirs {
  my ($build_cmk) = @_;
  my $result = '';
  my $dirs = {};
  foreach my $dir (@{$$build_cmk{'include-dirs'}}) {
    $dir = &fully_qualify_path($dir);
    if (! $$dirs{$dir}) {
      $$dirs{$dir} = 1;
      $result .= " -I$dir";
    }
  }
  return $result;
}
sub macros {
  my ($build_cmk) = @_;
  my $result = '';
  my $macros = {};
  foreach my $macro (@{$$build_cmk{'macros'}}) {
    if (! $$macros{$macro}) {
      $$macros{$macro} = 1;
      $result .= " -D$macro";
    }
  }
  return $result;
}
sub soname {
  my ($build_cmk, $root_tgt_name) = @_;
  my $host_system_name = &var('host_system_name');
  die "env var host_system_name not set" if ! $host_system_name;
  my $result;
  if (0) {
  } elsif ($host_system_name eq 'darwin') {
    $result = "-Wl,-install_name,\@rpath/$root_tgt_name";
  } elsif ($host_system_name eq 'linux') {
    $result = "-Wl,-soname,$root_tgt_name";
  } else {
    die "error: dakota-project does not support $host_system_name at this time."
  }
  return $result;
}
sub is_empty {
  my ($seq) = @_;
  if (@$seq == 0) {
    return 1;
  } else {
    return 0;
  }
}
sub gen_rules {
  my ($root_tgt, $parts, $build_cmk, $io_vars) = @_;
  my $in_var =  $$io_vars[0];
  my $out_var = $$io_vars[1];
  my $order_only_targets = $$build_cmk{'order-only-targets'};
  $order_only_targets = [] if ! $order_only_targets;
  my $dk_paths = [];
  my $so_paths = [];
  my $cc_paths = [];
  my $cc_o_paths = [];
  my $so_ctlg_ast_paths = [];
  my $dk_o_paths = [];
  my $dk_ast_paths = [];
  foreach my $path (@$parts) {
    if (&is_dk_path($path)) {
      &add_last($dk_paths, $path);
      my $dk_o_path = &o_path_from_dk_path($path);
      &add_last($dk_o_paths, $dk_o_path);
      my $dk_ast_path = &ast_path_from_dk_path($path);
      &add_last($dk_ast_paths, $dk_ast_path);
    } elsif (&is_cc_path($path)) {
      &add_last($cc_paths, $path);
      my $cc_o_path = &o_path_from_cc_path($path);
      &add_last($cc_o_paths, $cc_o_path);
    } else {
      &add_last($so_paths, $path);
      my $so_ctlg_path =     &ctlg_path_from_so_path($path);
      my $so_ctlg_ast_path = &ast_path_from_ctlg_path($so_ctlg_path);
      &add_last($so_ctlg_ast_paths, $so_ctlg_ast_path);
    }
  }
  my $root_tgt_name = &basename($root_tgt);
  my $root_tgt_type;
  if (&is_so_path($root_tgt)) {
    $root_tgt_type = 'shared-library';
  } else {
    $root_tgt_type = 'executable';
  }
  my $target_o_path =          &target_o_path();
  my $target_src_path =        &target_src_path();
  my $target_hdr_path =        &target_hdr_path();
  my $target_inputs_ast_path = &target_inputs_ast_path();
  my $target_srcs_ast_path =   &target_srcs_ast_path();
  my $target_libs_ast_path =   &target_libs_ast_path();
  my $cxx =                    &var('cxx');
  my $dakota_bin_dir =         &dakota_home() . '/bin';
  my $exe_suffix =             &var('exe_suffix');
  my $dakota =                 "${dakota_bin_dir}/dakota${exe_suffix}";
  my $dakota_catalog =         "${dakota_bin_dir}/dakota-catalog${exe_suffix}";
  my $current_source_dir =     &current_source_dir();
  my $source_dir =             &source_dir();
  my $build_dir =              &build_dir();
  my $target = &var('target');
  my $all_dir_vars = '';
  if (! &is_empty($dk_o_paths)) {
    $all_dir_vars = "--var current_source_dir=${current_source_dir} --var source_dir=${source_dir} --var build_dir=${build_dir} --var target=${target}";
  }
  my $rpaths = &rpaths($so_paths);
  my $soname = &soname($build_cmk, $root_tgt_name);
  $$build_cmk{'include-dirs'} = [] if ! $$build_cmk{'include-dirs'};
  &add_first($$build_cmk{'include-dirs'}, '${dakota_home}' . "/include");
  my $include_dirs = &include_dirs($build_cmk);
  my $macros =       &macros($build_cmk);
  # darwin: -rpath <> AND -install_name <>
  # linux:  -rpath <> AND -soname <>

  my $platform = &var('platform');

  $gbl_recipes = {
    'catalog' =>                [ "${dakota_catalog} --output $out_var $in_var" ],
    'parse' =>                  [ "${dakota} --output $out_var --action parse $in_var" ],

    'merge' =>                  [ "${dakota} --output $out_var --action merge $all_dir_vars $in_var" ], # could use $?
    'gen-target-hdr' =>         [ "${dakota} --output $out_var --action gen-target-hdr $all_dir_vars $in_var" ],
    'gen-target-src' =>         [ "${dakota} --output $out_var --action gen-target-src $all_dir_vars $in_var" ],

    'compile-shared-library' => [ "${dakota} --output $out_var \@\${dakota_lib_dir}/dakota/opts/compiler-shared-library-${platform}.opts \${compiler_shared_library_extra_opts} $all_dir_vars --var cxx=${cxx} $include_dirs $macros $in_var" ],
    'compile-executable' =>     [ "${dakota} --output $out_var \@\${dakota_lib_dir}/dakota/opts/compiler-executable-${platform}.opts \${compiler_executable_extra_opts} $all_dir_vars --var cxx=${cxx} $include_dirs $macros $in_var" ],
    'link-shared-library' =>    [ "${dakota} --output $out_var \@\${dakota_lib_dir}/dakota/opts/linker-shared-library-${platform}.opts \${linker_shared_library_extra_opts} --var cxx=${cxx} $rpaths $soname $in_var" ],
    'link-executable' =>        [ "${dakota} --output $out_var \@\${dakota_lib_dir}/dakota/opts/linker-executable-${platform}.opts \${linker_executable_extra_opts} --var cxx=${cxx} $rpaths $in_var" ],

    'cc-compile-shared-library' => [ "${cxx} --output $out_var \@\${dakota_lib_dir}/dakota/opts/compiler-shared-library-${platform}.opts \${cc_compiler_shared_library_extra_opts} $all_dir_vars $include_dirs $macros $in_var" ],
    'cc-compile-executable' =>     [ "${cxx} --output $out_var \@\${dakota_lib_dir}/dakota/opts/compiler-executable-${platform}.opts \${cc_compiler_executable_extra_opts} $all_dir_vars $include_dirs $macros $in_var" ],
    'cc-link-shared-library' =>    [ "${cxx} --output $out_var \@\${dakota_lib_dir}/dakota/opts/linker-shared-library-${platform}.opts \${cc_linker_shared_library_extra_opts} $rpaths $soname $in_var" ],
    'cc-link-executable' =>        [ "${cxx} --output $out_var \@\${dakota_lib_dir}/dakota/opts/linker-executable-${platform}.opts \${linker_executable_extra_opts} $rpaths $in_var" ],
  };
  my $rules = [];
  if (&is_so_path($root_tgt)) {
    if (! &is_empty($cc_o_paths) && &is_empty($dk_o_paths)) {
      &add_last($rules, [[$root_tgt],      'cc-link-shared-library', [@$cc_o_paths, @$so_paths], [@$order_only_targets]]);
    } else {
      &add_last($rules, [[$root_tgt],      'link-shared-library',    [@$dk_o_paths, @$cc_o_paths, $target_o_path, @$so_paths], [@$order_only_targets]]);
      &add_last($rules, [[$target_o_path], 'compile-shared-library', [$target_src_path], [$target_hdr_path]]); # using order-only prereqs
    }
  } else {
    if (! &is_empty($cc_o_paths) && &is_empty($dk_o_paths)) {
      &add_last($rules, [[$root_tgt],      'cc-link-executable', [@$cc_o_paths, @$so_paths], [@$order_only_targets]]);
    } else {
      &add_last($rules, [[$root_tgt],      'link-executable',    [@$dk_o_paths, $target_o_path, @$so_paths], [@$order_only_targets]]);
      &add_last($rules, [[$target_o_path], 'compile-executable', [$target_src_path], [$target_hdr_path]]); # using order-only prereqs
    }
  }
  if (! &is_empty($dk_o_paths)) {
    &add_last($rules, [[$target_hdr_path],        'gen-target-hdr', [$target_inputs_ast_path], []]);
    &add_last($rules, [[$target_src_path],        'gen-target-src', [$target_inputs_ast_path], []]);
    &add_last($rules, [[$target_inputs_ast_path], 'merge', [$target_srcs_ast_path, $target_libs_ast_path], []]);
    &add_last($rules, [[$target_srcs_ast_path],   'merge', [@$dk_ast_paths], []]);
    &add_last($rules, [[$target_libs_ast_path],   'merge', [@$so_ctlg_ast_paths], []]);
  }
  foreach my $dk_path (@$dk_paths) {
    my $dk_o_path = &o_path_from_dk_path($dk_path);
    my $dk_ast_path = &ast_path_from_dk_path($dk_path);
    if (&is_so_path($root_tgt)) {
      &add_last($rules, [[$dk_o_path], 'compile-shared-library', [$dk_path], [$target_hdr_path]]); # using order-only prereqs
    } else {
      &add_last($rules, [[$dk_o_path], 'compile-executable', [$dk_path], [$target_hdr_path]]); # using order-only prereqs
    }
    &add_last($rules, [[$dk_ast_path], 'parse', [$dk_path], []]);
  }
  foreach my $cc_path (@$cc_paths) {
    my $cc_o_path = &o_path_from_cc_path($cc_path);
    if (&is_so_path($root_tgt)) {
      &add_last($rules, [[$cc_o_path], 'cc-compile-shared-library', [$cc_path], []]);
    } else {
      &add_last($rules, [[$cc_o_path], 'cc-compile-executable', [$cc_path], []]);
    }
  }
  if (! &is_empty($dk_o_paths)) {
    foreach my $so_path (@$so_paths) {
      my $so_ctlg_path = &ctlg_path_from_so_path($so_path);
      my $so_ctlg_ast_path = &ast_path_from_ctlg_path($so_ctlg_path);
      &add_last($rules, [[$so_ctlg_ast_path], 'parse',   [$so_ctlg_path], []]);
      &add_last($rules, [[$so_ctlg_path],     'catalog', [$so_path], [$dakota_catalog]]); # using order-only prereqs
    }
  }
  return ($rules, $so_paths);
}
sub set_target {
  my ($input_path) = @_;
  my $build_cmk = &builder_cmk_parse($input_path);
  $ENV{'target'} = $$build_cmk{'target'};
}
# ${current_intmd_dir}/%.mk : ${current_source_dir}/%.cmk
# like used by out_path_from_in_path() in parse.pm
sub buildfile_path_from_cmk_path {
  my ($cmk_path) = @_;
  my $current_source_dir = $ENV{'current_source_dir'};
  my $current_intmd_dir =  $ENV{'current_intmd_dir'};

  $ENV{'current_source_dir'} = &dirname($cmk_path);
  $ENV{'current_intmd_dir'} =  &current_intmd_dir_from_current_source_dir($ENV{'current_source_dir'});

  my $path_from_cmk_path_sub = $$builder{'path-from-cmk-path'};
  my $result = &$path_from_cmk_path_sub($cmk_path);

  $ENV{'current_source_dir'} = $current_source_dir;
  $ENV{'current_intmd_dir'} =  $current_intmd_dir;

  return $result;
}
# ${library_output_directory}/${shared_library_prefix}${target}${shared_library_suffix}
# ${runtime_output_directory}/${target}${executable_suffix}
sub gen_root {
  my ($input_path, $output_path, $oi_tbl) = @_;
  my $force;
  &set_target($input_path);
  &set_dirs_from_input_path($input_path, $force = 1);
  die if ! &dirs();

  if (-e $output_path) {
    unlink $output_path;
  }
  my $header_lines = join($nl, @{$$builder{'header-lines'}});
  if ($header_lines) {
    &_filestr_to_file($header_lines . $nl . $nl, $output_path, '>>');
  }
  my $build_cmk = &builder_cmk_parse($input_path);

  if ($oi_tbl && scalar keys %$oi_tbl) {
    my $result = '';
    my $include_keyword = $$builder{'include-keyword'};
    my $ext =             $$builder{'ext'};

    foreach my $include_path (sort keys %$oi_tbl) {
      $result .= "$include_keyword $include_path" . $nl;
    }
    &_filestr_to_file($result, $output_path, '>>');
  }

  my $target = $$build_cmk{'target'};

  if (! $target) {
    return $output_path;
  }
  my $target_dir = $$build_cmk{'target-dir'};

  if ($target_dir) {
    $target_dir = &resolve_var($target_dir);
  } else {
    $target_dir = &current_build_dir();
  }
  my $target_type = $$build_cmk{'target-type'};
  my $target_path;
  if ($target_type && $target_type eq 'shared-library') {
   $target_path = $target_dir . '/' . &var('lib_prefix') . $target . &var('lib_suffix');
  } else {
   $target_path = $target_dir . '/' . $target . &var('exe_suffix');
  }
  $target_path = &expand($target_path);
  if (! &is_abs($target_path)) {
    $target_path = &current_source_dir() . '/' . $target_path;
  }
  my $io_vars = $$builder{'io-vars'};
  my ($rules, $so_paths) = &gen_rules($target_path, &parts_from_cmk($input_path, $force = 1), $build_cmk, $io_vars);
  my $gen_sub = $$builder{'gen-builder'};
  my $build_file = &$gen_sub($rules, $so_paths, $target);
  &_filestr_to_file($build_file, $output_path, '>>');
  &_filestr_to_file(&Dumper($rules), &build_rules_path(), '>');
  &add_last($all_rules, @$rules);
  &gen_dot($rules, &build_dot_path());
  #print STDERR &Dumper($rules);
  #print STDERR $build_file;
}
sub set_dirs_from_input_path {
  my ($input_path, $force) = @_;
  return if ! $force && &dirs();
  my $current_source_dir = &dirname($input_path);
  my $source_dir;
  if ($ENV{'source_dir'}) {
    $source_dir = $ENV{'source_dir'};
  } else {
    $source_dir = $gbl_prefix;
  }
  my $current_intmd_dir = &current_intmd_dir_from_current_source_dir($current_source_dir);
  my $build_dir = &build_dir();
  my $current_vars = {
    'current_source_dir' => $current_source_dir,
    'current_intmd_dir' =>  $current_intmd_dir,
    'source_dir' =>         $source_dir,
    'build_dir' =>          $build_dir,
  };
  &set_env_vars_core($current_vars, $force);
}
sub input_path {
  my ($input_path) = @_;
  if (-d $input_path) {
    $input_path .= "/$root_name.cmk";
  } elsif (! -f $input_path) {
    if (-f     "$input_path.cmk") {
      $input_path = "$input_path.cmk";
    } else {
      die &basename($0) . ': error: no-such-file: ' . $input_path . $nl;
    }
  }
  return $input_path;
}
sub subdir_paths {
  my ($input_path, $build_cmk) = @_;
  my $subdir_paths = [];
  $input_path = &input_path($input_path);
  if ($$build_cmk{'subdirs'} && scalar @{$$build_cmk{'subdirs'}}) {
    my $current_source_dir = &dirname($input_path);
    foreach my $subdir (@{$$build_cmk{'subdirs'}}) {
      my $subdir_path = &input_path("$current_source_dir/$subdir/$root_name.cmk");
      &add_last($subdir_paths, $subdir_path);
    }
  }
  return $subdir_paths;
}
sub target_lib_paths {
  my ($input_path, $build_cmk) = @_;
  my $target_lib_paths = [];
  $input_path = &input_path($input_path);
  if ($$build_cmk{'target-libs'} && scalar @{$$build_cmk{'target-libs'}}) {
    my $current_source_dir = &dirname($input_path);
    foreach my $target_lib (@{$$build_cmk{'target-libs'}}) {
      if (-f "$current_source_dir/$target_lib.cmk") {
        my $target_lib_path = &input_path("$current_source_dir/$target_lib.cmk"); # could be more clever here
        &add_last($target_lib_paths, $target_lib_path);
      }
    }
  }
  return $target_lib_paths;
}
sub dakota_make {
  my ($input_path, $output_path) = @_;
  my $output_paths = [ $output_path ];
  my $build_cmk = &builder_cmk_parse($input_path);

  my $subdir_inputs =     &subdir_paths($input_path, $build_cmk);
  my $target_lib_inputs = &target_lib_paths($input_path, $build_cmk);

  my $oi_tbl = {};
  foreach my $sub_input_path (@$subdir_inputs, @$target_lib_inputs) {
    my $sub_output_path = &buildfile_path_from_cmk_path($sub_input_path);
    my $sub_output_paths = &dakota_make($sub_input_path, $sub_output_path); # recursive
    $$oi_tbl{$sub_output_path} = $input_path;
    &add_last($output_paths, @$sub_output_paths);
    #print STDERR $output_path . $nl;
  }
  &gen_root($input_path, $output_path, $oi_tbl);
  #print STDERR $output_path . $nl;
  return $output_paths;
}
my $gbl_paths;
sub gbl_paths {
  return $gbl_paths;
}
sub set_gbl_paths {
  my ($output_files) = @_;
  my %tmp = map { $_ => 1 } split("\n", &_filestr_from_file($output_files));
  $gbl_paths = \%tmp;
}
sub dir_is_empty {
  my ($dir) = @_;
  opendir(my $dh, $dir) or die "Not a directory: $!";
  return scalar(grep { $_ ne "." && $_ ne ".." } readdir($dh)) == 0;
}
sub find_cb {
  if ($$gbl_cmd_info{'clean-excludes'}{ &basename($File::Find::name)} ) { return; }
  my $paths = &gbl_paths();
  if (-f $File::Find::name && ! $$paths{$File::Find::name}) {
    unlink $File::Find::name;
  } elsif (-d $File::Find::name && &dir_is_empty($File::Find::name)) {
    rmdir $File::Find::name;
  }
}
sub dakota_make_cmd {
  my ($argv) = @_;
  my $build_file_name = &build_file_name();
  my $rel_intmd_dir = &rel_intmd_dir();
  $gbl_cmd_info = &cmd_info_from_argv($argv);
  if (exists $$gbl_cmd_info{'opts'}{'build'}) {
    if (! $$gbl_cmd_info{'opts'}{'build'}) {
      $$gbl_cmd_info{'opts'}{'build'} = "$build_file_name.d";
      $$gbl_cmd_info{'opts'}{'build'} = File::Spec->rel2abs($$gbl_cmd_info{'opts'}{'build'});
      print STDERR &basename($0) . ": using '--build $$gbl_cmd_info{'opts'}{'build'}' as default" . $nl;
    } else {
      $$gbl_cmd_info{'opts'}{'build'} = File::Spec->rel2abs($$gbl_cmd_info{'opts'}{'build'});
    }

    if (! -e $$gbl_cmd_info{'opts'}{'build'} . '/' . $rel_intmd_dir &&
          -e $$gbl_cmd_info{'opts'}{'build'} . '/' . "$build_file_name.d" . '/' . $rel_intmd_dir) {
      $$gbl_cmd_info{'opts'}{'build'} .= '/' . "$build_file_name.d";
      print STDERR &basename($0) . ": using '--build $$gbl_cmd_info{'opts'}{'build'}'" . $nl;
    }
    $$gbl_cmd_info{'opts'}{'build'} = File::Spec->rel2abs($$gbl_cmd_info{'opts'}{'build'});
    $ENV{'build_dir'} = $$gbl_cmd_info{'opts'}{'build'};
    $builder = &get_builder();
    my $buildfile = $$gbl_cmd_info{'opts'}{'build'} . '/' . $rel_intmd_dir . "/build.$$builder{'ext'}";
    my $keep_going = "";
    if ($$gbl_cmd_info{'opts'}{'keep-going'}) {
      $keep_going = $$builder{'keep-going'};
    }
    my $cmd = "$ENV{'builder'} @{$$builder{'opts'}} $keep_going -f $buildfile " . "@{$$gbl_cmd_info{'inputs'}}";
    $cmd =~ s/\s+/ /g;
    print STDERR $cmd . $nl if $ENV{'print_builder_cmd'};
    system $cmd;
  } elsif ($$gbl_cmd_info{'opts'}{'clean'}) {
    my $bname = &parts_bname();
    $$gbl_cmd_info{'clean-excludes'}{$bname} = 1; # hackhack
    my $output_files = $$gbl_cmd_info{'opts'}{'clean'} . '/' . $rel_intmd_dir . "/build.files.txt";
    &set_gbl_paths($output_files);
    #no warnings 'File::Find';
    finddepth \&find_cb, $$gbl_cmd_info{'opts'}{'clean'};
  } else { # generate buildsystem
    if (! $$gbl_cmd_info{'opts'}{'source-dir'}) {
      $$gbl_cmd_info{'opts'}{'source-dir'} = '.';
    }
    $$gbl_cmd_info{'opts'}{'source-dir'} = File::Spec->rel2abs($$gbl_cmd_info{'opts'}{'source-dir'});
    # checkout for $build_file_name ???
    $ENV{'source_dir'} = $$gbl_cmd_info{'opts'}{'source-dir'};

    if (! $$gbl_cmd_info{'opts'}{'build-dir'}) {
      $$gbl_cmd_info{'opts'}{'build-dir'} = "$build_file_name.d";
      $$gbl_cmd_info{'opts'}{'build-dir'} = File::Spec->rel2abs($$gbl_cmd_info{'opts'}{'build-dir'});
      print STDERR &basename($0) . ": using '--build-dir $$gbl_cmd_info{'opts'}{'build-dir'}' as default" . $nl;
    } else {
      $$gbl_cmd_info{'opts'}{'build-dir'} = File::Spec->rel2abs($$gbl_cmd_info{'opts'}{'build-dir'});
    }

    if ($$gbl_cmd_info{'opts'}{'build-dir'} eq $$gbl_cmd_info{'opts'}{'source-dir'}) {
      $$gbl_cmd_info{'opts'}{'build-dir'} .= '/' . "$build_file_name.d";
      print STDERR &basename($0) . ": using '--build-dir $$gbl_cmd_info{'opts'}{'build-dir'}'" . $nl;
    }
    $$gbl_cmd_info{'opts'}{'build-dir'} = File::Spec->rel2abs($$gbl_cmd_info{'opts'}{'build-dir'});
    $ENV{'build_dir'} = $$gbl_cmd_info{'opts'}{'build-dir'};
    $builder = &init_builder();
    while (my ($k, $v) = each (%{$$gbl_cmd_info{'vars'}})) {
      $ENV{$k} = $v;
    }

    my $input_path = "$$gbl_cmd_info{'opts'}{'source-dir'}/$build_file_name";
    die "missing $build_file_name in '$$gbl_cmd_info{'opts'}{'source-dir'}'" if ! -f $input_path;
    $root_name = &basename($input_path);
    $root_name =~ s/\.cmk$//;
    my $output_path = &buildfile_path_from_cmk_path($input_path);

    if (exists $$gbl_cmd_info{'opts'}{'path-only'}) {
      print $output_path . $nl;
      exit 0;
    }
    my $output_paths = &dakota_make($input_path, $output_path);
    my $output_files = &dirname($output_path) . "/build.files.txt";
    my $output_dot =   &dirname($output_path) . "/build.dot";
    &add_first($output_paths, $output_files);
    &_filestr_to_file(join($nl, @$output_paths) . $nl, $output_files, '>');
    &gen_dot($all_rules, $output_dot);
  }
}
unless (caller) {
  &dakota_make_cmd(\@ARGV);
  exit 0;
}
