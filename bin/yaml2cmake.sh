#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace
#export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
source_dir=$(cd $samedir/.. && pwd)
source "$source_dir/vars.sh"

mkdir -p $build_dir/{dakota-dso,dakota-find-library,dakota-catalog,dakota-core,dakota}

yaml2cmake.pl dakota-dso/build.yaml          > $build_dir/dakota-dso/build.cmake
yaml2cmake.pl dakota-find-library/build.yaml > $build_dir/dakota-find-library/build.cmake
yaml2cmake.pl dakota-catalog/build.yaml      > $build_dir/dakota-catalog/build.cmake
yaml2cmake.pl dakota-core/build.yaml         > $build_dir/dakota-core/build.cmake
yaml2cmake.pl dakota/build.yaml              > $build_dir/dakota/build.cmake
