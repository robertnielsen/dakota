#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
source_dir=$(cd $samedir/.. && pwd)
source "$source_dir/vars.sh"

if test $# -eq 0; then
  progname=$(basename $0)
  echo "usage: $progname <shared-library> [<shared-library ...]"
  exit 1
fi

$source_dir/bin/dakota-catalog --json --output /dev/null $@ > klass-hier.json
$source_dir/klass-hier.pl < klass-hier.json > klass-hier.dot
open klass-hier.dot

#dot -Tpdf -o klass-hier.pdf klass-hier.dot
#open klass-hier.pdf
