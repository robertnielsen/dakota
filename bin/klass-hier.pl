#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-

use strict;
use warnings;
use Data::Dumper;
$Data::Dumper::Terse     = 1;
$Data::Dumper::Deepcopy  = 1;
$Data::Dumper::Purity    = 1;
$Data::Dumper::Quotekeys = 1;
$Data::Dumper::Indent    = 1; # default = 2

my $nl = "\n";
undef $/;
my $data = <STDIN>;
$data =~ s/("\s*):(\s*("|\{|\[|\d))/$1=>$2/g;
$data = eval "$data" or die;

my $graph = {
  'keywords' => {
    'graph' => {
      'rankdir'  => 'LR',
      'compound' => 'true',
      'nodesep'  => '0.04',
    },
    'node' => {
      'shape'    => 'rect',
      'style'    => 'rounded',
      'height'   => '0',
      'width'    => '0',
      'penwidth' => '1.5',
    },
    'edge' => {
      'dir' => 'back',
    },
  },
  'nodes' => {},
  'edges' => {},
  'subgraphs' => {}
};
my $extra_graph_roots = { 'object' => 1,
                          'number' => 1 };
#$extra_graph_roots = undef;

my $subklasses = { 'object' => {} };
foreach my $name (sort keys %$data) {
  my $info = $$data{$name};
  if ($$info{'type'} eq 'klass') {
    my $superklass = $$info{'superklass'};
    $superklass = 'object' if ! $superklass;
    $$subklasses{$superklass} = {} if ! $$subklasses{$superklass};
    $$subklasses{$superklass}{$name} = 1;
    foreach my $trait (sort @{$$info{'traits'} || []}) {
      #
    }
  } elsif ($$info{'type'} eq 'trait') {
    #
  }
}
#print STDERR &Dumper($subklasses);

foreach my $extra_graph_root (sort keys %{$extra_graph_roots || {}}) {
  my $num_methods = 0;
  $num_methods = scalar keys %{$$data{$extra_graph_root}{'methods'} || {}} if $$data{$extra_graph_root}{'methods'};
  $$graph{'subgraphs'}{"cluster-$extra_graph_root"}{'nodes'}{"alt-$extra_graph_root"} = { 'label' => "$extra_graph_root/$num_methods",
                                                                                          'color' => 'green' };
  $$graph{'subgraphs'}{'cluster-<main>'}{'nodes'}{$extra_graph_root} = { 'label' => "$extra_graph_root/$num_methods",
                                                                         'color' => 'green' };
  $$graph{'edges'}{$extra_graph_root}{"alt-$extra_graph_root"} = { 'color' => 'green' };
}
foreach my $name (sort keys %$data) {
  my $info = $$data{$name};
  my $num_methods = 0;
  $num_methods = scalar keys %{$$data{$name}{'methods'} || {}} if $$data{$name}{'methods'};
  if ($$info{'type'} eq 'klass') {
    my $superklass = $$info{'superklass'};
    if ($name ne 'object') {
      $superklass = 'object' if ! $superklass;
      if ($$extra_graph_roots{$superklass} && ! $$subklasses{$name}) {
        if (! $$graph{'subgraphs'}{"cluster-$superklass"}{'nodes'}{$name}) {
          $$graph{'subgraphs'}{"cluster-$superklass"}{'nodes'}{$name} = { 'label' => "$name/$num_methods" };
        }
        $$graph{'subgraphs'}{"cluster-$superklass"}{'edges'}{"alt-$superklass"}{$name} = {};
      } else {
        if (! $$graph{'subgraphs'}{'cluster-<main>'}{'nodes'}{$name}) {
          $$graph{'subgraphs'}{'cluster-<main>'}{'nodes'}{$name} = { 'label' => "$name/$num_methods" };
        }
        $$graph{'subgraphs'}{'cluster-<main>'}{'edges'}{$superklass}{$name} = {};
      }
    }
    foreach my $trait (sort @{$$info{'traits'} || []}) {
      $$graph{'subgraphs'}{'cluster-<main>'}{'edges'}{$trait}{$name} = { 'color' => 'blue' };
    }
  } elsif ($$info{'type'} eq 'trait') {
    $$graph{'subgraphs'}{'cluster-<main>'}{'nodes'}{$name} = { 'label' => "$name/$num_methods",
                                                               'color' => 'blue' };
    if (scalar @{$$info{'requires'} || []}) {
      foreach my $require (sort @{$$info{'requires'}}) {
        $$graph{'subgraphs'}{'cluster-<main>'}{'edges'}{$require}{$name} = { 'color' => 'magenta' };
      }
    } else {
      $$graph{'subgraphs'}{'cluster-<main>'}{'edges'}{'object'}{$name} = { 'color' => 'magenta' };
    }
    foreach my $trait (sort @{$$info{'traits'} || []}) {
      $$graph{'subgraphs'}{'cluster-<main>'}{'edges'}{$trait}{$name} = { 'color' => 'blue' };
    }
  } else {
    die;
  }
}
#print STDERR &Dumper($graph);

sub dump_digraph {
  my ($graph, $col) = @_;
  $col = 1 if ! $col;
  my $result = '';
  my $pad = '  ';
  print $pad x $col . 'graph [ ordering = "out" ];' . $nl;
  foreach my $keyword (sort keys %{$$graph{'keywords'}}) {
    foreach my $attr_lhs (sort keys %{$$graph{'keywords'}{$keyword}}) {
      my $attr_rhs = $$graph{'keywords'}{$keyword}{$attr_lhs};
      print $pad x $col . "$keyword [ $attr_lhs = \"$attr_rhs\" ];" . $nl;
    }
  }
  foreach my $subgraph (sort keys %{$$graph{'subgraphs'}}) {
    print $pad x $col . "subgraph \"$subgraph\" {" . $nl;
    $col++;
    $result .= &dump_digraph($$graph{'subgraphs'}{$subgraph}, $col);
    $col--;
    print $pad x $col . '}' . $nl;
  }
  foreach my $edge_lhs (sort keys %{$$graph{'edges'}}) {
    foreach my $edge_rhs (sort keys %{$$graph{'edges'}{$edge_lhs}}) {
      print $pad x $col . "\"$edge_lhs\" -> \"$edge_rhs\"";
      if (scalar keys %{$$graph{'edges'}{$edge_lhs}{$edge_rhs}}) {
        my $d = '';
        print ' [';
        foreach my $attr_lhs (sort keys %{$$graph{'edges'}{$edge_lhs}{$edge_rhs}}) {
          my $attr_rhs = $$graph{'edges'}{$edge_lhs}{$edge_rhs}{$attr_lhs};
          print "$d $attr_lhs = \"$attr_rhs\"";
          $d = ',';
        }
        print ' ]';
      }
      print ';' . $nl;
    }
  }
  foreach my $node (sort keys %{$$graph{'nodes'}}) {
    print $pad x $col . "\"$node\"";
    if (scalar keys %{$$graph{'nodes'}{$node}}) {
      my $d = '';
      print ' [';
      foreach my $attr_lhs (sort keys %{$$graph{'nodes'}{$node}}) {
        my $attr_rhs = $$graph{'nodes'}{$node}{$attr_lhs};
        print "$d $attr_lhs = \"$attr_rhs\"";
        $d = ',';
      }
      print ' ]';
    }
    print ';' . $nl;
  }
  return $result;
}
print 'digraph {' . $nl;
&dump_digraph($graph);
print '}' . $nl;
