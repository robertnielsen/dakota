// -*- mode: c++; c-basic-offset: 2; tab-width: 2; indent-tabs-mode: nil -*-

// Copyright (C) 2007 - 2022 Robert Nielsen <robert@dakota.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

# include "dakota-platform.h"
# include "dakota-config.h"

# if defined HAVE_ELF_H
  # include <elf.h> // struct Elf64_Ehdr, Elf64_Half
  # if defined HAVE_LINK_H
    # include <link.h> // struct link_map
  # endif
  # if !defined _GNU_SOURCE
    # define _GNU_SOURCE // dlinfo()
  # endif
# elif defined HAVE_MACH_O_LOADER_H
  # include <mach-o/loader.h>
  # if defined HAVE_MACH_O_DYLD_H
    # include <mach-o/dyld.h> // _dyld_image_count(), _dyld_get_image_name()
  # endif
# else
  # error
# endif

# include <cassert> // assert()
# include <cstdio>  // sprintf(), fprintf(), stdout, stderr
# include <cstdlib> // EXIT_SUCCESS, EXIT_FAILURE, realpath(), free()
# include <cstring> // strcmp()
# include <sys/param.h> // MAXPATHLEN

# include "dakota-dso.h"

# define cast(...) (__VA_ARGS__)

// http://pubs.opengroup.org/onlinepubs/009695399/basedefs/dlfcn.h.html

typealias bool_t = bool;
typealias char_t = char;
typealias int_t =  int;
typealias uint_t = unsigned int;
typealias dl_info_t = Dl_info;

static FUNC dso_dladdr(ptr_t addr, Dl_info* info) -> int_t {
  int_t result = dladdr(addr, info);
  return result;
}
FUNC dso_dlclose(ptr_t handle) -> int_t {
  int_t result = dlclose(handle);
  return result;
}
# if defined HAVE_DLINFO
static FUNC dso_dlinfo(ptr_t handle, int_t request, ptr_t info) -> int_t {
  int_t result = dlinfo(handle, request, info);
  return result;
}
# endif
FUNC dso_dlopen(str_t filename, int_t flags) -> ptr_t {
  ptr_t result = dlopen(filename, flags);
  return result;
}
FUNC dso_dlsym(ptr_t handle, str_t sym) -> ptr_t {
  ptr_t result = dlsym(handle, sym);
  return result;
}
// returns nullptr if no exact match
FUNC dso_sym_name_for_addr(ptr_t addr) -> str_t {
  assert(addr != nullptr);
  str_t result = nullptr;
  dl_info_t dli = {};
  if (dso_dladdr(addr, &dli) != 0 && (dli.dli_saddr == addr))
    result = dli.dli_sname;
  return result;
}
FUNC dso_path_for_handle(ptr_t handle) -> str_t {
  assert(handle != nullptr);
# if defined HAVE_DLINFO && defined HAVE_LINK_MAP
  struct link_map* l_map = nullptr;
  int_t r = dso_dlinfo(handle, RTLD_DI_LINKMAP, &l_map);
  if (r == 0 && l_map != nullptr && l_map->l_name != nullptr)
    return l_map->l_name;
# elif defined HAVE_MACH_O_DYLD_H
  int32_t image_count = cast(int32_t)_dyld_image_count();
  for (int32_t i = 0; i < image_count; i++) {
    str_t image_name = _dyld_get_image_name(cast(uint32_t)i);
    ptr_t image_handle = dso_dlopen(image_name, RTLD_NOLOAD);
    dso_dlclose(image_handle);
    if (handle == image_handle)
      return image_name;
  }
# else
  # error
# endif
  return nullptr;
}
FUNC dso_path_for_lib_name(str_t lib_name) -> str_t {
  assert(lib_name != nullptr);
  str_t path = nullptr;
  ptr_t handle = dso_dlopen(lib_name, RTLD_LAZY | RTLD_LOCAL);
  if (handle != nullptr) {
    path = dso_path_for_handle(handle);
    //dso_dlclose(handle); // bugbug (this should be unloaded)
  }
  return path;
}
static FUNC shared_library_prefix() -> str_t {
  return DKT_LIB_PREFIX_STR;
}
static FUNC shared_library_suffix() -> str_t {
  return DKT_LIB_SUFFIX_STR;
}
FUNC dso_find_library(str_t lib_name, char_t* buf, size_t buflen) -> str_t {
  assert(lib_name != nullptr);
  assert(buf != nullptr);
  assert(buflen != 0);
  // libfoo.so
  str_t path = dso_path_for_lib_name(lib_name);
  if (path != nullptr) {
    strncpy(buf, path, buflen);
    return buf;
  }
  char_t tmp[MAXPATHLEN] = "";
  // foo => libfoo.so
  snprintf(tmp, sizeof(tmp), "%s%s%s", shared_library_prefix(), lib_name, shared_library_suffix());
  path = dso_path_for_lib_name(tmp);
  if (path != nullptr) {
    strncpy(buf, path, buflen);
    return buf;
  }
  // libfoo => libfoo.so
  snprintf(tmp, sizeof(tmp), "%s%s", lib_name, shared_library_suffix());
  path = dso_path_for_lib_name(tmp);
  if (path != nullptr) {
    strncpy(buf, path, buflen);
    return buf;
  }
  // foo.so => libfoo.so
  snprintf(tmp, sizeof(tmp), "%s%s", shared_library_prefix(), lib_name);
  path = dso_path_for_lib_name(tmp);
  if (path != nullptr) {
    strncpy(buf, path, buflen);
    return buf;
  }
  return nullptr;
}
FUNC dso_header_of_image_containing_addr(ptr_t addr) -> ptr_t {
  assert(addr != nullptr);
  ptr_t result = nullptr;
  dl_info_t dli = {};
  if (dso_dladdr(addr, &dli))
    return dli.dli_fbase;
  return result;
}
FUNC dso_path_of_image_containing_addr(ptr_t addr) -> str_t {
  assert(addr != nullptr);
  str_t result = nullptr;
  dl_info_t dli = {};
  if (dso_dladdr(addr, &dli))
    result = dli.dli_fname;
  return result;
}
FUNC dso_file_type_of_image_containing_addr(ptr_t addr) -> str_t {
  assert(addr != nullptr);
  str_t result = nullptr;
  dl_info_t dli = {};
  if (dso_dladdr(addr, &dli)) {
    str_t executable =     "executable";
    str_t shared_library = "shared-library";
# if defined HAVE_ELF64_EHDR
    Elf64_Half type = (cast(const Elf64_Ehdr*)dli.dli_fbase)->e_type;
    switch (type) {
      case ET_EXEC:
        result = executable;
        break;
      case ET_DYN:
        result = shared_library;
        break;
    }
# elif defined HAVE_MACH_HEADER_64
    uint32_t type = (cast(const struct mach_header_64*)dli.dli_fbase)->filetype;
    switch (type) {
      case MH_EXECUTE:
        result = executable;
        break;
      case MH_DYLIB:
        result = shared_library;
        break;
    }
# else
  # error
# endif
  }
  return result;
}
