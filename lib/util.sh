#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }

# source_dir=          ...
# current_source_dir=  ...
# project_source_dir=  ...

# intmd_dir=           $(intmd-dir)
# build_dir=           $(build-dir)

# current_intmd_dir=   $(current-intmd-dir-from-current-source-dir $current_source_dir)
# current_build_dir=   $(current-build-dir-from-current-source-dir $current_source_dir)

# project_intmd_dir=   $(current-intmd-dir-from-current-source-dir $project_source_dir) # no special functions needed for project_<>_dir
# project_build_dir=   $(current-build-dir-from-current-source-dir $project_source_dir) # no special functions needed for project_<>_dir

# use pattern for find-source-dir()
#
# source_dir=$(find-source-dir)
# source "$source_dir/lib/util.sh"
# intmd_dir=$(intmd-dir)
# ...
function find-source-dir() {
  while test $PWD != / && ! test -f $PWD/lib/util.sh; do
    cd ..
  done
  test $PWD != / && echo $PWD
}
function ee() { ( PS4= && set -o xtrace && "$@" ); }
function canon-path() {
  local path=$1
  path=${path#./} # remove leading ./
  path=${path%/.} # remove trailing /.
  path=${path//\/\//\/}  # // => /
  path=${path//\/.\//\/} # /./ => /
  echo "$path"
}
function die() {
  local msg=$1
  local frame_id; frame_id=$(make-frame-id-frame 1)
  first_frame=1 msg=$msg dump-stack-trace $frame_id "$msg"
  exit 1
}
function ref-is-set() {
  local ref=$1
  test ${!ref:-0} -ne 0
}
function ref-is-empty() {
  local ref=$1
  ref=${ref/@/*} # thing[@] => thing[*]
  ! test "${!ref:-}"
}
function ref-is-equal() {
  local ref1=$1 ref2=$2
  ! test "${ref1:-}" == "${ref2:-}"
}
function make-frame-id() {
  local src=$1 lineno=$2 funcname=$3
  test "$lineno$funcname$src" || return
  echo "$(basename "$src"):$lineno:${funcname:-main}()"
}
function make-frame-id-frame() {
  local frame=$1
  local   lineno funcname src
  read -r lineno funcname src <<< $(caller $frame)
  test "$lineno$funcname$src" || return
  make-frame-id $src $lineno "$funcname"
}
function dump-stack-trace() {
  local error_frame_id=${1:-} cmd=${2:-}
  if ! ref-is-set no_debug; then
    local frame=${first_frame:-0}
    while true; do
      local   lineno funcname src # BASH_LINENO FUNCNAME BASH_SOURCE
      read -r lineno funcname src <<< $(caller $frame) && frame=$(( $frame + 1 ))
      ! test $src || src=$(abs-path $src)
      test "$lineno$funcname$src" || break
      local frame_id; frame_id=$(make-frame-id $src $lineno "$funcname") # can never be empty

      if test $frame_id == "$error_frame_id"; then
        printf "%s: line %s: %s(): %s\n" \
          $src $lineno $funcname "$cmd" 1>&2
      else
        printf "%s: line %s: %s()\n" \
          $src $lineno $funcname 1>&2
      fi
    done
  fi
} # dump-stack-trace()
function abs-path() {
  local path=$1
  echo $(cd $(dirname $path) && pwd)/$(basename $path)
}
function rel-path() {
  local path=$1 dir=${2:-$PWD}
  test $path == "." && path=$PWD
  test $dir  == "." && dir=$PWD
  if test $path == $dir; then
    echo "."
    return
  fi
  if ! test -d "$dir"; then
    die "$FUNCNAME() optional second arg must be a directory"
  fi
  local name=
  if test -f "$path"; then
    name=/$(basename "$path")
    path=$(dirname "$path")
  fi
  local s; s=$(cd ${dir%%/} && pwd)
  local d; d=$(cd $path     && pwd)
  local b=
  while test "${d#$s/}" == "$d"; do
    if test $s == "/"; then break; fi
    s=$(dirname $s)
    b="../$b"
  done
  if test ${debug:-0} -ne 0; then
    echo path: $path 1>&2
    echo dir:  $dir  1>&2
  fi
  local result=$b${d#$s/}$name
  canon-path $result
}
function rel-subdir-from-dir() {
  local inner_rel_dir=$1 subdir=$2 dir=$3
  validate-subdir $subdir $dir
  local rel_dir; rel_dir=$(rel-path $subdir $dir)
  local result=$dir/$inner_rel_dir/$rel_dir
  canon-path $result
}
function validate-subdir() {
  local subdir=$1 dir=$2
  subdir=$(abs-path $subdir)
  dir=$(abs-path $dir)
  orig_subdir=$subdir
  test $dir == "/" && return
  while test $subdir != /; do
    subdir=$(dirname $subdir)
    test $subdir == $dir && return
  done
  die "'$orig_subdir' is not a sub-directory of '$dir'"
}
function host-system-name() {
  uname -s | tr [:upper:] [:lower:]
}
function build-file-name() {
  echo build.cmk
}
# $HOME/projects/my-project/
# =>
# $HOME/tmp/projects/my-project/<build-file-name>.d
function default-build-dir-from-source-dir() {
  local source_dir=${1:-$source_dir}
  local rel_dir; rel_dir=$(rel-subdir-from-dir 'tmp' $source_dir $HOME)
  echo $rel_dir/$(build-file-name).d
}
# $HOME/projects/my-project/
# =>
# $HOME/opt-stage/projects/my-project/
function default-stage-prefix-dir-from-source-dir() {
  local source_dir=${1:-$source_dir}
  rel-subdir-from-dir 'opt-stage' $source_dir $HOME
}

function rel-intmd-dir() {
  echo dkt
}
function source-dir() {
  if ! test ${source_dir:-}; then
    die "missing env var 'source_dir'"
  fi
  canon-path $source_dir
}
function build-dir() {
  if ! test ${build_dir:-}; then
    die "missing env var 'build_dir'"
  fi
  canon-path $build_dir
}
function intmd-dir() {
  if test ${intmd_dir:-}; then
    echo $intmd_dir
  else
    local bld_dir; bld_dir=$(build-dir)
    local result; result=$bld_dir/$(rel-intmd-dir)
    canon-path $result
  fi
}
function current-intmd-dir-from-current-source-dir() {
  local current_source_dir=$1
  local current_build_dir; current_build_dir=$(current-build-dir-from-current-source-dir $current_source_dir)
  local result=$current_build_dir/$(rel-intmd-dir)
  canon-path $result
}
function current-build-dir-from-current-source-dir() {
  local current_source_dir=$1
  local bld_dir; bld_dir=$(build-dir)
  local rel_current_source_dir; rel_current_source_dir=$(rel-path $current_source_dir $(source-dir))
  if test $rel_current_source_dir == '.'; then
    echo $bld_dir
    return
  fi
  local result; result=$bld_dir/$rel_current_source_dir
  canon-path $result
}
function cpu-thread-count() {
  local threads=$(getconf _NPROCESSORS_ONLN)
  echo $threads
}
function cpu-core-count() {
  if test $(host-system-name) == darwin; then
    sysctl -n hw.physicalcpu
  else
    getconf _NPROCESSORS_ONLN
  fi
}
function builder-jobs() {
  local jobs; jobs=$(( $(cpu-core-count) + 2 ))
  echo $jobs
}

function translate-path() {
  local func=$1
  shift
  $func "$@"
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  translate-path "$@"
fi
