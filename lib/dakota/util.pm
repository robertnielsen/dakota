#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-
# -*- tab-width: 2
# -*- indent-tabs-mode: nil

# Copyright (C) 2007 - 2022 Robert Nielsen <robert@dakota.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

package dakota::util;

use strict;
use warnings;
use sort 'stable';

use Digest::MD5 qw(md5 md5_hex md5_base64);

my $nl;
my $gbl_prefix;
my $gbl_platform_tbl;
my $h_ext;
my $cc_ext;
my $o_ext;
my $lib_prefix;
my $lib_suffix;
my $exe_suffix;

sub dk_prefix {
  my ($path) = @_;
  $path =~ s|//+|/|;
  $path =~ s|/\./+|/|;
  $path =~ s|^./||;
  if (-d "$path/bin" && -d "$path/lib") {
    return $path
  } elsif ($path =~ s|^(.+?)/+[^/]+$|$1|) {
    return &dk_prefix($path);
  } else {
    die "Could not determine \$prefix from executable path $0: $!\n";
  }
}
use Carp; $SIG{ __DIE__ } = sub { Carp::confess( @_ ) };
#use Carp::Assert; # not installed on stock macos

use Data::Dumper;
$Data::Dumper::Terse =     1;
$Data::Dumper::Deepcopy =  1;
$Data::Dumper::Purity =    1;
$Data::Dumper::Useqq =     1;
$Data::Dumper::Sortkeys =  0;
$Data::Dumper::Indent =    1;  # default = 2

use Getopt::Long qw(GetOptionsFromArray);
$Getopt::Long::ignorecase = 0;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT= qw(
                 add_first
                 add_last
                 ann
                 as_literal_symbol
                 as_literal_symbol_interior
                 at
                 basename
                 bin_dir
                 build_dir
                 build_file_name
                 build_dot_path
                 build_mk_path
                 build_rules_path
                 builder_cmk_parse
                 canon_path
                 colin
                 colout
                 core_count
                 cpp_directives
                 ct
                 current_build_dir
                 current_intmd_dir
                 current_source_dir
                 cxx
                 dakota_home
                 decode_comments
                 decode_strings
                 deep_copy
                 default_stage_prefix_dir_from_source_dir
                 digsig
                 dirname
                 dirs
                 dk_mangle
                 dk_mangle_seq
                 dmp
                 do_json
                 dqstr_pre_ident
                 dqstr_regex
                 dqstr_strip
                 dump_env
                 echo_output_path
                 encode_char
                 encode_comments
                 encode_strings
                 _filestr_to_file
                 filestr_from_file
                 filestr_to_file
                 find_cmd
                 first
                 flatten
                 gen_parts
                 has_kw_arg_names
                 has_kw_args
                 has_va_prefix
                 header_file_regex
                 ident_regex
                 int_from_str
                 builder_jobs
                 rel_intmd_dir
                 intmd_dir
                 current_build_dir_from_current_source_dir
                 current_intmd_dir_from_current_source_dir
                 current_target_build_dir
                 current_target_intmd_dir
                 current_target_x_build_dir
                 current_target_x_intmd_dir
                 is_abs
                 is_array
                 is_array_type
                 is_ast_path
                 is_box_type
                 is_cc_path
                 is_ctlg_path
                 is_ctlg_ast_path
                 is_debug
                 is_decl
                 is_dk_path
                 is_dk_src_path
                 is_exe_path
                 is_exported
                 is_kw_args_method
                 is_o_path
                 is_out_of_date
                 is_slots
                 is_so_path
                 is_src
                 is_src_decl
                 is_src_defn
                 is_super
                 is_target
                 is_target_decl
                 is_target_defn
                 is_target_o_path
                 is_va
                 int_klass4alias
                 kw_arg_generics
                 kw_arg_placeholders
                 kw_args_method_sig
                 macro_balenced_info
                 macro_lang_info
                 last
                 lib_dir
                 longest_common_prefix
                 make_dir
                 make_dirname
                 max
                 method_sig_regex
                 method_sig_type_regex
                 min
                 mtime
                 needs_hex_encoding
                 normalize_paths
                 num_kw_arg_names
                 num_kw_args
                 out_of_date
                 pad_var
                 pann
                 param_types_str
                 parts
                 parts_from_cmk
                 parts_bname
                 parts_path
                 path_split
                 platform_tbl
                 prepend_dot_slash
                 realpath
                 rel_target_hdr_path
                 remove_extra_whitespace
                 remove_first
                 remove_last
                 remove_name_va_scope
                 remove_non_newlines
                 replace_first
                 replace_last
                 resolve_var
                 rewrite_klass_defn_with_implicit_metaklass_defn
                 rewrite_scoped_int_uint
                 rewrite_stencils
                 root_cmd
                 scalar_from_file
                 scalar_to_file
                 set_dirs
                 set_env_vars
                 set_env_vars_core
                 set_root_cmd
                 set_src_decl
                 set_src_defn
                 set_target_decl
                 set_target_defn
                 source_dir
                 split_path
                 sqstr_regex
                 stage_bin_dir
                 stage_lib_dir
                 str_from_seq
                 str_klass4alias
                 suffix
                 target
                 target_hdr_path
                 target_inputs_ast_path
                 target_libs_ast
                 target_libs_ast_path
                 target_o_path
                 target_src_path
                 target_srcs_ast
                 target_srcs_ast_path
                 use_abs_path
                 var
                 vars
                 verbose_exec
                 which
                 yaml_parse
 );
use Cwd;
use File::Spec;
use Fcntl qw(:DEFAULT :flock);

my $show_stat_info = 0;
my $global_should_echo = 0;

my $gbl_lang_info = &macro_lang_info();

my ($id,  $mid,  $bid,  $tid,
   $rid, $rmid, $rbid, $rtid) = &ident_regex();

my $ENCODED_COMMENT_BEGIN = 'ENCODEDCOMMENTBEGIN';
my $ENCODED_COMMENT_END =   'ENCODEDCOMMENTEND';

my $ENCODED_STRING_BEGIN = 'ENCODEDSTRINGBEGIN';
my $ENCODED_STRING_END =   'ENCODEDSTRINGEND';

my $int_klass4alias = { # should not contain dash or underscore
  'i8'     => 'int8',
  'u8'     => 'uint8',
  'i16'    => 'int16',
  'u16'    => 'uint16',
  'i32'    => 'int32',
  'u32'    => 'uint32',
  'i64'    => 'int64',
  'u64'    => 'uint64',
  'iptr'   => 'intptr',
  'uptr'   => 'uintptr',
  'imax'   => 'intmax',
  'umax'   => 'uintmax',

  'ifst8'  => 'int-fast8',
  'ufst8'  => 'uint-fast8',
  'ifst16' => 'int-fast16',
  'ufst16' => 'uint-fast16',
  'ifst32' => 'int-fast32',
  'ufst32' => 'uint-fast32',
  'ifst64' => 'int-fast64',
  'ufst64' => 'uint-fast64',

  'ilst8'  => 'int-least8',
  'ulst8'  => 'uint-least8',
  'ilst16' => 'int-least16',
  'ulst16' => 'uint-least16',
  'ilst32' => 'int-least32',
  'ulst32' => 'uint-least32',
  'ilst64' => 'int-least64',
  'ulst64' => 'uint-least64',

  # ---

   'int' =>  'int32', # hackhack
  'uint' => 'uint32', # hackhack

   'char' =>  'char8',
  'uchar' => 'uchar8',
  'schar' => 'schar8',

   'bool' => 'boole',
};
sub int_klass4alias {
  my ($alias) = @_;
  my $klass = $$int_klass4alias{$alias};
  $klass = $alias if ! $klass;
  return $klass;
}
my $str_klass4alias = { # should not contain dash or underscore
  #'str' => 'str8',
};
sub str_klass4alias {
  my ($alias) = @_;
  my $klass = $$str_klass4alias{$alias};
  $klass = $alias if ! $klass;
  return $klass;
}
my $root_cmd;
sub set_root_cmd {
  my ($tbl) = @_;
  $root_cmd = $tbl;
}
sub root_cmd {
  return $root_cmd;
}
sub macro_lang_info {
  my ($lang_info_file) = @_;
  if (! $lang_info_file) {
    if ($ENV{'DK_LANG_INFO_FILE'}) {
      $lang_info_file = $ENV{'DK_LANG_INFO_FILE'};
    } else {
      my $dakota_home = &dakota_home();
      $lang_info_file = "$dakota_home/lib/dakota/macro-lang-dakota.json";
    }
  }
  my $lang_info = &do_json($lang_info_file) or die "&do_json(\"$lang_info_file\") failed: $!\n";
  $$lang_info{'-balenced-'} = &macro_balenced_info($$lang_info{'balenced'});
  return $lang_info;
}
sub macro_balenced_info {
  my ($lang_balenced_info) = @_;
  my $balenced_info = {};
  $$balenced_info{'open'} = {};
  $$balenced_info{'close'} = {};
  $$balenced_info{'open-for-close'} = {};
  $$balenced_info{'close-for-open'} = {};
  $$balenced_info{'sep'} = {};
  my $keys = [keys %$lang_balenced_info];
  for my $key (@$keys) {
    if ($$lang_balenced_info{$key}{'sep'}) {
      foreach my $sep (keys %{$$lang_balenced_info{$key}{'sep'}}) { # normally something like , and ;
        $$balenced_info{'sep'}{$sep} = 1;
      }
    }
    if ($$lang_balenced_info{$key}{'open'} && $$lang_balenced_info{$key}{'open'}) {
      foreach my $open_token (keys %{$$lang_balenced_info{$key}{'open'}}) {
        foreach my $close_token (keys %{$$lang_balenced_info{$key}{'close'}}) {
          $$balenced_info{'open'}{$open_token} = 1;
          $$balenced_info{'close'}{$close_token} = 1;
          $$balenced_info{'open-for-close'}{$close_token}{$open_token} = 1;
          $$balenced_info{'close-for-open'}{$open_token} = $close_token;
        }
      }
    }
  }
  return $balenced_info;
}
sub at {
  my ($tbl, $key) = @_;
  if (exists $$tbl{$key} && defined $$tbl{$key}) {
    return $$tbl{$key};
  }
  return undef;
}
sub ct {
  my ($seq) = @_;
  my $string = join('', @$seq);
  return $string;
}
sub concat3 {
  my ($s1, $s2, $s3) = @_;
  return "$s1$s2$s3";
}
sub concat5 {
  my ($s1, $s2, $s3, $s4, $s5) = @_;
  return "$s1$s2$s3$s4$s5";
}
sub encode_strings5 {
  my ($s1, $s2, $s3, $s4, $s5) = @_;
  return &concat5($s1, $s2, unpack('H*', $s3), $s4, $s5);
}
sub encode_comments3 {
  my ($s1, $s2, $s3) = @_;
  return &concat5($s1, $ENCODED_COMMENT_BEGIN, unpack('H*', $s2), $ENCODED_COMMENT_END, $s3);
}
sub encode_strings1 {
  my ($s) = @_;
  $s =~ m/^(.)(.*?)(.)$/;
  my ($s1, $s2, $s3) = ($1, $2, $3);
  return &encode_strings5($s1, $ENCODED_STRING_BEGIN, $s2, $ENCODED_STRING_END, $s3);
}
sub remove_non_newlines {
  my ($str) = @_;
  my $result = $str;
  $result =~ s|[^\n]+||gs;
  return $result;
}
sub tear {
  my ($filestr) = @_;
  my $tbl = { 'src' => '', 'src-comments' => '' };
  foreach my $line (split /\n/, $filestr) {
    if ($line =~ m=^(.*?)(/\*.*\*/|//.*)?$=m) {
      $$tbl{'src'} .= $1 . $nl;
      if ($2) {
        $$tbl{'src-comments'} .= $2;
      }
      $$tbl{'src-comments'} .= $nl;
    } else {
      die "$!";
    }
  }
  return $tbl;
}
sub mend {
  my ($filestr_ref, $tbl) = @_;
  my $result = '';
  my $src_lines = [split /\n/, $$filestr_ref];
  my $src_comment_lines = [split /\n/, $$tbl{'src-comments'}];
  #if (scalar(@$src_lines) != scalar(@$src_comment_lines) ) {
  #  print "warning: mend: src-lines = " . scalar(@$src_lines) . ", src-comment-lines = " . scalar(@$src_comment_lines) . $nl;
  #}
  for (my $i = 0; $i < scalar(@$src_lines); $i++) {
    $result .= $$src_lines[$i];
    if (defined $$src_comment_lines[$i]) {
      $result .= $$src_comment_lines[$i];
    }
    $result .= $nl;
  }
  return $result;
}
sub encode_comments {
  my ($filestr_ref) = @_;
  my $tbl = &tear($$filestr_ref);
  $$filestr_ref = $$tbl{'src'};
  return $tbl;
}
sub encode_strings {
  my ($filestr_ref) = @_;
  my $dqstr = &dqstr_regex();
  my $h =  &header_file_regex();
  my $sqstr = &sqstr_regex();
  $$filestr_ref =~ s|($dqstr)|&encode_strings1($1)|eg;
  $$filestr_ref =~ s|(#\s+\w+\s*)(<$h+>)|$1 . &encode_strings1($2)|eg;
  $$filestr_ref =~ s|($sqstr)|&encode_strings1($1)|eg;
}
sub decode_comments {
  my ($filestr_ref, $tbl) = @_;
  $$filestr_ref = &mend($filestr_ref, $tbl);
}
sub decode_strings {
  my ($filestr_ref) = @_;
  $$filestr_ref =~ s{$ENCODED_STRING_BEGIN([A-Za-z0-9]*)$ENCODED_STRING_END}{pack('H*',$1)}gseo;
}
# method-ident: allow end in ! or ?
# symbol-ident: allow end in ! or ? and allow . or : or / but never as first nor last char

# -       only interior char (never first nor last)
# .  x2e  only interior char (never first nor last)
# /  x2f  only interior char (never first nor last)
# :  x3a  only interior char (never first nor last)
#
# !  x21  only as last char
# ?  x3f  only as last char

sub remove_extra_whitespace {
  my ($str) = @_;
  $str =~ s/(\w)\s+(\w)/$1__WHITESPACE__$2/g;
  $str =~ s/(:)\s+(::)/$1__WHITESPACE__$2/g;
  $str =~ s/(&)\s+(&&)/$1__WHITESPACE__$2/g;
  $str =~ s/(<)\s+(<<)/$1__WHITESPACE__$2/g;
  $str =~ s/(>)\s+(>>)/$1__WHITESPACE__$2/g;
  $str =~ s/(-)\s+(--)/$1__WHITESPACE__$2/g;
  $str =~ s/(\+)\s+(\+\+)/$1__WHITESPACE__$2/g;
  $str =~ s/(\|)\s+(\|\|)/$1__WHITESPACE__$2/g;
  $str =~ s/\s+//g;
  $str =~ s/__WHITESPACE__/ /g;
  if (0) {
    $str =~ s/\s*->\s*/ -> /g;
    $str =~ s/\s*,\s*/, /g;
  }
  return $str;
}
# oct: 0[0-7]
# hex: 0x
# bin: 0b
sub int_from_str {
  my ($str) = @_;
  my $int = $str;
  my $chars = [split(//, $str)];
  if ('0' eq $$chars[0] && 1 < scalar @$chars) {
    if ($$chars[1] =~ /[0-7]|(x|X)|(b|B)/) {
      $int = oct($str);
    } else {
      die;
    }
  }
  return $int;
}
sub needs_hex_encoding {
  my ($str) = @_;
  $str =~ s/^#//;
  my $k = qr/[\w-]/;
  foreach my $char (split(//, $str)) {
    if ($char !~ m/$k/) {
      return 1;
    }
  }
  return 0;
}
sub rand_str {
  my ($len, $alphabet) = @_;
  if (!$len) {
    $len = 16;
  }
  if (!$alphabet) {
    $alphabet = ["A".."Z", "a".."z", "0".."9"];
  }
  my $str = '';
  $str .= $$alphabet[rand @$alphabet] for 1..$len;
  return $str;
}
sub is_debug {
  return 0;
}
sub is_method_ident {
  my ($str) = @_;
  if ($str =~ m/^$mid$/) {
    return 0;
  } else {
    return 1;
  }
}
sub as_literal_symbol_interior {
  my ($str) = @_;
  $str =~ s/^#\|(.*)\|$/#$1/; # strip only the framing | while leave leading #
  $str =~ s/^#//; # now remove leading #
  $str =~ s/\\"/"/g;
  #$str = &dqstr_pre_ident($str);
  return $str;
}
sub as_literal_symbol {
  my ($str) = @_;
  $str = &as_literal_symbol_interior($str);
  my $result;
  if (&is_method_ident($str)) {
    $result = '#|' . $str . '|'
  } else {
    $result = '#' . $str;
  }
  return $result;
}
my $legal_last_chars = { '?' => 'Q',
                         '!' => 'J' };
# swap I and U
# swap G and O
my $alphabet = [split(//, "ABCDEFOHUJKLMNGPQRSTIVWXYZ")];
my $alphabet_len = scalar @$alphabet;
sub encode_char {
  my ($char) = @_;
  my $val = $$legal_last_chars{$char};
  if ($val) {
    return $val;
  }
  #return sprintf("%02x", ord($char));
  return $$alphabet[ord($char) % $alphabet_len];
}
# #|left "and" right|
# #"left \"and\" right"
#
# #|up \| down|
# #"up | down"
sub dk_mangle {
  my ($symbol) = @_;
  &decode_strings(\$symbol);
  # remove \ preceeding |
  $symbol =~ s/\\\|/\|/g;
  my $fix = 0;
  my $fix_str;

  if ($fix) {
    # prevent the (-) from -> to be converted to (_)
    $fix_str = &rand_str();
    $symbol =~ s/->/$fix_str/g;
  }
  # swap underscore (_) with dash (-)
  my $rand_str = &rand_str();
  $symbol =~ s/_/$rand_str/g;
  $symbol =~ s/-/_/g;
  $symbol =~ s/$rand_str/-/g;

  if ($fix) {
    $symbol =~ s/$fix_str/->/g;
  }
  my $ident_symbol = [];

  my $chars = [split //, $symbol];
  my $num_encoded = 0;
  my $last_encoded_char;

  foreach my $char (@$chars) {
    my $part;
    if ($char =~ /\w/) {
      $part = $char;
    } else {
      $part = &encode_char($char);
      $last_encoded_char = $char;
      $num_encoded++;
    }
    &add_last($ident_symbol, $part);
  }
  if ($num_encoded && ! (1 == $num_encoded && $$legal_last_chars{$last_encoded_char})) {
    if (0) {
      &add_first($ident_symbol, '_');
      &add_last( $ident_symbol, '_');
    } else {
      &add_first($ident_symbol, &encode_char('|'));
      &add_last( $ident_symbol, &encode_char('|'));
    }
  }
  &add_first($ident_symbol, '_');
  &add_last( $ident_symbol, '_');
  my $value = &ct($ident_symbol);
  return $value;
}
sub dk_mangle_seq {
  my ($seq) = @_;
  my $ident_symbols = [map { &dk_mangle($_) } @$seq];
  return &ct($ident_symbols);
}
sub ann {
  my ($file, $line, $should_skip) = @_;
  my $string = '';
  return $string if defined $should_skip && 0 != $should_skip;
  if (1) {
    $file =~ s|^.*/(.+)|$1|;
    $string = " // $file:$line:";
  }
  return $string;
}
sub pann {
  my ($file, $line) = @_;
  my $string = '';
  if (0) {
    $string = &ann($file, $line);
  }
  return $string;
}
sub kw_arg_placeholders {
  return { 'default' => '{}', 'nodefault' => '{~}' };
}
# literal symbol/keyword grammar: ^_* ... _*$
# interior only chars: - : . /
# last only chars: ? !
sub ident_regex {
  my $id =  $$gbl_lang_info{'constraint-regex'}{'?ident'};
  my $mid = qr/[_a-zA-Z](?:(?:[_a-zA-Z0-9-]*[_a-zA-Z0-9\?\!])|(?:[\?\!]))?/x; # method ident
 #my $bid = qr/[_a-zA-Z](?:[_a-zA-Z0-9-]*[_a-zA-Z0-9\?]  )|(?:[\?]  )/x; # bool   ident
  my $bid = qr/[\w-]+\??/x; # bool   ident
  my $tid = $$gbl_lang_info{'constraint-regex'}{'?type-ident'};

  my $sro =  qr/::/;
  my $rid =  qr/$sro?(?:$id$sro)*$id/;
  my $rmid = qr/$sro?(?:$id$sro)*$mid/;
  my $rbid = qr/$sro?(?:$id$sro)*$bid/;
  my $rtid = qr/$sro?(?:$id$sro)*$tid/;
  my $uint =  qr/0[xX][0-9a-fA-F]+|0[bB][01]+|0[0-7]+|\d+/;
 #my $qtid = qr/(?:$sro?$tid)|(?:$id$sro(?:$id$sro)*$tid)/;

  return ( $id,  $mid,  $bid,  $tid,
          $rid, $rmid, $rbid, $rtid, $uint);
}
my $implicit_metaklass_stmts = qr/( *klass\s+(((klass|trait|superklass)\s+[\w:-]+)|(slots|method|func)).*)/s;
sub rewrite_metaklass_stmts {
  my ($stmts) = @_;
  my $result = $stmts;
  $result =~ s/klass\s+(klass     \s+[\w:-]+)/$1/x;
  $result =~ s/klass\s+(trait     \s+[\w:-]+)/$1/gx;
  $result =~ s/klass\s+(superklass\s+[\w:-]+)/$1/x;
  $result =~ s/klass\s+(slots)               /$1/x;
  $result =~ s/klass\s+(method)              /$1/gx;
  $result =~ s/klass\s+(func)                /$1/gx;
  return $result;
}
sub rewrite_stencils_replacement {
  my ($construct, $name_list, $block) = @_;
  my $result = '';
  my $names = [split(/\s*,\s*/, $name_list)];
  foreach my $name (@$names) {
    $result .= "$construct $name $block\n";
  }
  return $result;
}
sub rewrite_stencils {
  my ($filestr_ref) = @_;
  $$filestr_ref =~ s/(klass|trait)(\s+)($rid)(\s*),(\s*{)/$1$2$3$4$5/gms; # removing trailing comma (,)
  $$filestr_ref =~ s/(klass|trait)\s+($rid(\s*,\s*$rid)+(\s*,)?)\s*($main::block)/&rewrite_stencils_replacement($1, $2, $5)/egms;
}
sub rewrite_klass_defn_with_implicit_metaklass_defn_replacement {
  my ($s1, $klass_name, $s2, $body) = @_;
  my $result;
  if ($body =~ s/$implicit_metaklass_stmts/"} klass $klass_name-klass { superklass klass;\n" . &rewrite_metaklass_stmts($1)/egs) {
    $result = "klass$s1$klass_name$s2\{ klass $klass_name-klass;" . $body . "}";
  } else {
    $result =  "klass$s1$klass_name$s2\{" . $body . "}";
  }
  return $result;
}
sub rewrite_klass_defn_with_implicit_metaklass_defn {
  my ($filestr_ref) = @_;
  $$filestr_ref =~ s/^klass(\s+)([\w:-]+)(\s*)\{(\s*$main::block_in\s*)\}/&rewrite_klass_defn_with_implicit_metaklass_defn_replacement($1, $2, $3, $4)/egms;
}
sub rewrite_scoped_int_uint_replacement1 {
  my ($name, $rhs) = @_;
  $name = &int_klass4alias($name);
  return "$name$rhs";
}
sub rewrite_scoped_int_uint_replacement2 {
  my ($lhs, $name) = @_;
  $name = &int_klass4alias($name);
  return "$lhs$name";
}
sub rewrite_scoped_int_uint {
  my ($filestr_ref) = @_;
  # int :: => int64 ::
  # klass int => klass int64
  $$filestr_ref =~ s/($id)(\s*::)/&rewrite_scoped_int_uint_replacement1($1, $2)/ge;
  $$filestr_ref =~ s/(klass\s+)($id)/&rewrite_scoped_int_uint_replacement2($1, $2)/ge;
}
sub header_file_regex {
  return qr|[/.\w-]|;
}
sub method_sig_type_regex {
  return qr/object-t|slots-t|slots-t\s*\*/;
}
my $method_sig_type = &method_sig_type_regex();
sub method_sig_regex {
  return qr/(va::)?$mid(\($method_sig_type?\))?/;
}
sub dqstr_strip {
  my ($string) = @_;
  $string =~ s/^"(.*)"$/$1/; # remove leading/trailing double quotes
  return $string;
}
sub dqstr_pre_ident {
  my ($string) = @_;
  $string =~ s/\\"/"/g;
  $string =~ s/\\\\/\\/g;
  return $string;
}
sub dqstr_regex {
  return $$gbl_lang_info{'constraint-regex'}{'?dquote-str'};
}
sub sqstr_regex {
  return $$gbl_lang_info{'constraint-regex'}{'?squote-str'};
}
sub path_split {
  my ($path) = @_;
  my $parts = [split /\//, $path];
  my $name = &remove_last($parts);
  my $dir = join '/', @$parts;
  $dir = '.' if $dir eq "";
  return ($dir, $name);
}
sub dirname {
  my ($path) = @_;
  my ($dir, $name) = &path_split($path);
  return $dir;
}
sub basename {
  my ($path, $suffix) = @_;
  my ($dir, $name) = &path_split($path);
  if ($suffix) {
    $name =~ s/$suffix$//;
  }
  return $name;
}
sub verbose_exec {
  my ($argv) = @_;
  if ($ENV{'DAKOTA_VERBOSE'}) {
    print join(' ', @$argv) . $nl;
  }
  my $exit_val = system(@$argv);
  if ($exit_val) {
    print STDERR "error: cd " . &cwd() . " && " . join(' ', @$argv) . $nl;
  }
  return $exit_val;
}
sub make_dir {
  my ($path, $should_echo) = @_;
  $path = join('/', @$path) if &is_array($path);
  if (! -e $path) {
    if ($should_echo) {
      print STDERR $0 . ': info: make_dir(' . $path . ')' . $nl;
    }
    my $cmd = ['mkdir', '-p', $path];
    my $exit_val = &verbose_exec($cmd);
    exit 1 if $exit_val;
  }
}
sub make_dirname {
  my ($path, $should_echo) = @_;
  my $dirname = &dirname($path);
  if ('.' ne $dirname) {
    &make_dir($dirname, $should_echo);
  } elsif (0) {
    print STDERR $0 . ': warning: skipping: make_dirname(' . $path . ')' . $nl;
  }
}
my $vars = {};
sub vars {
  return $vars;
}
sub check_set_env_var {
  my ($key, $val, $force) = @_;
  $force = 1; # hackhack
  die if ! $key;
  if ($ENV{$key} && ! $force) {
    die "env var $key=$ENV{$key}, cannot set to $val\n" if $ENV{$key} ne $val;
  } else {
    $ENV{$key} = $val;
    $$vars{$key} = $val;
  }
}
sub run_cmd {
  my ($cmd) = @_;
  my $result = `$cmd` or die "$!";
  $result =~ s/\s*$//; #chomp $result;
  return $result;
}
sub build_file_name {
  my $cmd = "$gbl_prefix/lib/util.sh build-file-name";
  return &run_cmd($cmd);
}
sub rel_intmd_dir {
  my $cmd = "$gbl_prefix/lib/util.sh rel-intmd-dir";
  return &run_cmd($cmd);
}
sub default_stage_prefix_dir_from_source_dir {
  my ($source_dir) = @_; # $source_dir is optional
  my $cmd = "$gbl_prefix/lib/util.sh default-stage-prefix-dir-from-source-dir $source_dir";
  return &run_cmd($cmd);
}
# build_dir
# source_dir
# current_source_dir
# =>
# intmd_dir =         intmd_dir
# current_build_dir = current_build_dir_from_current_source_dir $source_dir $current_source_dir
# current_intmd_dir = current_intmd_dir_from_current_source_dir $source_dir $current_source_dir
sub intmd_dir {
  my $cmd = "$gbl_prefix/lib/util.sh intmd-dir";
  return &run_cmd($cmd);
}
sub build_dir {
  my $cmd = "$gbl_prefix/lib/util.sh build-dir";
  return &run_cmd($cmd);
}
sub current_intmd_dir_from_current_source_dir {
  my ($current_source_dir) = @_;
  my $cmd = "$gbl_prefix/lib/util.sh current-intmd-dir-from-current-source-dir $current_source_dir";
  return &run_cmd($cmd);
}
sub current_build_dir_from_current_source_dir {
  my ($current_source_dir) = @_;
  my $cmd = "$gbl_prefix/lib/util.sh current-build-dir-from-current-source-dir $current_source_dir";
  return &run_cmd($cmd);
}
sub builder_jobs {
  my $cmd = "$gbl_prefix/lib/util.sh builder-jobs";
  return &run_cmd($cmd);
}
sub which {
  my ($cmd) = @_;
  my $result = `which $cmd`;
  if ($? != 0) {
    $result = $cmd;
  }
  $result =~ s/\n$//;
  return $result;
}
sub find_cmd {
  my ($cmd, $var) = @_;
  if ($var && $ENV{$var}) {
    $cmd = $ENV{$var}
  } else {
    $cmd = &which($cmd);
  }
  return $cmd;
}
sub dirs {
  my $dirs = {};
  $$dirs{'current_source_dir'} = $ENV{'current_source_dir'};
  $$dirs{'source_dir'} =         $ENV{'source_dir'};
  $$dirs{'build_dir'} =          $ENV{'build_dir'};
  if ($$dirs{'current_source_dir'} &&
      $$dirs{'source_dir'} &&
      $$dirs{'build_dir'}) {
    return $dirs;
  }
  return undef;
}
sub set_env_vars_core {
  my ($tbl, $force) = @_;
  while (my ($key, $val) = each (%$tbl)) {
    &check_set_env_var($key, $val, $force);
  }
  if (my $dirs = &dirs()) {
    &set_dirs($$dirs{'current_source_dir'},
              $$dirs{'source_dir'},
              $$dirs{'build_dir'},
              $force);
  }
}
sub set_env_vars {
  my ($kvs) = @_;
  my $vars = {};
  foreach my $kv (@$kvs) {
    $kv =~ /^([\w-]+)=(.*)$/;
    my ($key, $val) = ($1, $2);
    $$vars{$key} = $val;
  }
  &set_env_vars_core($vars);
}
sub set_dirs {
  my ($current_source_dir, $source_dir, $build_dir, $force) = @_;
  my $rel_current_source_dir = $current_source_dir =~ s#^$source_dir/##r;
  my $current_build_dir = &current_build_dir_from_current_source_dir($current_source_dir);
  my $intmd_dir = &intmd_dir();
  my $current_intmd_dir = &current_intmd_dir_from_current_source_dir($current_source_dir);
  &check_set_env_var('current_build_dir',  $current_build_dir, $force);
  &check_set_env_var('current_intmd_dir',  $current_intmd_dir, $force);
  &check_set_env_var('intmd_dir',          $intmd_dir, $force);
  return ($current_intmd_dir, $current_build_dir);
}
sub cxx {
  my $cxx = $ENV{'cxx'};
  die if ! $cxx;
  return $cxx;
}
sub current_intmd_dir {
  my $dir = $ENV{'current_intmd_dir'};
  die if ! $dir;
  return $dir;
}
sub current_source_dir {
  my $dir = $ENV{'current_source_dir'};
  die if ! $dir;
  return $dir;
}
sub current_build_dir {
  my $dir = $ENV{'current_build_dir'};
  die if ! $dir;
  return $dir;
}
sub source_dir {
  my $dir = $ENV{'source_dir'};
  die if ! $dir;
  return $dir;
}
sub bin_dir {
  my $dir = $ENV{'bin_dir'};
  die if ! $dir;
  return $dir;
}
sub lib_dir {
  my $dir = $ENV{'lib_dir'};
  die if ! $dir;
  return $dir;
}
sub stage_prefix_dir {
  my $source_dir_bname = &basename(&source_dir());
  my $dir = $ENV{'stage_prefix_dir'} ||= "$ENV{'HOME'}/opt-stage/$source_dir_bname";
  die if ! $dir;
  return $dir;
}
sub stage_bin_dir {
  my $dir = $ENV{'stage_bin_dir'};
  if (! $dir) {
    my $pdir = &stage_prefix_dir();
    $dir = "$pdir/bin";
  }
  return $dir;
}
sub stage_lib_dir {
  my $dir = $ENV{'stage_lib_dir'};
  if (! $dir) {
    my $pdir = &stage_prefix_dir();
    $dir = "$pdir/lib";
  }
  return $dir;
}
sub stage_include_dir {
  my $dir = $ENV{'stage_include_dir'};
  if (! $dir) {
    my $pdir = &stage_prefix_dir();
    $dir = "$pdir/include";
  }
  return $dir;
}
sub target {
  my $name = $ENV{'target'};
  die if ! $name;
  return $name;
}
sub rel_target_dir_name {
  return 'z';
}
sub parts_bname {
  return 'build.parts';
}
sub parts_path {
  my $path = &current_target_intmd_dir() . '/' . &parts_bname();
  return $path;
}
sub current_target_intmd_dir {
  my $dir = &current_intmd_dir() . '/' . &target() . '.d';
  return $dir;
}
sub current_target_build_dir {
  my $dir = &current_build_dir() . '/' . &target() . '.d';
  return $dir;
}
sub current_target_x_intmd_dir {
  my $dir = &current_target_intmd_dir() . '/' . &rel_target_dir_name(); # /z/
  return $dir;
}
sub current_target_x_build_dir {
  my $dir = &current_target_build_dir() . '/' . &rel_target_dir_name(); # /z/
  return $dir;
}
sub rel_target_hdr_path {
  my $rel_path = &rel_target_dir_name() . '/target' . $h_ext; # /z/
  return $rel_path;
}
sub target_hdr_path {
  my $path = &current_target_x_intmd_dir() . '/target' . $h_ext;
  return $path;
}
sub target_src_path {
  my $path = &current_target_x_intmd_dir() . '/target' . $cc_ext;
  return $path;
}
sub target_o_path {
  my $path = &current_target_x_build_dir() . '/target' . $cc_ext . $o_ext;
  return $path;
}
sub target_srcs_ast_path {
  my $path = &current_target_intmd_dir() . '/srcs.ast';
  return $path;
}
sub target_libs_ast_path {
  my $path = &current_target_intmd_dir() . '/libs.ast';
  return $path;
}
sub target_inputs_ast_path {
  my $path = &current_target_intmd_dir() . '/inputs.ast';
  return $path;
}
sub build_cmk_path {
  my ($name) = @_;
  $name = &build_file_name() if ! $name;
  #assert($name =~ /\.cmk$/);
  my $path = &current_source_dir() . '/' . $name;
  return $path;
}
sub build_mk_path {
  my ($name) = @_;
  $name = 'build.mk' if ! $name;
  #assert($name =~ /\.mk$/);
  my $path = &current_intmd_dir() . '/' . $name;
  return $path;
}
sub build_ninja_path {
  my ($name) = @_;
  $name = 'build.ninja' if ! $name;
  #assert($name =~ /\.ninja$/);
  my $path = &current_intmd_dir() . '/' . $name;
  return $path;
}
sub build_dot_path {
  my ($name) = @_;
  $name = 'build.dot' if ! $name;
  #assert($name =~ /\.dot$/);
  my $path = &current_intmd_dir() . '/' . $name;
  return $path;
}
sub build_rules_path {
  my ($name) = @_;
  $name = 'build.rules.json' if ! $name;
  #assert($name =~ /\.rules.json$/);
  my $path = &current_intmd_dir() . '/' . $name;
  return $path;
}
sub is_o_path {
  my ($path) = @_;
  return $path =~ /\.o$/;
}
sub is_target_o_path {
  my ($path) = @_;
  return $path eq &target_o_path();
}
sub is_exe_path {
  my ($path) = @_;
  my $state = 1;
  if ($exe_suffix) {
    $state = 0 if $path !~ /$exe_suffix$/;
  } else {
    $state = 0 if $path =~ /\..+$/;
  }
  return $state;
}

# 1. cmd line
# 2. environment
# 3. config file
# 4. compile-time default

sub var {
  my ($lhs, $default_rhs) = @_;
  my $result = &var_array($gbl_platform_tbl, $lhs, $default_rhs);
  if (&is_array($result)) {
    $result = join(' ', @$result);
  }
  return $result;
}
sub pad_var {
  my ($lhs, $default_rhs) = @_;
  my $result = &var($lhs, $default_rhs);
  if ($result) { $result = ' ' . $result; }
  return $result;
}
sub dump_env {
  my ($line) = @_;
  my $current_vars = {
    'current_source_dir' => $ENV{'current_source_dir'},
    'current_intmd_dir' =>  $ENV{'current_intmd_dir'},
    'source_dir' =>         $ENV{'source_dir'},
    'build_dir' =>          $ENV{'build_dir'},
  };
  print STDERR "line: $line " . &Dumper($current_vars);
}

sub var_array {
  my ($platform_tbl, $lhs, $default_rhs) = @_;
  my $result;
  my $env_rhs = $ENV{$lhs};
  my $platform_rhs = $$platform_tbl{$lhs};
  if ($env_rhs) {
    $result = $env_rhs;
  } elsif ($platform_rhs) {
    $result = $platform_rhs;
  } else {
    $result = $default_rhs;
  }
  die if !defined $result || $result =~ /^\s+$/; # die if undefined or only whitespace
  die if 'HASH' eq ref($result);
  return $result;
}
sub str_from_seq {
  my ($seq) = @_;
  my $str = &remove_extra_whitespace(join(' ', @$seq)); # str is lexically correct
  return $str;
}
sub param_types_str {
  my ($param_types) = @_;
  my $strs = [];
  foreach my $type (@$param_types) {
    &add_last($strs, &str_from_seq($type));
  }
  my $result = join(',', @$strs);
  return $result;
}
# [ name, fixed-param-types ]
# [ [ x :: signal ], [ object-t, int64-t, ... ] ]
sub kw_args_method_sig {
  my ($method) = @_;
  my $len = 0;
  if (0) {
  } elsif ($len = &num_kw_arg_names($method)) {
  } elsif ($len = &num_kw_args($method)) {
  }
  my $param_types = &deep_copy($$method{'param-types'});
  my $offset = scalar @$param_types - $len;
  my $kw_args = [splice @$param_types, 0, $offset];
  &add_last($kw_args, ['...']);
  return ($$method{'name'}, $kw_args);
}
my $target_srcs_ast;
sub target_srcs_ast {
  if (! $target_srcs_ast) {
    $target_srcs_ast = &scalar_from_file(&target_srcs_ast_path());
  }
  return $target_srcs_ast;
}
my $target_libs_ast;
sub target_libs_ast {
  if (! $target_libs_ast) {
    $target_libs_ast = &scalar_from_file(&target_libs_ast_path());
  }
  return $target_libs_ast;
}
my $global_is_target = undef; # <klass>--klasses.{h,cc} vs lib/libdakota--klasses.{h,cc}
my $global_is_defn = undef; # klass decl vs defn
my $global_suffix = undef;

sub set_src_decl {
  my ($path) = @_;
  $global_is_target =   0;
  $global_is_defn = 0;
  $global_suffix = $h_ext;
}
sub set_src_defn {
  my ($path) = @_;
  $global_is_target =   0;
  $global_is_defn = 1;
  $global_suffix = $cc_ext;
}
sub set_target_decl {
  my ($path) = @_;
  $global_is_target =   1;
  $global_is_defn = 0;
  $global_suffix = $h_ext;
}
sub set_target_defn {
  my ($path) = @_;
  $global_is_target =   1;
  $global_is_defn = 1;
  $global_suffix = $cc_ext;
}
sub suffix {
  return $global_suffix
}
sub is_src_decl {
  if (!$global_is_target && !$global_is_defn) {
    return 1;
  } else {
    return 0;
  }
}
sub is_src_defn {
  if (!$global_is_target && $global_is_defn) {
    return 1;
  } else {
    return 0;
  }
}
sub is_target_decl {
  if ($global_is_target && !$global_is_defn) {
    return 1;
  } else {
    return 0;
  }
}
sub is_target_defn {
  if ($global_is_target && $global_is_defn) {
    return 1;
  } else {
    return 0;
  }
}
sub is_src {
  if (!$global_is_target) {
    return 1;
  } else {
    return 0;
  }
}
sub is_target {
  if ($global_is_target) {
    return 1;
  } else {
    return 0;
  }
}
sub is_decl {
  if (!$global_is_defn) {
    return 1;
  } else {
    return 0;
  }
}
sub is_exported {
  my ($method) = @_;
  if (exists $$method{'is-exported'} && $$method{'is-exported'}) {
    return 1;
  } else {
    return 0;
  }
}
sub is_slots {
  my ($method) = @_;
  if ('object-t' ne $$method{'param-types'}[0][0]) {
    return 1;
  } else {
    return 0;
  }
}
my $box_type_set = {
  'const slots-t*' => 1,
  'const slots-t&' => 1,
  'slots-t*' => 1,
  'slots-t&' => 1,
  'slots-t'  => 1,
};
sub is_box_type {
  my ($type_seq) = @_;
  my $result;
  my $type_str = &remove_extra_whitespace(join(' ', @$type_seq));

  if ($$box_type_set{$type_str}) {
    $result = 1;
  } else {
    $result = 0;
  }
  return $result;
}
sub is_super {
  my ($generic) = @_;
  if ('super-t' eq $$generic{'param-types'}[0][0]) {
    return 1;
  }
  return 0;
}
sub is_array_type {
  my ($type) = @_;
  my $is_array_type = 0;

  if ($type && $type =~ m|\[.*?\]$|) {
    $is_array_type = 1;
  }
  return $is_array_type;
}
sub is_array {
  my ($ref) = @_;
  my $state;
  if ('ARRAY' eq ref($ref)) {
    $state = 1;
  } else {
    $state = 0;
  }
  return $state;
}
sub is_va {
  my ($method) = @_;
  if ($$method{'param-types'} && $$method{'param-types'}[-1] && $$method{'param-types'}[-1][0] eq 'va-list-t') {
    return 1;
  } else {
    return 0;
  }
}
sub has_va_prefix {
  my ($method) = @_;
  if ('va' eq $$method{'name'}[0]) {
    return 1;
  } else {
    return 0;
  }
}
sub num_kw_arg_names {
  my ($func) = @_;
  return scalar @{$$func{'kw-arg-names'} || []};
}
sub has_kw_arg_names {
  my ($func) = @_;
  return $$func{'kw-arg-names'};
}
sub num_kw_args {
  my ($func) = @_;
  return scalar @{$$func{'kw-args'} || []};
}
sub has_kw_args {
  my ($func) = @_;
  return $$func{'kw-args'};
}
sub is_kw_args_method {
  my ($method) = @_;
  return 1 if &num_kw_args($method);
  return 1 if &num_kw_arg_names($method);
  my ($name, $types) = &kw_args_method_sig($method);
  my $name_str = &str_from_seq($name);
  my $asts = [ &target_srcs_ast(),
               &target_libs_ast() ];
  foreach my $ast (@$asts) {
    if (exists $$ast{'kw-arg-generics'}{$name_str}) {
      return 1;
    }
  }
  return 0
}
sub min { my ($x, $y) = @_; return $x <= $y ? $x : $y; }
sub max { my ($x, $y) = @_; return $x >= $y ? $x : $y; }
sub mtime {
  my ($file) = @_;
  if (! -e $file) {
    return 0;
  }
  my $mtime = 9;
  my $result = (stat ($file))[$mtime];
  return $result;
}
sub path_stat {
  my ($path_db, $path, $text) = @_;
  my $stat;
  if (exists $$path_db{$path}) {
    $stat = $$path_db{$path};
  } else {
    if ($show_stat_info) {
      print "STAT $path, text=$text\n";
    }
    @$stat{qw(dev inode mode nlink uid gid rdev size atime mtime ctime blksize blocks)} = stat($path);
  }
  return $stat;
}
sub digsig {
  my ($filestr) = @_;
  my $sig;
  $sig = &md5_hex($filestr);
  $sig =~ s/^.*?([a-fA-F0-9]+)\s*$/$1/s;
  $sig =~ s/^.*?(........)$/$1/s; # extra trim
  return $sig;
}
sub is_out_of_date {
  my ($infiles, $outfile, $file_db) = @_;
  my $files = &out_of_date($infiles, $outfile, $file_db);
  my $result = scalar @$files;
  return $result;
}
sub out_of_date {
  my ($infiles, $outfile, $file_db) = @_;
  my $result = [];
  $file_db = {} if ! defined $file_db;
  if (!&is_array($infiles)) {
    $infiles = [$infiles];
  }
  my $outfile_stat = &path_stat($file_db, $outfile, '--output');
  if (!$$outfile_stat{'mtime'}) {
    return $infiles;
  }
  foreach my $infile (@$infiles) {
    if (! -e $infile) {
      die "error: missing $infile" . $nl;
    }
    my $infile_stat =  &path_stat($file_db, $infile,  '--inputs');

    if (!$$infile_stat{'mtime'}) {
      die $0 . ': warning: no-such-file: ' . &cwd() . ' / ' . $infile . ' on which ' . $outfile . ' depends' . $nl;
    }
    if ($$outfile_stat{'mtime'} < $$infile_stat{'mtime'}) {
      &add_last($result, $infile);
    }
  }
  return $result;
}
sub flatten {
    my ($a_of_a) = @_;
    my $a = [map {@$_} @$a_of_a];
    return $a;
}
sub use_abs_path {
  return 0;
}
# found at http://linux.seindal.dk/2005/09/09/longest-common-prefix-in-perl
sub longest_common_prefix {
  if (&use_abs_path()) {
    return '/';
  }
  my $path_prefix = shift;
  for (@_) {
    chop $path_prefix while (! /^$path_prefix/);
  }
  return $path_prefix;
}
sub dmp {
  my ($ref) = @_;
  print STDERR &Dumper($ref);
}
sub is_cc_path {
  my ($arg) = @_;
  if ($arg =~ m/$cc_ext$/) {
    return 1;
  } else {
    return 0;
  }
}
sub is_dk_src_path {
  my ($arg) = @_;
  if ($arg =~ m/\.dk$/ ||
      $arg =~ m/\.ctlg(\.\d+)*$/) {
    return 1;
  } else {
    return 0;
  }
}
sub is_ast_path { # ast
  my ($arg) = @_;
  if ($arg =~ m/\.ast$/) {
    return 1;
  } else {
    return 0;
  }
}
sub is_ctlg_path { # ctlg
  my ($arg) = @_;
  if ($arg =~ m/\.ctlg(\.\d+)*$/) {
    return 1;
  } else {
    return 0;
  }
}
sub is_ctlg_ast_path { # ctlg
  my ($arg) = @_;
  if ($arg =~ m/\.ctlg(\.\d+)*\.ast$/) {
    return 1;
  } else {
    return 0;
  }
}
sub is_dk_path {
  my ($arg) = @_;
  if ($arg =~ m/\.dk$/) {
    return 1;
  } else {
    return 0;
  }
}
# linux:
#   libX.so.3.9.4
#   libX.so.3.9
#   libX.so.3
#   libX.so
# darwin:
#   libX.3.9.4.so
#   libX.3.9.so
#   libX.3.so
#   libX.so
sub is_so_path {
  my ($name) = @_;
  # linux and darwin so-regexs are combined
  my $lib_suffix_re = qr/(?:so|dylib)/;
  my $result = $name =~ m=^(.*/)?(lib([.\w-]+))($lib_suffix_re((\.\d+)+)?|((\.\d+)+)?$lib_suffix_re)$=;
  #my $libname = $2 . $lib_suffix;
  return $result;
}
sub normalize_paths {
  my ($in, $key) = @_;
  die if !defined $in;
  if ($key && !exists $$in{$key}) {
    return $in;
  }
  my $items_in = $in;
  if ($key) {
    $items_in = $$in{$key};
  }
  my $items = [map { &canon_path($_) } @$items_in];
  $items = &copy_no_dups($items);
  if ($key) {
    $$in{$key} = $items;
  }
  return $items;
}
my $gbl_col_width = '  ';
sub colin {
  my ($col) = @_;
  my $len = length($col)/length($gbl_col_width);
  #print STDERR "$len" . "++" . $nl;
  $len++;
  my $result = $gbl_col_width x $len;
  return $result;
}
sub colout {
  my ($col) = @_;
  &confess("Aborted because of &colout(0)") if '' eq $col;
  my $len = length($col)/length($gbl_col_width);
  #print STDERR "$len" . "--" . $nl;
  $len--;
  my $result = $gbl_col_width x $len;
  return $result;
}
sub copy_no_dups {
  my ($strs) = @_;
  my $cmd_info = &root_cmd();
  my $str_set = {};
  my $result = [];
  foreach my $str (@$strs) {
    $str = &canon_path($str);
    if (!$$str_set{$str}) {
      &add_last($result, $str);
      $$str_set{$str} = 1;
    } else {
      #printf "$0: warning: removing duplicate $str\n";
    }
  }
  return $result;
}
sub is_abs {
  my ($path) = @_;
  die if !$path;
  my $result = File::Spec->file_name_is_absolute($path);
  return $result;
}
sub realpath {
  my ($path) = @_; # base is optional
  my $result = Cwd::realpath($path);
  return $result;
}
sub prepend_dot_slash {
  my ($path) = @_;
  return $path if &is_abs($path);
  return $path if $path =~ /^\.\//;
  return $path if $path =~ /^\.\.\//;
  $path = './' . $path;
  return $path;
}
sub canonpath {
  my ($path) = @_;
  die if !$path;
  my $result = File::Spec->canonpath($path);
  return $result;
}
sub canon_path {
  my ($path) = @_;
  if ($path) {
    $path =~ s|//+|/|g; # replace multiple /s with single /s
    $path =~ s|/+\./+|/|g; # replace /./s with single /
    $path =~ s|^\./(.+)|$1|g; # remove leading ./
    $path =~ s|(.+)/\.$|$1|g; # remove trailing /.
    $path =~ s|/+$||g; # remove trailing /s
    $path =~ s|/[^/]+/\.\./|/|g; # /xx/../ => /
  }
  return $path;
}
sub split_path {
  my ($path, $ext_re) = @_;
  die if !$path;
  my ($vol, $dir, $name) = File::Spec->splitpath($path);
  $dir = &canon_path($dir);
  my $ext;
  if ($ext_re) {
    $ext = $name =~ s|^.+?($ext_re)$|$1|r;
    $name =~ s|^(.+?)\.$ext_re$|$1|;
  }
  return ($dir, $name, $ext);
}
sub deep_copy {
  my ($ref) = @_;
  my $result = eval &Dumper($ref);
  die if ! $result;
  return $result;
}
sub do_json {
  my ($path) = @_;
  my $result = do "$path" or die "$!: $path";
  return $result;
}
sub remove_name_va_scope {
  my ($method) = @_;
  #die if 'va' ne $$method{'name'}[0];
  #die if 'va-list-t' ne $$method{'param-types'}[-1];
  if (3 == scalar @{$$method{'name'}} && 'va' eq $$method{'name'}[0] && '::' eq $$method{'name'}[1]) {
    &remove_first($$method{'name'});
    &remove_first($$method{'name'});
  }
  else { print "not-va-method: " . &Dumper($method); }
}
sub add_first {
  my $seq = shift @_;
  if (!defined $seq) { die __FILE__, ":", __LINE__, ": error:\n"; }             unshift @$seq, @_; return;
}
sub add_last {
  my $seq = shift @_;
  if (!defined $seq) { die __FILE__, ":", __LINE__, ": error:\n"; }             push    @$seq, @_; return;
}
sub remove_first {
  my ($seq) =           @_; if (!defined $seq) { die __FILE__, ":", __LINE__, ": error:\n"; } my $first = shift   @$seq;           return $first;
}
sub remove_last {
  my ($seq) =           @_; if (!defined $seq) { die __FILE__, ":", __LINE__, ": error:\n"; } my $last =  pop     @$seq;           return $last;
}
sub first {
  my ($seq) = @_; if (!defined $seq) { die __FILE__, ":", __LINE__, ": error:\n"; } my $first = $$seq[0];  return $first;
}
sub last {
  my ($seq) = @_; if (!defined $seq) { die __FILE__, ":", __LINE__, ": error:\n"; } my $last =  $$seq[-1]; return $last;
}
sub replace_first {
  my $seq = shift @_;
  if (!defined $seq) {
    die __FILE__, ":", __LINE__, ": error:\n";
  }
  my $old_first = &remove_first($seq);
  &add_first($seq, @_);
  return $old_first;
}
sub replace_last {
  my $seq = shift @_;
  if (!defined $seq) {
    die __FILE__, ":", __LINE__, ": error:\n";
  }
  my $old_last = &remove_last($seq);
  &add_last($seq, @_);
  return $old_last;
}
sub unwrap_seq {
  my ($seq) = @_;
  $seq =~ s/\s*\n+\s*/ /gms;
  $seq =~ s/\s+/ /gs;
  return $seq;
}
sub _filestr_to_file {
  my ($filestr, $file, $mode) = @_;
  if (! defined $mode) { $mode = '>'; }
  &make_dirname($file);
  #sleep 1;
  open FILE, $mode, $file or die __FILE__, ":", __LINE__, ": error: " . &cwd() . " / " . $file . ": $!" . $nl;
  flock FILE, LOCK_EX or die;
  if ($mode eq '>') { truncate FILE, 0; }
  print FILE $filestr;
  flock FILE, LOCK_UN or die;
  close FILE            or die __FILE__, ":", __LINE__, ": error: " . &cwd() . " / " . $file . ": $!" . $nl;
  #print STDERR "written: $file" . $nl;
}
sub echo_output_path {
  my ($file, $filestr_sig, $filestr) = @_;
  if (1) {
    if (1) {
      print $file . $nl;
    } else {
      if (!$filestr_sig) {
        if (!$filestr) {
          $filestr = &filestr_from_file($file);
        }
        $filestr_sig = &digsig($filestr);
      }
      print $file . '  ' . $filestr_sig . $nl;
    }
  }
}
sub filestr_to_file {
  my ($filestr, $file, $should_echo) = @_;
  my $filestr_sig = &digsig($filestr);
  if (-e $file) {
    my $output_filestr_sig = &digsig(&filestr_from_file($file));
    if ($output_filestr_sig ne $filestr_sig) {
      &_filestr_to_file($filestr, $file);
    } else {
      #print STDERR "not written: $file" . $nl;
    }
  } else {
    &_filestr_to_file($filestr, $file);
  }
  if (0) {
    my $filestr_sig = &digsig($filestr);
    my $file_md5 = "$file.md5";
    &_filestr_to_file($filestr_sig . $nl, $file_md5);
    &echo_output_path($file, $filestr_sig, $filestr) if $should_echo;
  }
}
sub scalar_to_file {
  my ($file, $ref) = @_;
  if (!defined $file) {
    die __FILE__ . ':' . __LINE__ . ": error: scalar_to_file(\$file, \$ref): \$file undefined" . $nl;
  }
  if (!defined $ref) {
    die __FILE__ . ':' . __LINE__ . ": error: scalar_to_file(\"$file\", \$ref): \$ref undefined" . $nl;
  }
  my $refstr = '';
  if (scalar keys %$ref) {
    $refstr = '# -*- mode: perl -*-' . $nl . &Dumper($ref);
  }
  &filestr_to_file($refstr, $file);
}
sub scalar_from_file {
  my ($file) = @_;
  die "$!: $file" if ! -e $file;
  my $filestr = &filestr_from_file($file);
  $filestr = '{}' if $filestr eq '';
  my $result = eval $filestr;
  die if ! $result;
  return $result;
}
# implements only a subset of yaml
#
# key1: val
# key2:
#  - val1
#  - val2
#
# but, this ambiguity
#
# key1: val
# key2:
#
# is it a single key/val pair, or is is key/array?
#
sub yaml_parse {
  my ($path) = @_;
  my $tbl = {};
  my $key;
  my $filestr = &filestr_from_file($path);
  foreach my $line (split($nl, $filestr)) {
    if (0) {
    } elsif ($line =~ m/^([\w-]+): +(.+?) *$/) {
      $$tbl{$1} = $2;
      $key = undef;
    } elsif ($line =~ m/^([\w-]+): *$/) {
      $$tbl{$1} = [];
      $key = $1;
    } elsif ($line =~ m/^ +- +(.+?) *$/) {
      push @{$$tbl{$key}}, $1;
    } else { die $line . $nl; }
  }
  while (my ($k, $v) = each (%$tbl)) {
    if (&is_array($v)) {
      if (! scalar @$v) { # an empty array
        #$$tbl{$k} = '';
      }
    } else {
      $v =~ s/^\s+//;
      $v =~ s/\s+$//;
      $$tbl{$k} = $v;
      if (! $$tbl{$k}) {
        $$tbl{$k} = []; # set to empty array if empty string (cannot determine, so pick safest???)
      }
    }
  }
  return $tbl;
}
sub cmk_parse {
  my ($file) = @_;
  my $str = &filestr_from_file($file);
  $str =~ s/^\s*(\w+)\s*\(/[ $1 /gms;
  $str =~ s/^\s*\)\s*$/],/gms;
  $str =~ s/\s*\)\s*$/ ],/gms;
  $str =~ s/^/  /gms;
  $str =~ s/([^\s\[\],]+)/'$1',/gms;
  $str = '[' . $nl . $str . $nl . ']' . $nl;
  my $result = eval($str);
}
sub builder_cmk_parse {
  my ($file) = @_;
  my $stmts = &cmk_parse($file);
  my $is_unary = {
    'target-type' => 1,
    'target' => 1,
    'target-libs-dir' => 1,
    'no-default-libs' => 1,
    'target-dir' => 1,
  };
  my $result = {};
  foreach my $tkns (@$stmts) {
    my $lhs = &remove_first($tkns);
    if (0) {
    } elsif ($lhs eq 'set') {
      $lhs = &remove_first($tkns);
      $$result{$lhs} = $tkns;

      if ($${is_unary{$lhs}}) { $$result{$lhs} = $$tkns[0]; }
    } elsif ($lhs eq 'subdir') {
      if (! $$result{'subdirs'}) { $$result{'subdirs'} = []; }
      &add_last($$result{'subdirs'}, @$tkns);
    } else {
      die "unknown cmk command '$lhs'; only 'set' and 'subdir' are allowed";
    }
  }
  if ($$result{'target'} && ! $$result{'no-default-libs'}) {
    if (! $$result{'libs'}) { $$result{'libs'} = []; }
    my %libs_tbl = @{$$result{'libs'}};
    if (! $libs_tbl{'dakota-core'}) { &add_last($$result{'libs'}, 'dakota-core'); }
    if (! $libs_tbl{'dakota'})      { &add_last($$result{'libs'}, 'dakota'); }
  }
  return $result;
}
sub core_count {
  my $cores = 1;
  my $threads = `getconf _NPROCESSORS_ONLN`;
  chomp $threads;
  my $num_threads_per_core = 2; # hackhack
  if ($threads >= $num_threads_per_core) { # hackhack (test is only required because of above hackhack)
    $cores = $threads/$num_threads_per_core;
  }
  return $cores;
}

# ${current_build_dir}
# =>
# $ENV{current_build_dir}
sub resolve_var {
  my ($var) = @_;
  my $result = $var;
  $result =~ s/\$\{(\w+)\}/$1/;
  $result = $ENV{$result};
  if (! $result) {
    $result = $var;
  }
  return $result;
}
sub gen_parts {
  my ($build_cmk, $parts_path) = @_;
  my $build = &builder_cmk_parse($build_cmk);
  my $outstr = '';
  foreach my $input (@{$$build{'srcs'}}) {
    $outstr .= $input . $nl;
  }
  foreach my $files ('target-lib-paths', 'lib-files') {
    foreach my $input (@{$$build{$files}}) {
      $outstr .= $input . $nl;
    }
  }
  # builder_cmk_parse should take all the appropriate cmk files and resolve this
  # exe.cmk, l1.cmk, l2.cmk, ... 'target-libs-dir' should not be required
  my $target_libs_dir = $$build{'target-libs-dir'};
  if ($target_libs_dir) {
    $target_libs_dir = &resolve_var($target_libs_dir);
  }
  if (! $target_libs_dir) {
    $target_libs_dir = &current_build_dir();
  }
  foreach my $input (@{$$build{'target-libs'}}) {
    my $target_lib_file = $target_libs_dir . '/' . $lib_prefix . $input . $lib_suffix;
    $outstr .= $target_lib_file . $nl;
  }
  foreach my $input (@{$$build{'libs'}}) {
    my $lib_file = $input;
    if ($lib_file eq '${found_lib_dl}') {
      if ($$gbl_platform_tbl{'found_lib_dl'}) {
        $lib_file = $$gbl_platform_tbl{'found_lib_dl'};
      } else {
        die "missing 'found_lib_dl'";
      }
    }
    if (! &is_abs($lib_file)) {
      my $dakota_home = &dakota_home();
      if (! $ENV{'LD_LIBRARY_PATH'}) {
        $ENV{'LD_LIBRARY_PATH'} = "$dakota_home/lib";
      } elsif ($ENV{'LD_LIBRARY_PATH'} !~ m=$dakota_home/lib=) {
        $ENV{'LD_LIBRARY_PATH'} = "$dakota_home/lib:$ENV{'LD_LIBRARY_PATH'}";
      }
      $lib_file = `$dakota_home/bin/dakota-find-library $lib_file`;
      die if $?;
      $lib_file =~ s/\s*$//; # chomp
    }
    $outstr .= $lib_file . $nl;
  }
  &filestr_to_file($outstr, $parts_path);
}
sub parts_from_cmk {
  my ($cmk_file, $force) = @_;
  my $parts_path = &parts_path();
  my $build_cmk = &build_cmk_path(&basename($cmk_file));
  if (&is_out_of_date($build_cmk, $parts_path)) {
    #print STDERR "out-of-date: $parts_path\n";
    &gen_parts($build_cmk, $parts_path);
  }
  die if ! -e $parts_path;
  my $result = [];
  my $paths = [split /\s+/, &filestr_from_file($parts_path)];
  foreach my $path (@$paths) {
    my $abs_path = $path;
    if ($path !~ m=^/= && $path !~ m=^\$=) { # if its relative
      $abs_path = &cwd() . '/' . $path; # cannot use Cwd::abs_path() here since $path may not exist
      if (! -e $abs_path) {
        $abs_path = &current_source_dir() . '/' . $path;
      }
    }
    $abs_path = &canon_path($abs_path);
    die if ! $force && ! -e $abs_path;
    &add_last($result, $abs_path);
  }
  die if ! scalar @$result;
  return $result;
}
sub parts {
  my $filestr = &filestr_from_file(&parts_path());
  my $result = [split($nl, $filestr)];
  return $result;
}
sub platform_tbl {
  my $dakota_home = &dakota_home();
  my $result = &yaml_parse("$dakota_home/lib/dakota/platform.yaml");
  return $result;
}
sub filestr_from_file {
  my ($file) = @_;
  if (! -e $file) {
    die __FILE__, ":", __LINE__, ": error: " . &cwd() . " / " . $file . ": $!" . $nl;
  }
  undef $/; ## force files to be read in one slurp
  open FILE, "<", $file or die __FILE__, ":", __LINE__, ": error: " . &cwd() . " / " . $file . ": $!" . $nl;
  flock FILE, LOCK_SH or die;
  my $filestr = <FILE>;
  flock FILE, LOCK_UN or die;
  close FILE            or die __FILE__, ":", __LINE__, ": error: " . &cwd() . " / " . $file . ": $!" . $nl;
  return $filestr;
}
sub dakota_home() {
  if (! $ENV{'dakota_home'}) {
    $ENV{'dakota_home'} = $gbl_prefix;
  }
  my $dakota_home = $ENV{'dakota_home'};
  die if ! $dakota_home;
  return &canon_path($dakota_home);
}
sub start {
  my ($argv) = @_;
  # just in case ...
}
BEGIN {
  $nl = "\n";
  $gbl_prefix = &dk_prefix($0);
  $gbl_platform_tbl = &platform_tbl();
  $h_ext = &var('h_ext');
  $cc_ext = &var('cc_ext');
  $o_ext = &var('o_ext');
  $lib_prefix = &var('lib_prefix');
  $lib_suffix = &var('lib_suffix'); # default dynamic shared object/library extension
  $exe_suffix = &var('exe_suffix');
};
unless (caller) {
  &start(\@ARGV);
}
1;
