#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail

#       1 linker-executable-linux-clang.opts
#       3 linker-executable-darwin-appleclang.opts
#       3 linker-executable-darwin-gnu.opts
#       3 linker-executable-extra-darwin-appleclang.opts
#       3 linker-executable-extra-darwin-gnu.opts
#       3 linker-executable-extra-linux-clang.opts
#       3 linker-executable-extra-linux-gnu.opts
#       3 linker-executable-extra.opts
#       3 linker-executable-linux-gnu.opts
#       3 linker-executable.opts
#       3 linker-shared-library-darwin-appleclang.opts
#       3 linker-shared-library-darwin-gnu.opts
#       3 linker-shared-library-extra-darwin-appleclang.opts
#       3 linker-shared-library-extra-darwin-gnu.opts
#       3 linker-shared-library-extra-linux-clang.opts
#       3 linker-shared-library-extra-linux-gnu.opts
#       3 linker-shared-library-extra.opts
#       3 linker-shared-library-linux-clang.opts
#       3 linker-shared-library-linux-gnu.opts
#       3 linker-shared-library.opts
#       6 compiler-executable-extra-darwin-appleclang.opts
#       6 compiler-executable-extra-darwin-gnu.opts
#       6 compiler-executable-extra-linux-clang.opts
#       6 compiler-executable-extra-linux-gnu.opts
#       6 compiler-executable-extra.opts
#       6 compiler-shared-library-extra-darwin-appleclang.opts
#       6 compiler-shared-library-extra-darwin-gnu.opts
#       6 compiler-shared-library-extra-linux-clang.opts
#       6 compiler-shared-library-extra-linux-gnu.opts
#       6 compiler-shared-library-extra.opts
#      18 compiler-executable-linux-clang.opts
#      19 compiler-executable-darwin-appleclang.opts
#      19 compiler-executable.opts
#      19 compiler-shared-library-darwin-appleclang.opts
#      19 compiler-shared-library-linux-clang.opts
#      19 compiler-shared-library.opts
#      28 compiler-executable-darwin-gnu.opts
#      28 compiler-executable-linux-gnu.opts
#      28 compiler-shared-library-darwin-gnu.opts
#      28 compiler-shared-library-linux-gnu.opts

# clang=clang or clang=appleclang
# {compiler,linker}-{executable,shared-library}-{darwin,linux}-{$clang,gnu}.opts
# {compiler,linker}-{executable,shared-library}-extra-{darwin,linux}-{$clang,gnu}.opts

touch-all-opts() {
  local clang=clang

  if [[ $OSTYPE =~ ^darwin ]]; then
    clang=appleclang
  fi
  local as=( compiler linker )
  local bs=( executable shared-library )

  local cs=( darwin linux )
  local ds=( clang gnu ) # or appleclang

  local a; for a in ${as[@]}; do
    local b; for b in ${bs[@]}; do
      local c; for c in ${cs[@]}; do
        local d; for d in ${ds[@]}; do
          if test $c == darwin; then
            touch $a-$b-$c-appleclang.opts
            touch $a-$b-extra-$c-appleclang.opts
          fi
          touch $a-$b-$c-$d.opts
          touch $a-$b-extra-$c-$d.opts
        done
      done
    done
  done
}

# clang=clang or clang=appleclang
# {compiler,linker}-{executable,shared-library}-{darwin,linux}-{$clang,gnu}.opts
# {compiler,linker}-{executable,shared-library}-extra-{darwin,linux}-{$clang,gnu}.opts

# compiler-executable-{darwin,linux}-$clang.opts
# compiler-executable-{darwin,linux}-gnu.opts

start() {
  local clang=clang
  if [[ $OSTYPE =~ ^darwin ]]; then
    clang=appleclang
  fi

  diff compiler-executable-darwin-$clang.opts \
       compiler-executable-darwin-gnu.opts || true
  diff compiler-executable-linux-$clang.opts \
       compiler-executable-linux-gnu.opts || true

  diff compiler-shared-library-darwin-$clang.opts \
       compiler-shared-library-darwin-gnu.opts || true
  diff compiler-shared-library-linux-$clang.opts \
       compiler-shared-library-linux-gnu.opts || true
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  touch-all-opts "$@"
  set -o xtrace
 #start "$@"
fi
