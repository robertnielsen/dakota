#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-
# -*- tab-width: 2
# -*- indent-tabs-mode: nil

# Copyright (C) 2007 - 2022 Robert Nielsen <robert@dakota.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

package dakota::sst;

use strict;
use warnings;
use sort 'stable';

my $gbl_prefix;
my $nl = "\n";

sub dk_prefix {
  my ($path) = @_;
  $path =~ s|//+|/|;
  $path =~ s|/\./+|/|;
  $path =~ s|^./||;
  if (-d "$path/bin" && -d "$path/lib") {
    return $path
  } elsif ($path =~ s|^(.+?)/+[^/]+$|$1|) {
    return &dk_prefix($path);
  } else {
    die "Could not determine \$prefix from executable path $0: $!\n";
  }
}
BEGIN {
  $gbl_prefix = &dk_prefix($0);
  unshift @INC, "$gbl_prefix/lib";
  use dakota::util;
};
use dakota::sst_match;

use Carp; $SIG{ __DIE__ } = sub { Carp::confess( @_ ) };

use Data::Dumper;
$Data::Dumper::Terse =     1;
$Data::Dumper::Deepcopy =  1;
$Data::Dumper::Purity =    1;
$Data::Dumper::Useqq =     1;
$Data::Dumper::Sortkeys =  0;
$Data::Dumper::Indent =    1;  # default = 2

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT= qw(
                 append_leading_ws
                 add_tkn
                 at
                 changes
                 count_newlines
                 dump
                 filestr
                 is_close_tkn
                 is_open_tkn
                 make
                 prev_tkn
                 seq::balenced
                 add_lost_ws
                 size
                 sst::balenced
                 sst::balenced_in
                 sst::filestr
                 sst::splice
                 sst_cursor::at
                 sst_cursor::balenced
                 sst_cursor::balenced_in
                 sst_cursor::balenced_range
                 sst_cursor::current_tkn
                 sst_cursor::current_tkn_p
                 sst_cursor::dump
                 sst_cursor::make
                 sst_cursor::match
                 sst_cursor::match_any
                 sst_cursor::match_re
                 sst_cursor::next_tkn_n
                 sst_cursor::next_tkn
                 sst_cursor::prev_tkn
                 sst_cursor::size
                 sst_cursor::tkn
                 sst_cursor::token_index
                 token_seq
             );

my ($id,  $mid,  $bid,  $tid,
   $rid, $rmid, $rbid, $rtid) = &ident_regex();
my $h =  &header_file_regex();
my $dqstr = &dqstr_regex();
my $sqstr = &sqstr_regex();

my $sst_debug = 0;
sub log_sub_name {
  my ($name) = @_;
  #print "$name\n";
}
sub sst::open_tkns_for_close_tkn {
  my ($close_tkn, $balenced_info) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  die if ! defined $close_tkn || ! $balenced_info;
  die if (!&sst::is_close_tkn($close_tkn, $balenced_info));
  my $open_for_close = $$balenced_info{'open-for-close'};
  my $open_tkn = $$open_for_close{$close_tkn};
  #die if (!&sst::is_open_tkn($open_tkn, $balenced_info));
  return $open_tkn;
}
sub sst::close_tkn_for_open_tkn {
  my ($open_tkn, $balenced_info) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  die if ! defined $open_tkn || ! $balenced_info;
  die if (!&sst::is_open_tkn($open_tkn, $balenced_info));
  my $close_for_open = $$balenced_info{'close-for-open'};
  my $close_tkn = $$close_for_open{$open_tkn};
  #die if (!&sst::is_open_tkn($open_tkn, $balenced_info));
  return $close_tkn;
}
sub sst::is_open_tkn {
  my ($str, $balenced_info) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  die if ! defined $str || ! $balenced_info;
  my $result = 0;
  my $open_tkns = $$balenced_info{'open'};
  if ($$open_tkns{$str}) {
    $result = 1;
  }
  return $result;
}
sub sst::is_close_tkn {
  my ($str, $balenced_info) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  die if ! defined $str || ! $balenced_info;
  my $result = 0;
  my $close_tkns = $$balenced_info{'close'};
  if ($$close_tkns{$str}) {
    $result = 1;
  }
  return $result;
}
sub sst::make {
  my ($lang_info, $filestr, $file) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  #print STDERR $filestr;
  local $_ = $filestr;
  #&encode_cpp(\$_);
  #&encode_strings(\$_);
  #&encode_comments(\$_);

  my $sst = {
    'prev-line' => 0,
    'file' => undef,
    'line' => 1,
    'tokens' => [],
    'tokens-count' => 0,
    'leading-ws' => '',
    'changes' => {},
    'lang-info' => $lang_info,
  };
  if (defined $file) {
    $$sst{'file'} = $file;
  }
  while (1) {
    if (0) {}
    elsif (m/\G(\s*\@\@\@.*?\n)/gcs)      { } # eat encoded cpp directives
    elsif (m/\G(\s+)/gc)                  { &sst::append_leading_ws($sst, $1); }
    elsif (m~\G(//[^\n]*\n)~gc)           { &sst::append_leading_ws($sst, $1); }
    elsif (m~\G(/\*.*?\*/)~gcs)           { &sst::append_leading_ws($sst, $1); }
    elsif (m/\G(<$h+>)/gc)                { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G(\#$mid)/gc)               { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); } # same as rewrite_symbols() in rewrite.pm
    elsif (m/\G(\#$id)/gc)                { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G(\$?$mid)/gc)              { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G(\#(\(|\{|\[))/gc)         { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G(\$(\(|\{|\[))/gc)         { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G(\@(\(|\{|\[))/gc)         { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G($id)/gc)                  { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G(\d+)/gc)                  { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G(->)/gc)                   { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G(<<)/gc)                   { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G(>>)/gc)                   { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G(::)/gc)                   { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G(\[\[)/gc)                 { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G(\]\])/gc)                 { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G($dqstr)/gc)               { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G($sqstr)/gc)               { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G(\.\.\.)/gcs)              { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G(\.\.)/gcs)                { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G(!~|=~)/gcs)               { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    elsif (m/\G(##)/gc)                   { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }

    elsif (m:\G\?                 (/.+?/)       :gcx) { &sst::add_tkns($sst, [ '?(', '=~', $1, ')' ], $$sst{'file'}, $$sst{'line'}); } # for macro system  ?/regex/
    elsif (m:\G(\?\()\s*          (/.+?/)\s*(\)):gcx) { &sst::add_tkns($sst, [ $1,   '=~', $2, $3  ], $$sst{'file'}, $$sst{'line'}); } # for macro system  ?( [!~|=~] /.../ )
    elsif (m:\G(\?\()\s*(!~|=~)\s*(/.+?/)\s*(\)):gcx) { &sst::add_tkns($sst, [ $1,   $2,   $3, $4  ], $$sst{'file'}, $$sst{'line'}); } # for macro system  ?( [!~|=~] /.../ )

    elsif (m/\G(\?(\(|\{|\[))/gc)         { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); } # for macro system  ?(, ?{, ?[

    elsif (m/\G(\?$id)/gc)                { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); } # for macro system  ?ident
    elsif (m/\G(\?(?:\-|\+)?\d+)/gc)      { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); } # for macro system  ?1, ?2, ...
    elsif (m/\G(\?\.\.\.)/gcs)            { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); } # for macro system  ?...

    elsif (m/\G(=>)/gc)                   { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); } # for macro system  =>

    elsif (m/\G(\\\{)/gcs)                { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); } # for macro system  \{ and \}
    elsif (m/\G(\\\})/gcs)                { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); } # for macro system  \{ and \}
    elsif (m/\G(\\\()/gcs)                { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); } # for macro system  \( and \)
    elsif (m/\G(\\\))/gcs)                { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); } # for macro system  \( and \)
    elsif (m/\G(\\\[)/gcs)                { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); } # for macro system  \[ and \]
    elsif (m/\G(\\\])/gcs)                { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); } # for macro system  \[ and \]

    elsif (m/\G(.)/gcs)                   { &sst::add_tkn($sst, $1, $$sst{'file'}, $$sst{'line'}); }
    else                                  { last; }
  }
  # ?/.../
  # =>
  # ?( =~ /.../ )

  # ?( /.../ )
  # =>
  # ?( =~ /.../ )

  my $size = @{$$sst{'tokens'}};
  if ($size > 0 && $$sst{'leading-ws'}) {
    $$sst{'tokens'}[$size - 1]{'trailing-ws'} .= $$sst{'leading-ws'};
    $$sst{'leading-ws'} = '';
  }
  if ($size > 0 && $$sst{'tokens'}[0]{'leading-ws'}) {
    my ($v, $h) = &bisect_ws($$sst{'tokens'}[0]{'leading-ws'});
    $$sst{'leading-ws'} =              $v;
    $$sst{'tokens'}[0]{'leading-ws'} = $h;
  }
  delete $$sst{'line'};
  delete $$sst{'prev-line'};
  #print STDERR &Dumper($sst);
  #&decode_comments(\$_);
  #&decode_strings(\$_);
  #&decode_cpp(\$_);
  return $sst;
}
sub sst::changes {
  my ($sst) = @_;
  return $$sst{'changes'};
}
sub sst::line {
  my ($sst, $index) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  return undef if ! defined $index || $index < 0;
  my $index_w_line = $index;

  while (! $$sst{'tokens'}[$index_w_line]{'line'}) {
    if (0 eq $index_w_line) {
      last;
    }
    $index_w_line--;
  }
  my $line = $$sst{'tokens'}[$index_w_line]{'line'};
  return $line;
}
sub sst::frag {
  my ($sst, $start_index, $end_index) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  my $frag = [@{$$sst{'tokens'}}[$start_index..$end_index]];
  return $frag;
}
sub sst::size {
  my ($sst) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  my $size = @{$$sst{'tokens'}};
  return $size;
}
sub sst::at {
  my ($sst, $index) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  #my $size = &sst::size($sst);
  #my $file = $$sst{'file'};
  #print "$file:  $__sub__(size=$size, index=$index)\n";
  #if (0 > $index || $index + 1 > $size)
  #{
  #die "sst::at($index) where sst::size() = $size\n";
  #}
  return $$sst{'tokens'}[$index]{'tkn'};
}
sub sst::prev_tkn {
  my ($sst) = @_;
  #my $__sub__ = (caller(0))[3];
  my $token = $$sst{'tokens'}[-1];
  return $$token{'tkn'};
}
sub sst::add_tkns {
  my ($sst, $tkns, $file, $line) = @_;
  if (ref($tkns) eq 'ARRAY') {
    foreach my $tkn (@$tkns) {
      sst::add_tkn($sst, $tkn, $file, $line);
    }
  } else {
    sst::add_tkn($sst, $tkns, $file, $line);
  }
}
sub bisect_ws {
  my ($str) = @_;
  die "invalid args" if ! defined $str;
  $str =~ m/^((?:.*\v)?)((?:[^\v]*))$/s;
  my ($verticals, $horizontals) = ( $1, $2 );
  die "bad internal regex-1:" if ! defined $verticals || ! defined $horizontals;
  die "bad internal regex-2:" if $str ne "$verticals$horizontals";
  return ($verticals, $horizontals);
}
sub sst::add_tkn {
  my ($sst, $tkn, $file, $line) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);

  die "invalid token" if (! defined $tkn || $tkn =~ m/^\s+$/gs);
  $$sst{'line'} += ($tkn =~ tr/\n/\n/);
  my $token = { 'tkn' => $tkn };
  if ($$sst{'line'} != $$sst{'prev-line'}) {
    $$sst{'prev-line'} = $$token{'line'} = $$sst{'line'}; # this is critical in the macro-system adjust_ws()
  }
  if (0 == $$sst{'tokens-count'}) {
    if ($sst_debug) {
      print STDERR "$tkn\n";
    }
  } else {
    my $prev_tkn = &sst::prev_tkn($sst);
    if ($prev_tkn && $$sst{'lang-info'}{'statement'}{'term'}{$prev_tkn}) {
      if ($tkn =~ m|$id|) {
        if ($sst_debug) {
          print STDERR "$tkn\n";
        }
      } elsif ($sst_debug) {
        print STDERR "  $tkn\n";
      }
    } elsif ($sst_debug) {
      print STDERR "  $tkn\n";
    }
  }
  if ($$sst{'leading-ws'} ne '') {
    my $size = @{$$sst{'tokens'}};

    if ($$sst{'leading-ws'} !~ m/\v/g || 0 == $size) {
      $$token{'leading-ws'} .= $$sst{'leading-ws'};
    } else {
      my ($verticals, $horizontals) = &bisect_ws($$sst{'leading-ws'});
      my $prev_token = $$sst{'tokens'}[-1];
      if (! $$prev_token{'trailing-ws'}) { $$prev_token{'trailing-ws'} = ''; }
      $$prev_token{'trailing-ws'} .= $verticals;

      if ($horizontals) {
        $$token{'leading-ws'} .= $horizontals;
      }
    }
    $$sst{'leading-ws'} = '';
  }
  &add_last($$sst{'tokens'}, $token);
  $$sst{'tokens-count'} = scalar @{$$sst{'tokens'}};
}
sub sst::append_leading_ws {
  my ($sst, $str) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  $$sst{'line'} += ($str =~ tr/\n/\n/);
  $$sst{'leading-ws'} .= $str;

  if ($sst_debug) {
    while ($$sst{'leading-ws'} =~ s|\n|\\n|g) {}
    while ($$sst{'leading-ws'} =~ s| |\\s|g)  {}
  }
}
sub sst::add_lost_ws {
  my ($sst, $index, $all_ws) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);

  if (0 < $index && $all_ws) {
    if (!exists $$sst{'tokens'}[$index - 1]{'trailing-ws'}) {
      $$sst{'tokens'}[$index - 1]{'trailing-ws'} = '';
    }
    $$sst{'tokens'}[$index - 1]{'trailing-ws'} .= $all_ws;
  }
}
sub sst::dump {
  my ($sst, $begin_index, $end_index) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);

  my $delim = '';
  my $str = '';

  for (my $i = $begin_index; $i <= $end_index; $i++) {
    my $tkn .= &sst::at($sst, $i);
    $str .= $delim;
    $str .= "'$tkn'";
    $delim = ',';
  }
  return "\[$str\]";
}
sub count_newlines {
  my ($frag, $index, $len) = @_;
  $len = @$frag if ! defined $len;
  $index = 0 if ! defined $index;
  die if $index + $len > scalar @$frag; # ???
  my $count = 0;
  for (my $i = 0; $i < $len; $i++) {
    foreach my $key ('leading-ws', 'trailing-ws') {
      if (defined $$frag[$index + $i]{$key}) {
        my $str = $$frag[$index + $i]{$key};
        $count += ($str =~ tr/\n/\n/);
      }
    }
  }
  return $count;
}
sub sst::splice {
  my ($sst, $index, $lhs_num_tokens, $rhs) = @_;
  my $rhs_num_tokens = scalar @$rhs;
  my $lhs_newline_count = &count_newlines($$sst{'tokens'}, $index, $lhs_num_tokens);
  my $rhs_newline_count = &count_newlines($rhs, 0, scalar @$rhs);

  if (scalar @$rhs) { # assume this was handled earlier using sst::add_lost_ws()
    if (0) {
    } elsif ($lhs_newline_count > $rhs_newline_count) {
      my $diff = $lhs_newline_count - $rhs_newline_count;
      &sst::warning($sst, $index,  "warning: losing $diff newline(s)");
    } elsif ($lhs_newline_count < $rhs_newline_count) {
      my $diff = $rhs_newline_count - $lhs_newline_count;
      &sst::warning($sst, $index,  "warning: gaining $diff newline(s)");
    }
  }
  splice @{$$sst{'tokens'}}, $index, $lhs_num_tokens, @$rhs;
  &sst::adjust_last_trailing_ws($sst);
  return $rhs_num_tokens;
}
sub sst::adjust_last_trailing_ws {
  my ($sst) = @_;
  return if $$sst{'tokens'}[-1]{'trailing-ws'} &&
            $$sst{'tokens'}[-1]{'trailing-ws'} =~ /\n$/;
  my $last_trailing_ws;
  my $size = @{$$sst{'tokens'}};
  for (my $i = $size - 1; $i >= 0; $i--) {
    if ($$sst{'tokens'}[$i]{'trailing-ws'}) {
      $last_trailing_ws = $$sst{'tokens'}[$i]{'trailing-ws'};
      delete $$sst{'tokens'}[$i]{'trailing-ws'};
      last;
    }
  }
  $$sst{'tokens'}[-1]{'trailing-ws'} = $last_trailing_ws if $last_trailing_ws;
  die "" if $$sst{'tokens'}[-1]{'trailing-ws'} !~ /\n$/;
}
sub sst::filestr {
  my ($sst) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  my $prev_was_ident = 0;
  my $filestr = $$sst{'leading-ws'};
  die "" if ! defined $filestr;
  foreach my $token (@{$$sst{'tokens'}}) {
    my $is_ident = 0;
    if ($$token{'tkn'} =~ m/^$id$/) {
      $is_ident = 1;
    }

    if ($$token{'leading-ws'}) {
      #while ($$token{'leading-ws'} =~ s|[^\v]||gs) {} # remove comments
      $filestr .= $$token{'leading-ws'};
    } elsif ($prev_was_ident && $is_ident) {
      $filestr .= ' ';
    }
    $filestr .= $$token{'tkn'};

    if ($$token{'trailing-ws'}) {
      #while ($$token{'trailing-ws'} =~ s|[^\v]||gs) {} # remove comments
      $prev_was_ident = 0;
      $filestr .= $$token{'trailing-ws'};
    } else {
      $prev_was_ident = $is_ident;
    }
  }
  if ($nl ne substr $filestr, -1) { # add trailing newline if missing
    print STDERR "Warning: Adding missing final newline\n";
    $filestr .= $nl;
  }
  return $filestr;
}
sub seq::at {
  my ($seq, $i) = @_;
  return $$seq[$i];
}
sub seq::error {
  my ($seq, $i, $msg) = @_;
  die "$msg";
}
sub seq::balenced {
  my ($seq, $balenced_info, $open_token_index) = @_;
  my $funcs = { 'at'    => \&seq::at,
                'error' => \&seq::error };
  return &seq_balenced($seq, scalar @$seq, $funcs, $balenced_info, $open_token_index);
}
sub sst::balenced_in {
  my ($sst, $token_index) = @_;
  die if 0 == $token_index;
  my $result = &sst::balenced($sst, $token_index - 1);
  if (-1 != $result) {
    $result--;
  }
  return $result;
}
sub sst::balenced {
  my ($sst, $open_token_index) = @_;
  my $funcs = { 'at'    => \&sst::at,
                'error' => \&sst::error };
  my $balenced_info = $$sst{'lang-info'}{'-balenced-'};
  return &seq_balenced($sst, &sst::size($sst), $funcs, $balenced_info, $open_token_index);
}
sub seq_balenced {
  my ($seq, $size, $funcs, $balenced_info, $open_token_index) = @_;
  my $result = -1;
  my $at =    $$funcs{'at'};
  my $error = $$funcs{'error'};

  $open_token_index = 0 if ! defined $open_token_index;
  my $opens = [];
  for (my $close_token_index = $open_token_index;
       $close_token_index < $size;
       $close_token_index++) {
    my $token = &$at($seq, $close_token_index);
    if (&sst::is_open_tkn($token, $balenced_info)) {
      &add_last($opens, $token);
    } elsif (&sst::is_close_tkn($token, $balenced_info)) {
      my $open_token = &remove_last($opens);
      my $expected_close_token = &sst::close_tkn_for_open_tkn($open_token, $balenced_info);
      if ($expected_close_token ne $token) {
        &$error($seq, $close_token_index, "expected '$expected_close_token' but got '$token'");
      }
    }
    if (0 == @$opens) {
      if ($open_token_index < $close_token_index ) {
        $result = $close_token_index;
      }
      last;
    }
  }
  return $result;
}
sub sst_cursor::balenced_in {
  my ($sst_cursor) = @_;
  my $token_index = $$sst_cursor{'current-token-index'};
  $token_index = &sst_cursor::translate_token_index_to_sst($sst_cursor, $token_index);
  return &sst::balenced_in($$sst_cursor{'sst'}, $token_index);
}
sub sst_cursor::balenced {
  my ($sst_cursor) = @_;
  my $token_index = &sst_cursor::translate_token_index_to_sst($sst_cursor);
  my $result = sst::balenced($$sst_cursor{'sst'}, $token_index);
  if ($result != -1) {
    $result = &sst_cursor::translate_token_index_from_sst($sst_cursor, $result);
  }
  return $result;
}
sub sst_cursor::balenced_range {
  my ($sst_cursor) = @_;
  my $open_token_index = $$sst_cursor{'current-token-index'};
  my $close_token_index = &sst_cursor::balenced($sst_cursor);
  return ($open_token_index, $close_token_index);
}
# take 1 or 2 or 3 args
sub sst_cursor::make {
  my ($sst, $first_token_index, $last_token_index) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  my $size = &sst::size($sst);
  die "cannot make sst_cursor from empty sst" if $size == 0;
  my $sst_cursor = { 'sst' => $sst, 'current-token-index' => 0 };

  $first_token_index = 0         if ! defined $first_token_index;
  $last_token_index =  $size - 1 if ! defined $last_token_index;

  $$sst_cursor{'first-token-index'} = $first_token_index;
  $$sst_cursor{'last-token-index'} =  $last_token_index;

  if (0) {
    my $file = $$sst_cursor{'sst'}{'file'};
    delete $$sst_cursor{'sst'};
    my $ident = $Data::Dumper::Indent;
    $Data::Dumper::Indent =    0;
    print STDERR "$file: " . &Dumper($sst_cursor) . $nl;
    $Data::Dumper::Indent =    $ident;
    $$sst_cursor{'sst'} = $sst;
  }
  return $sst_cursor;
}
sub sst_cursor::translate_token_index_to_sst {
  my ($sst_cursor, $token_index) = @_;
  $token_index = $$sst_cursor{'current-token-index'} if ! defined $token_index;
  die "" if ! defined $token_index || $token_index < 0;
  die "" if ! defined $$sst_cursor{'first-token-index'} || $$sst_cursor{'first-token-index'} < 0;
  return $$sst_cursor{'first-token-index'} + $token_index;
}
sub sst_cursor::translate_token_index_from_sst {
  my ($sst_cursor, $token_index) = @_;
  die "" if ! defined $token_index || $token_index < 0;
  die "" if ! defined $$sst_cursor{'first-token-index'} || $$sst_cursor{'first-token-index'} < 0;
  return $token_index - $$sst_cursor{'first-token-index'};
}
sub sst_cursor::dump {
  my ($sst_cursor) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  my $sst_tokens = $$sst_cursor{'sst'}{'tokens'};
  delete $$sst_cursor{'sst'}{'tokens'};

  my $sst_leading_ws = $$sst_cursor{'sst'}{'leading-ws'};
  delete $$sst_cursor{'sst'}{'leading-ws'};

  print STDERR &Dumper($sst_cursor);

  $$sst_cursor{'sst'}{'tokens'} = $sst_tokens;
  $$sst_cursor{'sst'}{'leading-ws'} = $sst_leading_ws;
  return $sst_cursor;
}
sub sst_cursor::tkn {
  my ($sst_cursor, $range) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);

  if (!defined $range) {
    $range = [0, @{$$sst_cursor{'sst'}} - 1];
  }
  my $tkn = '';

  for (my $i = $$range[0]; $i <= $$range[1]; $i++) {
    $tkn .= &sst_cursor::at($sst_cursor, $i);
  }
  return $tkn;
}
sub sst_cursor::at {
  my ($sst_cursor, $token_index) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  #my $size = @{$$sst_cursor{'sst'}{'tokens'}};
  #my $file = $$sst_cursor{'sst'}{'file'};
  #print "$file:  $__sub__(size=$size, index=$token_index)\n";

  #die if ($size <= $token_index);

  $token_index = &sst_cursor::translate_token_index_to_sst($sst_cursor, $token_index);
  if (exists $$sst_cursor{'last-token-index'}) {
    if ($token_index > $$sst_cursor{'last-token-index'}) {
      print STDERR "index $token_index larger than last-token-index $$sst_cursor{'last-token-index'}\n";
    }
  }
  return &sst::at($$sst_cursor{'sst'}, $token_index);
}
sub sst_cursor::size {
  my ($sst_cursor) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);

  my $size;
  if ($$sst_cursor{'last-token-index'}) {
    $size = $$sst_cursor{'last-token-index'} + 1;
  } else {
    $size = @{$$sst_cursor{'sst'}{'tokens'}};
  }

  if (exists $$sst_cursor{'first-token-index'}) {
    $size -= $$sst_cursor{'first-token-index'};
  }
  return $size;
}
sub sst_cursor::prev_tkn {
  my ($sst_cursor) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  die "no prev token for token at index 0" if $$sst_cursor{'current-token-index'} == 0;
  return &sst_cursor::at($sst_cursor, $$sst_cursor{'current-token-index'} - 1);
}
sub sst_cursor::current_tkn_p {
  my ($sst_cursor) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  if ($$sst_cursor{'current-token-index'} < @{$$sst_cursor{'sst'}{'tokens'}}) {
    return 1;
  } else {
    return 0;
  }
}
sub sst_cursor::current_tkn {
  my ($sst_cursor) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  return &sst_cursor::at($sst_cursor, $$sst_cursor{'current-token-index'});
}
sub sst_cursor::next_tkn_n {
  my ($sst_cursor, $n) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  return &sst_cursor::at($sst_cursor, $$sst_cursor{'current-token-index'} + $n);
}
sub sst_cursor::next_tkn {
  my ($sst_cursor) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  return &sst_cursor::next_tkn_n($sst_cursor, 1);
}
sub sst_cursor::logger {
  my ($sst_cursor, $file, $line) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  my $current_token = &sst_cursor::current_tkn($sst_cursor);
  if (defined $current_token) {
    print STDERR $file, ":", $line, ": ", $current_token, $nl;
  }
}
sub sst_cursor::token_index {
  my ($sst_cursor) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  my $token_index = $$sst_cursor{'current-token-index'};
  if (exists $$sst_cursor{'first-token-index'}) {
    $token_index -= $$sst_cursor{'first-token-index'};
  }
  return $token_index;
}
sub sst_cursor::match {
  my ($sst_cursor, $match_token, $file, $line) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);

  if (&sst_cursor::current_tkn($sst_cursor) eq $match_token) {
    $$sst_cursor{'current-token-index'}++;
  } else {
    &sst_cursor::error($sst_cursor, $$sst_cursor{'current-token-index'}, "expected '$match_token'");
    #&error($file, $line, $$sst_cursor{'current-token-index'});
  }
  return $match_token;
}
sub sst_cursor::match_any {
  my ($sst_cursor, $file, $line) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  my $token = &sst_cursor::current_tkn($sst_cursor);
  $$sst_cursor{'current-token-index'}++;
  return $token;
}
sub sst_cursor::match_re {
  my ($sst_cursor, $match_token, $file, $line) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);

  if (&sst_cursor::current_tkn($sst_cursor) =~ /$match_token/) {
    $$sst_cursor{'current-token-index'}++;
  } else {
    printf STDERR "%s:%i: expected '%s', but got '%s'\n",
      $file,
      $line,
      $match_token,
      &sst_cursor::current_tkn($sst_cursor);
    #&error($file, $line, &sst_cursor::current_tkn($sst_cursor));
  }
  return &sst_cursor::at($sst_cursor, $$sst_cursor{'current-token-index'} - 1);
}
sub sst_cursor::warning {
  my ($sst_cursor, $token_index, $msg) = @_;
  $token_index = &sst_cursor::translate_token_index_to_sst($sst_cursor, $token_index);
  &sst::warning($$sst_cursor{'sst'}, $token_index, $msg);
}
sub sst::warning_msg {
  my ($sst, $token_index, $msg) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  my $line = &sst::line($sst, $token_index);
  my $result;

  if ($msg) {
    $result = sprintf "%s:%s: %s\n",
      $$sst{'file'} || "<unknown>",
      $line || "<unknown>",
      $msg;
  } else {
    $result = sprintf "%s:%s: did not expect \'%s\'\n",
      $$sst{'file'} || "<unknown>",
      $line || "<unknown>",
      &sst::at($sst, $token_index);
  }
  return $result;
}
sub sst::warning {
  my ($sst, $token_index, $msg) = @_;
  my $warning_msg = &sst::warning_msg($sst, $token_index, $msg);
  print STDERR $warning_msg;
}
sub sst_cursor::error {
  my ($sst_cursor, $token_index, $msg) = @_;
  $token_index = &sst_cursor::translate_token_index_to_sst($sst_cursor, $token_index);
  &sst::error($$sst_cursor{'sst'}, $token_index, $msg);
}
sub sst::error {
  my ($sst, $token_index, $msg) = @_;
  #my $__sub__ = (caller(0))[3]; &log_sub_name($__sub__);
  my $warning_msg = &sst::warning_msg($sst, $token_index, $msg);
  die $warning_msg;
}
sub start {
  my ($argv) = @_;
  # just in case ...
}
unless (caller) {
  &start(\@ARGV);
}
1;
