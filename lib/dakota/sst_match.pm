#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-
# -*- tab-width: 2
# -*- indent-tabs-mode: nil

# Copyright (C) 2007 - 2022 Robert Nielsen <robert@dakota.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

package dakota::sst;

use strict;
use warnings;
use sort 'stable';

my $gbl_prefix;
my $nl = "\n";

BEGIN {
  $gbl_prefix = &dk_prefix($0);
  unshift @INC, "$gbl_prefix/lib";
  use dakota::util;
};
use Carp; $SIG{ __DIE__ } = sub { Carp::confess( @_ ) };

use Data::Dumper;
$Data::Dumper::Terse =     1;
$Data::Dumper::Deepcopy =  1;
$Data::Dumper::Purity =    1;
$Data::Dumper::Useqq =     1;
$Data::Dumper::Sortkeys =  0;
$Data::Dumper::Indent =    1;  # default = 2

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT= qw(
                 sst_cursor::match_pattern_seq
             );

my ($id,  $mid,  $bid,  $tid,
   $rid, $rmid, $rbid, $rtid) = &ident_regex();
my $h =  &header_file_regex();
my $dqstr = &dqstr_regex();
my $sqstr = &sqstr_regex();

sub constraint_literal_dquoted_cstring {
  my ($sst, $range) = @_;
  my ($result_lhs, $result_rhs) = (undef, []);
  my $token = &sst::at($sst, $$range[0]);

  if ($token =~ m/\#$dqstr/) {
    $result_lhs = [ $$range[0], $$range[0] ];
    &add_last($result_rhs, $token);
  }
  return ($result_lhs, $result_rhs);
}
sub constraint_literal_squoted_cstring {
  my ($sst, $range) = @_;
  my ($result_lhs, $result_rhs) = (undef, []);
  my $token = &sst::at($sst, $$range[0]);

  if ($token =~ m/\#$sqstr/) {
    $result_lhs = [ $$range[0], $$range[0] ];
    &add_last($result_rhs, $token);
  }
  return ($result_lhs, $result_rhs);
}
sub constraint_ident {
  my ($sst, $range) = @_;
  my ($result_lhs, $result_rhs) = (undef, []);
  my $token = &sst::at($sst, $$range[0]);

  if ($token =~ m/$id/) {
    $result_lhs = [ $$range[0], $$range[0] ];
    &add_last($result_rhs, $token);
  }
  return ($result_lhs, $result_rhs);
}
sub constraint_qual_scope {
  my ($sst, $range) = @_;
  my ($result_lhs, $result_rhs) = &constraint_qual_ident($sst, $range);
  my $special_idents = { 'slots-t' => 1, 'klass' => 1, 'box' => 1, 'unbox' => 1 };
  #print "result_lhs: " . &Dumper($result_lhs) . $nl;
  #print "result_rhs: " . &Dumper($result_rhs) . $nl;

  if (0 != @$result_rhs) {
    if ($$special_idents{$$result_rhs[-1]}) {
      $$result_lhs[1] -= 1;
      remove_last($result_rhs);
      if (0 != @$result_rhs) {
        if ('::' eq $$result_rhs[-1]) {
          $$result_lhs[1] -= 1;
          remove_last($result_rhs);
        }
        if ($$result_lhs[0] > $$result_lhs[1]) {
          ($result_lhs, $result_rhs) = (undef, []);
        }
      }
    }
  }
  return ($result_lhs, $result_rhs);
}
sub constraint_qual_ident {
  my ($sst, $range) = @_;
  #print 'qual-ident-input-range; ' . &Dumper($range);
  my ($result_lhs, $result_rhs) = (undef, []);
  my $i = $$range[0];
  my $token = &sst::at($sst, $i);

  # ::? $id (:: $id)* should be non-greedy

  # missing code to match optional leading ::

  if ($token =~ m/^$id$/) {
    $result_lhs = [ $$range[0], $i ];
    &add_last($result_rhs, $token);

    while (1) {
      $i++;
      my $sro = &sst::at($sst, $i);
      #print "SRO: " . '{' . $sro . '}' . $nl;
      if ($sro eq '::') {
        $i++;
        $token = &sst::at($sst, $i);
        #print "TKN: " . '{' . $token . '}' . $nl;
        if ($token =~ m/^$id$/) {
          $result_lhs = [ $$range[0], $i ];
          &add_last($result_rhs, $sro);
          &add_last($result_rhs, $token);
        } else {
          last;
        }
      } else {
        last;
      }
    }
  }
  if ($result_lhs) {
    #print 'qual-ident-result: ' . &Dumper([$result_lhs, $result_rhs]);
  }
  return ($result_lhs, $result_rhs);
}
sub constraint_method_name {
  my ($sst, $range) = @_;
  my ($result_lhs, $result_rhs) = (undef, []);
  my $token = &sst::at($sst, $$range[0]);

  if ($token =~ m/$mid/) {
    $result_lhs = [ $$range[0], $$range[0] ];
    &add_last($result_rhs, $token);
  }
  return ($result_lhs, $result_rhs);
}
sub constraint_generic_name {
  my ($sst, $range) = @_;
  my ($result_lhs, $result_rhs) = (undef, []);
  my $token = &sst::at($sst, $$range[0]);

  if ($token =~ m/\$$mid/) {
    $result_lhs = [ $$range[0], $$range[0] ];
    &add_last($result_rhs, $token);
  }
  return ($result_lhs, $result_rhs);
}
sub constraint_block {
  my ($sst, $range) = @_;
  return &constraint_balenced($sst, $range);
}
sub constraint_list {
  my ($sst, $range) = @_;
  return &constraint_balenced($sst, $range);
}
sub constraint_block_in {
  my ($sst, $range) = @_;
  return &constraint_balenced_in($sst, $range);
}
sub constraint_list_in {
  my ($sst, $range) = @_;
  return &constraint_balenced_in($sst, $range);
}
sub constraint_for_name {
  my ($name) = @_;
  my $constraint_tbl = {
    #'?qual-ident' => \&constraint_qual_ident,
    #'?qual-scope' => \&constraint_qual_scope,
    '?method-name' =>  \&constraint_method_name,
    '?generic-name' => \&constraint_generic_name,
    '?literal-dquoted-cstring' => \&constraint_literal_dquoted_cstring,
    '?literal-squoted-cstring' => \&constraint_literal_squoted_cstring,
    '?ident' => \&constraint_ident,
    '?block' => \&constraint_block,
  };
  my $constraint = $$constraint_tbl{$name};
  return $constraint;
}
sub constraint_balenced_in {
  my ($sst, $range) = @_;
  my ($result_range, $result_tokens) = (undef, []);

  if (1 <= $$range[0]) {
    ($result_range, $result_tokens) = &constraint_balenced($sst, [ $$range[0] - 1, $$range[1] ]);

    if ($result_range) {
      $$result_range[1]--;
      die if 0 == $$result_range[1];

      for (my $i = $$result_range[0]; $i < $$result_range[1]; $i++) {
        my $token = &sst::at($sst, $i);
        &add_last($result_tokens, $token);
      }
    }
  }
  return ($result_range, $result_tokens);
}
sub constraint_balenced {
  my ($sst, $range) = @_;
  my $balenced_info = $$sst{'lang-info'}{'-sst'}{'balenced'};
  my ($result_range, $result_tokens) = (undef, []);
  my $first_token = $$sst{'tokens'}[$$range[0]]{'str'};

  if (&sst::is_open_tkn($first_token, $balenced_info)) {
    my $opens = [];
    my $close_token_index = $$range[0];
    while ($close_token_index <= $$range[1]) {
      my $token = &sst::at($sst, $close_token_index);

      if (&sst::is_open_tkn($token, $balenced_info)) {
        &add_last($opens, $token);
      } elsif (&sst::is_close_tkn($token, $balenced_info)) {
        my $open_token = &remove_last($opens);

        if (!defined $open_token) {
          return undef;
        }

        my $open_tokens = &sst::open_tkns_for_close_tkn($token, $balenced_info);
        die if ! exists $$open_tokens{$open_token}
      }
      &add_last($result_tokens, $token);

      if (0 == @$opens) {
        $result_range = [ $$range[0], $close_token_index ];
        last;
      }
      $close_token_index++;
    }
  }
  return ($result_range, $result_tokens);
}

# really should return -1, 0, 1
# but were not using it for sorting (yet)
sub sst_cursor::match_pattern_seq {
  my ($sst_cursor, $pattern_seq) = @_;
  my $balenced_info = $$sst_cursor{'sst'}{'lang-info'}{'-balenced-'};
  my $range = undef;
  my $matches = undef;
  my $input_len = &sst_cursor::size($sst_cursor);
  my $pattern_seq_len = @$pattern_seq;

  if ($input_len >= $pattern_seq_len) {
    my $all_index = [];
    $range = [];
    $matches = {};
    for (my $i = 0; $i < $pattern_seq_len; $i++) {
      my $input_token = &sst_cursor::at($sst_cursor, $i);
      my $pattern_token = $$pattern_seq[$i];

      if (!defined $input_token || !defined $pattern_token) {
        $range = undef;
        $matches = undef;
        last;
      }
      #print STDERR "$input_token  <=>  $pattern_token\n";

      if ($pattern_token =~ m/^\?/) {
        my $constraint = &constraint_for_name($pattern_token);
        my ($result_lhs, $result_rhs) = &$constraint($$sst_cursor{'sst'},
                                                     [ $$sst_cursor{'first-token-index'} + $i,
                                                       @{$$sst_cursor{'sst'}{'tokens'}} - 1 ],
                                                     $balenced_info);
        if ($result_lhs) {
          $range = $result_lhs;
          $$matches{$pattern_token} = $result_rhs;
          &add_last($all_index, $$result_lhs[0]);
          &add_last($all_index, $$result_lhs[1]);
          if ('?qual-scope' eq $pattern_token) {
            my $prev_indent = $Data::Dumper::Indent;
            $Data::Dumper::Indent = 0;
            print "RANGE: " . &Dumper($range) . ", QUAL_IDENT: " . &Dumper($result_rhs) . $nl;
            $Data::Dumper::Indent = $prev_indent;
          }
        } else {
          $all_index = [];
          $range = undef;
          $matches = undef;
          last;
        }
      } else {
        if ($input_token eq $pattern_token) {
          $range = [ $$sst_cursor{'first-token-index'} + $i,
                     $$sst_cursor{'first-token-index'} + $i ];

          &add_last($all_index, $$sst_cursor{'first-token-index'} + $i);

          if (0) {
            my $prev_indent = $Data::Dumper::Indent;
            $Data::Dumper::Indent = 0;
            print "RANGE: " . &Dumper($range) . ", TOKEN: \"" . $input_token . "\"" . $nl;
            $Data::Dumper::Indent = $prev_indent;
          }
        } else {
          $all_index = [];
          $range = undef;
          $matches = undef;
          last;
        }
      }
    }
    if ($range && 0 != @$range) {
      my $sorted_all_index = [sort {$a <=> $b} @$all_index];
      my $first_token_index = &first($sorted_all_index);
      my $last_token_index = &last($sorted_all_index);
      my $from_index = 0;
      my $to_index = $from_index + $last_token_index - $first_token_index;
      $range = [ $from_index, $to_index ];
    } else {
      $all_index = [];
      $range = undef;
      $matches = undef;
    }
  }
  return ($range, $matches);
}
1;
