klass foo {
  superklass ...;
  klass ...;
  slots ...;
  method ...;
  object-t initialize ...;
  object-t finalize ...;
}
