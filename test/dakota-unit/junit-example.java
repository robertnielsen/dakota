import org.junit.*;

public class TestFoobar {
  public static Resource resource;
    
  @BeforeClass public static void setUpClass() throws Exception {
      // Code executed before the first test method
  }
  @AfterClass public static void tearDownClass() throws Exception {
      // Code executed after the last test method
  }
  @Before public void setUp() throws Exception {
      // Code executed before each test
  }
  @After public void tearDown() throws Exception {
      // Code executed after each test
  }
  @Test public void testOneThing() {
      // Code that tests one thing
  }
  @Test public void testAnotherThing() {
      // Code that tests another thing
  }
  @Test public void testSomethingElse() {
      // Code that tests something else
  }
}
