#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set +o errexit # required!!
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
rel_source_dir=../..
export source_dir=$(cd $samedir/$rel_source_dir && pwd)
source "$source_dir/vars.sh"

export source_dir=$1
export project_source_dir=$2
export current_source_dir=$3

export project_build_dir=$(current-build-dir-from-current-source-dir $project_source_dir)
export current_intmd_dir=$(current-intmd-dir-from-current-source-dir $current_source_dir)
export current_build_dir=$(current-build-dir-from-current-source-dir $current_source_dir)

timeout=5
interpreter="$project_build_dir/run-with-timeout/run-with-timeout $timeout"
#interpreter=
rm -f $current_intmd_dir/check.pass
touch $current_intmd_dir/check.fail


pushd $current_source_dir > /dev/null
if   test -e "$current_build_dir/exe" && test -e ./check.sh; then
  clean=1 project_build_dir=$project_build_dir current_build_dir=$current_build_dir $interpreter ./check.sh $current_build_dir/exe > /dev/null 2>&1 && rc=$? || rc=$?
elif test -e "$current_build_dir/exe"; then
  clean=1 project_build_dir=$project_build_dir current_build_dir=$current_build_dir $interpreter            $current_build_dir/exe > /dev/null 2>&1 && rc=$? || rc=$?
else
  rc=1
fi

if test $rc -eq 0; then
  mv $current_intmd_dir/check.fail $current_intmd_dir/check.pass
fi
popd > /dev/null
