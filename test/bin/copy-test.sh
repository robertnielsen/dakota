#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
project_source_dir=$(cd $samedir/.. && pwd)

if [ 2 != $# ]; then
  echo "usage: $0 <0|1|2|3> <test-name>"
fi

cp -pr $project_source_dir/templates/exe-$1 $2
