#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-

use strict;
use warnings;
use sort 'stable';
use Cwd;
use File::Spec;
use Fcntl qw(:DEFAULT :flock);

my $dakota_home;
my $nl = "\n";

sub dk_prefix {
  my ($path) = @_;
  $path =~ s|//+|/|;
  $path =~ s|/\./+|/|;
  $path =~ s|^./||;
  if (-d "$path/bin" && -d "$path/lib") {
    return $path
  } elsif ($path =~ s|^(.+?)/+[^/]+$|$1|) {
    &dk_prefix($path);
  } else {
    die "Could not determine \$prefix from executable path $0: $!" . $nl;
  }
}

BEGIN {
  $dakota_home = $ENV{'dakota_home'};
  die "missing env var 'dakota_home'" if ! $dakota_home;
  unshift @INC, "$dakota_home/lib";
};
use Carp; $SIG{ __DIE__ } = sub { Carp::confess( @_ ) };

use dakota::util;

use Data::Dumper;
$Data::Dumper::Terse =    1;
$Data::Dumper::Purity =   1;
$Data::Dumper::Sortkeys = 1;
$Data::Dumper::Indent =   1; # default = 2

sub ds {
  my ($paths) = @_;
  my $data = {};
  foreach my $path (@$paths) {
    my ($current_source_dir, $name) = &path_split($path);
    if (! exists $$data{$current_source_dir}) {
      $$data{$current_source_dir} = {};
    }
    $$data{$current_source_dir}{$name} = 1;

    my $names = {};
    foreach my $cmk_file (glob "$current_source_dir/*.cmk") {
      $cmk_file = &basename($cmk_file);
      $$names{$cmk_file} = 1;
    }
  }
  return $data;
}
sub gen_check_mk {
  my ($project_source_dir, $current_source_dir) = @_;
  my $source_dir = &dirname($project_source_dir);
  my $project_build_dir = &current_build_dir_from_current_source_dir($project_source_dir);
  my $current_build_dir = &current_build_dir_from_current_source_dir($current_source_dir);
  my $current_intmd_dir = &current_intmd_dir_from_current_source_dir($current_source_dir);
  system(('mkdir', '-p', "$current_build_dir"));
  system(('mkdir', '-p', "$current_intmd_dir"));
  my $dakota_home = $ENV{'dakota_home'};
  die if ! $dakota_home;
  my $rel_current_intmd_dir = File::Spec->abs2rel($current_intmd_dir, $project_source_dir);
  my $recipe =
    ".PHONY : all" . $nl .
    $nl .
    "all : $current_intmd_dir/check.pass" . $nl .
    $nl .
    "$rel_current_intmd_dir/check.pass : $current_intmd_dir/check.pass" . $nl .
    $nl .
    "$current_intmd_dir/check.pass :" . $nl .
    "\t\@$project_source_dir/bin/current-check-recipe.sh $source_dir $project_source_dir $current_source_dir" . $nl;
  open(my $fh, '>', "$current_intmd_dir/check.mk") or die;
  flock $fh, LOCK_EX or die;
  print $fh $recipe;
  flock $fh, LOCK_UN or die;
  close($fh);
  return "include $current_intmd_dir/check.mk" . $nl;
}
sub gen {
  my ($project_source_dir, $data) = @_;
  my $source_dir = &dirname($project_source_dir);
  my $current_source_dirs = [keys %$data];
  my $check_mk_lines = '';
  foreach my $current_source_dir (sort @$current_source_dirs) {
    #print $current_source_dir . $nl;
    $check_mk_lines .= &gen_check_mk($project_source_dir, $current_source_dir);
  }
  my $project_build_dir = &current_build_dir_from_current_source_dir($project_source_dir);
  my $project_intmd_dir = &current_intmd_dir_from_current_source_dir($project_source_dir);
  system(('mkdir', '-p', "$project_build_dir"));
  system(('mkdir', '-p', "$project_intmd_dir"));
  open(my $fh2, '>>', "$project_intmd_dir/check.mk") or die "$!";
  flock $fh2, LOCK_EX or die;
  print $fh2 $check_mk_lines;
  flock $fh2, LOCK_UN or die;
  close($fh2);
}
sub start {
  my ($paths) = @_;
  my $prefix = &dk_prefix($0);
  my $project_source_dir = "$prefix/test";
  my $data = &ds($paths);
  &gen($project_source_dir, $data);
}
&start(\@ARGV);
