#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
project_source_dir=$(cd $samedir/.. && pwd)
source_dir=$(cd $project_source_dir/.. && pwd)
source "$source_dir/vars.sh"

system=$host_system_name

skip=0
for path in $(cat check-history/$system/should-pass/pass.txt); do
    if test 0 -ne $skip; then
        skip=0
    else
        touch $path
        sleep 2
        skip=1
    fi
done
