#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
project_source_dir=$(cd $samedir/.. && pwd)
source_dir=$(cd $project_source_dir/.. && pwd)
source "$source_dir/vars.sh"

system=$(uname -s | tr [:upper:] [:lower:])

mkdir -p check-history/$system/should-fail
mkdir -p check-history/$system/should-pass/{fail,pass}

num=$(printf "%0.5i" $$)

if test ${min_test:-0} -ne 0; then
  dir=should-pass/pass
  path=check-history/$system/$dir/$num.txt
  DIR=$dir $project_source_dir/bin/check-report.pl --output $path should-pass/pass/min-exe{0,1,2,3}/ # also hardcoded elsewhere
  cat $path
  exit 0
fi

for dir in $@; do
  path=check-history/$system/$dir/$num.txt
  DIR=$dir $project_source_dir/bin/check-report.pl --output $path $dir/*/
  cat $path

  rm -f check-history/$system/$dir/last.txt
  pushd check-history/$system/$dir > /dev/null; ln -s $num.txt last.txt; popd > /dev/null
  echo "$path" >> check-history/$system/$dir.txt
  git add $path
done
