#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
project_source_dir=$(cd $samedir/.. && pwd)
source_dir=$(cd $project_source_dir/.. && pwd)
source "$source_dir/vars.sh"

project_intmd_dir=$(current-intmd-dir-from-current-source-dir $project_source_dir)

make -k -j $jobs -f $project_intmd_dir/check.mk || true
#echo "elapsed: $(($SECONDS / 60))m$(($SECONDS % 60))s" 1>&2
