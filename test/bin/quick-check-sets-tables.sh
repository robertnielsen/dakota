#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
project_source_dir=$(cd $samedir/.. && pwd)
$project_source_dir/bin/quick-check.sh should-pass/pass/{sets,counted-sets,tables}
