#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
project_source_dir=$(cd $samedir/.. && pwd)
source_dir=$(cd $project_source_dir/.. && pwd)
source "$source_dir/vars.sh"

system=$host_system_name

dirs=$@

if test 0 -eq $#; then
    dirs="should-pass/pass"
fi

for dir in $dirs; do
  grep "summary: " $(ls -r -t check-history/$system/$dir/*.txt | grep -v last.txt)
done
