#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
project_source_dir=$(cd $samedir/.. && pwd)
source_dir=$(cd $project_source_dir/.. && pwd)
source "$source_dir/vars.sh"
project_build_dir=$(current-build-dir-from-current-source-dir $project_source_dir)
project_intmd_dir=$(current-intmd-dir-from-current-source-dir $project_source_dir)

export PATH=$dakota_home/bin:$PATH
#export DKT_BUILD_PASS_FAIL=1

function gen-check() {
  # this allows xargs to run $proc (concurrent) processes each with $lines (or less) lines per process
  local procs=${procs:-$(cpu-core-count)}
  local args=( $project_source_dir/{should-fail,should-pass/fail,should-pass/pass}/*/build.cmk ) # also hardcoded elsewhere
  if test ${min_test:-0} -ne 0; then
    args=( $project_source_dir/should-pass/pass/min-exe{0,1,2,3}/build.cmk ) # also hardcoded elsewhere
  fi

  echo "# generating check files (*/check.mk) ..." 1>&2
  local lines=$(( ${#args[@]} / $procs ))
  rm -f $project_intmd_dir/check.mk
  (printf "%s\n" ${args[@]} | sort | xargs -L $lines -P $procs $project_source_dir/bin/gen-check.pl)
  #echo "elapsed: $(($SECONDS / 60))m$(($SECONDS % 60))s" 1>&2
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  gen-check "$@"
fi
