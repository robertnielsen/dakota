#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
project_source_dir=$(cd $samedir/.. && pwd)

source "$project_source_dir/bin/common.sh"

dk_paths=$(paths-from-pattern "$2/*/exe.dk")

for exe_src_path in $dk_paths; do
    dir=$(dirname $exe_src_path)
    echo "$dir/$1"
done
