#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-

use strict;
use warnings;
use sort 'stable';
use Fcntl qw(:DEFAULT :flock);

use Getopt::Long;
$Getopt::Long::ignorecase = 0;

my $gbl_prefix;
my $nl = "\n";

sub dk_prefix {
  my ($path) = @_;
  $path =~ s|//+|/|;
  $path =~ s|/\./+|/|;
  $path =~ s|^./||;
  if (-d "$path/bin" && -d "$path/lib") {
    return $path
  } elsif ($path =~ s|^(.+?)/+[^/]+$|$1|) {
    &dk_prefix($path);
  } else {
    die "Could not determine \$prefix from executable path $0: $!" . $nl;
  }
}

BEGIN {
  $gbl_prefix = &dk_prefix($0);
  unshift @INC, "$gbl_prefix/lib";
};
use Carp; $SIG{ __DIE__ } = sub { Carp::confess( @_ ) };

use dakota::util;

my $opts = {};
&GetOptions($opts, 'output=s', 'verbose');

my $verbose = 0;

my $num_pass = 0;
my $num_failed_build = 0;
my $num_failed_run = 0;
my $failed_build = [];
my $failed_run = [];

sub _dirname {
  my ($path) = @_;
  $path =~ s#(/[^/]+$)##;
  return $path;
}
sub assert_file {
  my ($file) = @_;
  die "missing file '$file'" if ! -f "$file";
}
sub assert_dir {
  my ($dir) = @_;
  die "missing dir '$dir'" if ! -d "$dir";
}

die if ! defined $ENV{'source_dir'};
my $source_dir = $ENV{'source_dir'};
die if ! defined         $ENV{'project_source_dir'};
my $project_source_dir = $ENV{'project_source_dir'};
&assert_dir($source_dir);
&assert_dir($project_source_dir);
my $project_intmd_dir = &current_intmd_dir_from_current_source_dir($project_source_dir);
&assert_dir($project_intmd_dir);

# require that $rel_dir is relative to $project_source_dir # oddodd
foreach my $rel_dir (@ARGV) {
  $rel_dir =~ s=/$==; # remove trailing slash
  my $current_source_dir = "$project_source_dir/$rel_dir"; # oddodd
  &assert_file("$current_source_dir/exe.dk");
  my $current_intmd_dir = &current_intmd_dir_from_current_source_dir($current_source_dir);
  my $current_build_dir = &current_build_dir_from_current_source_dir($current_source_dir);
  &assert_dir($current_source_dir);
  &assert_dir($current_intmd_dir);
  &assert_dir($current_build_dir);

  if (! -d $current_intmd_dir) {
    print STDERR "error: missing current_intmd_dir '$current_intmd_dir'" . $nl;
    next;
  }
  if (! -d $current_build_dir) {
    print STDERR "error: missing current_build_dir '$current_build_dir'" . $nl;
    next;
  }
  my $rel_exe = "$rel_dir/exe";
  my $exe = "$current_build_dir/exe";
  if (0) {
  } elsif (! -e $exe) { # fail-build
    print "fail  $rel_exe\n" if $verbose;
    push @$failed_build, $rel_exe;
    $num_failed_build++;
  } elsif (! -e "$current_intmd_dir/check.pass") { # fail-check
    print "fail  $rel_exe\n" if $verbose;
    push @$failed_run, $rel_exe;
    $num_failed_run++;
  } else { # pass-check
    print "pass  $rel_exe\n" if $verbose;
    $num_pass++;
  }
}

my $num_failed = $num_failed_build + $num_failed_run;
my $total = $num_pass + $num_failed;

if ($$opts{'output'}) {
  my $file = $$opts{'output'};
  open STDOUT, ">$file" or die __FILE__, ":", __LINE__, ": error: $file: $!\n";
  flock STDOUT, LOCK_EX or die;
}

if ($$opts{'verbose'}) {
  if (0 != $num_failed_build) {
    print "# build ($num_failed_build failure(s))\n";
    print "build-fail-exe-files :=\\\n";
    my $exe;
    foreach $exe (@$failed_build) {
      print " $exe\\\n";
    }
    print "\n";
  }

  if (0 != $num_failed_run) {
    print "# run ($num_failed_run failure(s))\n";
    print "run-fail-exe-files :=\\\n";
    my $exe;
    foreach $exe (@$failed_run) {
      print " $exe\\\n";
    }
    print "\n";
  }
}
my $pad = ' ' x (17 - length($ENV{'DIR'}));
my $summary = sprintf("# summary: $ENV{'DIR'}/*:" . $pad . "pass/total = %3i/%3i (%3i + %3i = %3i failure(s))\n",
                      $num_pass,
                      $total,
                      $num_failed_build,
                      $num_failed_run,
                      $num_failed);
print $summary;

if ($$opts{'output'}) {
  flock STDOUT, LOCK_UN or die;
  close STDOUT;
}
