#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'

samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
project_source_dir=$(cd $samedir/.. && pwd)

function start() {
  local buildfiles=(); buildfiles=( $(find $project_source_dir/{should-fail,should-pass/fail,should-pass/pass} -name build.cmk) ) # also hardcoded elsewhere
  local bf; for bf in ${buildfiles[@]}; do
    local d; d=$(dirname $bf)
    if   test -f $d/l3.cmk; then
      cp $project_source_dir/templates/exe3/*.cmk $d
    elif test -f $d/l2.cmk; then
      cp $project_source_dir/templates/exe2/*.cmk $d
    elif test -f $d/l1.cmk; then
      cp $project_source_dir/templates/exe1/*.cmk $d
    else
      cp $project_source_dir/templates/exe0/*.cmk $d
    fi
  done
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  start "$@"
fi
