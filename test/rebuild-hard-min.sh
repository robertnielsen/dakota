#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)

function rebuild-hard-min() {
  min_test=1 $samedir/rebuild-hard.sh "$@"
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  rebuild-hard-min "$@"
fi
