#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
project_source_dir=$samedir
source_dir=$(cd $project_source_dir/.. && pwd)
source "$source_dir/vars.sh"
project_build_dir=$(current-build-dir-from-current-source-dir $project_source_dir)

function project-check() {
  local secs=$SECONDS
  echo "# generate checking ..." 1>&2
  $project_source_dir/bin/gen-check.sh
  echo "# checking ..." 1>&2
  ee make ${make_opts[@]} --file $project_source_dir/check.mk --keep-going --silent $@
  echo "# reporting ..." 1>&2
  ee make ${make_opts[@]} --file $project_source_dir/check.mk --keep-going --silent $@ report
  secs=$(( $SECONDS - $secs ))
  echo "# checking [duration=$(($secs / 60))m$(($secs % 60))s]" 1>&2
  echo "elapsed: $(($SECONDS / 60))m$(($SECONDS % 60))s" 1>&2
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  project-check "$@"
fi
