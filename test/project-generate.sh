#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
project_source_dir=$samedir
source_dir=$(cd $project_source_dir/.. && pwd)
source "$source_dir/vars.sh"
project_build_dir=$(current-build-dir-from-current-source-dir $project_source_dir)

function project-generate() {
  local args=( $project_source_dir/{should-fail,should-pass/fail,should-pass/pass}/*/build.cmk ) # also hardcoded elsewhere
  if test ${min_test:-0} -ne 0; then
    args=( $project_source_dir/should-pass/pass/min-exe{0,1,2,3}/build.cmk ) # also hardcoded elsewhere
  fi

  echo "# updating build specs (*/build.cmk) ..." 1>&2
  $project_source_dir/bin/update-buildfiles.sh
  #echo "elapsed: $(($SECONDS / 60))m$(($SECONDS % 60))s" 1>&2

  if test ${min_test:-0} -ne 0; then
    cp $project_source_dir/build.cmk /tmp/build-$$.cmk
    trap "mv /tmp/build-$$.cmk $project_source_dir/build.cmk" EXIT
  fi
  echo "# updating root build spec build.cmk ..." 1>&2
  {
    local arg
    echo "subdir ( run-with-timeout )"
    while read arg; do
      arg=$(rel-path $arg $project_source_dir)
      echo "subdir ( $(dirname $arg) )"
    done < <( printf "%s\n" ${args[@]} | sort )
  } > $project_source_dir/build.cmk
  sort -o $project_source_dir/build.cmk $project_source_dir/build.cmk
  #echo "elapsed: $(($SECONDS / 60))m$(($SECONDS % 60))s" 1>&2

  local secs=$SECONDS
  echo "# generating build files (*/build.{mk,ninja}) ..." 1>&2
  ee $dakota_home/bin/dakota-project \
      --source-dir $project_source_dir \
      --build-dir  $project_build_dir \
      --var         project_source_dir=$project_source_dir
  secs=$(( $SECONDS - $secs ))
  echo "# generating build files (*/build.cmk) [duration=$(($secs / 60))m$(($secs % 60))s]" 1>&2
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  project-generate "$@"
fi
