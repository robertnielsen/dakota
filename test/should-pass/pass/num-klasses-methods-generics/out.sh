#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'

num_methods=$(cat out.txt | egrep "^method:" | wc -l)
echo num-methods: $num_methods

num_unique_methods=$(cat out.txt | egrep "^method:" | sort -u | wc -l)
echo num-unique-methods: $num_unique_methods

num_klasses=$(cat out.txt | egrep "^klass:" | wc -l)
echo num-klasses: $num_klasses

(( num_generics = $num_methods / $num_klasses ))
echo num-generics: $num_generics

cat out.txt | egrep "num-generics:"
echo
(( size1 = $num_klasses * $num_generics * 8 ))
echo num-klasses \* num-generics \* 8 = $size1
echo
(( size2 = $num_klasses * $num_generics * 2 + ( $num_unique_methods * 8 ) ))
echo num-klasses \* num-generics \* 2 + \(num-unique-methods \* 8\) = $size2
echo
echo \(100 \* \($size1 - $size2\)\) / $size1
(( reduction = (100 * ($size1 - $size2)) / $size1 ))
echo reduction: $reduction\%
(( size1_pages = $size1/4096 ))
echo from $size1_pages pages
(( size2_pages = $size2/4096 ))
echo to  $size2_pages pages
echo
echo -n first-method: 
cat out.txt | egrep "^method:" | sort -u | head -1
echo -n last-method: 
cat out.txt | egrep "^method:" | sort -u | tail -1
