#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace
function finish() {
  if test ${clean:-0} -ne 0; then
    rm -f dakota.perl dakota.perl.dump
  fi
}
trap finish EXIT

exe=$1
$exe < dakota.rep > dakota.perl
./dump.pl < dakota.perl > dakota.perl.dump
