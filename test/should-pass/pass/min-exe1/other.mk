include $(shell /usr/local/bin/dakota-build2mk --output ${BUILDDIR}/l1.mk l1.build)

include $(shell /usr/local/bin/dakota-build2mk --output ${BUILDDIR}/exe.mk dakota.build)

.PHONY: single ls

single: exe.dk | project l1.project
	${DAKOTA} --compile --project l1.project l1.dk
	${DAKOTA} --compile --project l1.project share-l1.dk
	${DAKOTA} --shared  --project l1.project --output ${lib_prefix}l1${lib_suffix}
	${DAKOTA} --compile --project project  exe.dk
	${DAKOTA}           --project project --output exe

l1: | l1.project
	${DAKOTA} --compile --project l1.project l1.dk
	${DAKOTA} --compile --project l1.project share-l1.dk
	${DAKOTA} --shared  --project l1.project --output ${lib_prefix}l1${lib_suffix}

ls:
	find build | grep \\.o | sort
