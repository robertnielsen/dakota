#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace
function finish() {
  if test ${clean:-0} -ne 0; then
    :
  fi
}
trap finish EXIT
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
current_source_dir=$samedir
source_dir=$(cd $current_source_dir/../../../.. && pwd)
source "$source_dir/vars.sh"

current_intmd_dir=$(current-intmd-dir-from-current-source-dir $current_source_dir)

exe_inc=$current_intmd_dir/exe.d/exe.inc # .../<target>.d/<bname>.inc
exe=$1
line=$(grep -n 'dkt_hash_switch' $exe_inc /dev/null | grep int1)
if test 0 -ne $?; then
    echo $line: switch expression should not have been rewritten.
fi
