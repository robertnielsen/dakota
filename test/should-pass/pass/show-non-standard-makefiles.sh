#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail

wc -c */Makefile | sort -n
