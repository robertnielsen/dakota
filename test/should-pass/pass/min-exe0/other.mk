include $(shell /usr/local/bin/dakota-build2mk --output ${BUILDDIR}/exe.mk dakota.build)

.PHONY: single ls

single: exe.dk | project
	${DAKOTA} --compile --project project  exe.dk
	${DAKOTA}           --project project --output exe

ls:
	find build | grep \\.o | sort
