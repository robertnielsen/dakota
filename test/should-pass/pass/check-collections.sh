#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'

function check() {
  local dir=$1
  echo $dir
  pushd $dir > /dev/null
  make check
  popd > /dev/null
}

check sets
check counted-sets
check tables
check collection-add-all-first
check collection-forward-iteration
