.PHONY: single

single: exe.dk | project
	${DAKOTA} --compile --project project exe.dk
	${DAKOTA}           --project project --output exe
