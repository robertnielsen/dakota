#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace
function finish() {
  if test ${clean:-0} -ne 0; then
    rm -f out.pl out.dot
  fi
}
trap finish EXIT

exe=$1
$exe > out.pl
./gen-object-graph.pl out.pl > out.dot
#open out.dot
