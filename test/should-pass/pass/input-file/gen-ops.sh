#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail

g++ -Wall -g3 -o gen-ops gen-ops.cc
./gen-ops > ops.txt
sort -o ops.txt ops.txt
cat ops.txt
