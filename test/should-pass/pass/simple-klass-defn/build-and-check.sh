#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
current_source_dir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
rel_source_dir=../../../..
source_dir=$(cd $rel_source_dir && pwd)
source "$source_dir/vars.sh"

function start() {
  if test ${clean:-0} -ne 0; then
    current_build_dir=$(current-build-dir-from-current-source-dir $current_source_dir)
    rm -fr $current_build_dir/t
  fi

  make_opts=(
      --no-builtin-rules
      --no-builtin-variables
      --warn-undefined-variables
  )
  current_intmd_dir=$(current-intmd-dir-from-current-source-dir $current_source_dir)
  make ${make_opts[@]} -f $current_intmd_dir/build.mk $current_source_dir/exe

  if test -f $current_source_dir/check.sh; then
    (cd $current_source_dir && $current_source_dir/check.sh)
  else
    (cd $current_source_dir && $current_source_dir/exe)
  fi
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  start "$@"
fi
