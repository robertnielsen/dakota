#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace
function finish() {
  if test ${clean:-0} -ne 0; then
    rm -f out.pl out-dumped.pl
  fi
}
trap finish EXIT

exe=$1
$exe > out.pl
./dump.pl < out.pl > out-dumped.pl
