include $(shell /usr/local/bin/dakota-build2mk --output ${BUILDDIR}/l1.mk l1.build)
include $(shell /usr/local/bin/dakota-build2mk --output ${BUILDDIR}/l2.mk l2.build)
include $(shell /usr/local/bin/dakota-build2mk --output ${BUILDDIR}/l3.mk l3.build)

include $(shell /usr/local/bin/dakota-build2mk --output ${BUILDDIR}/exe.mk dakota.build)

.PHONY: single l1 ls

single: exe.dk | project l1.project l2.project l3.project
	${DAKOTA} --compile --project l1.project l1.dk
	${DAKOTA} --compile --project l1.project share-l1.dk
	${DAKOTA} --shared  --project l1.project --output l1${lib_suffix}
	${DAKOTA} --compile --project l2.project l2.dk
	${DAKOTA} --compile --project l2.project share-l2.dk
	${DAKOTA} --shared  --project l2.project --output l2${lib_suffix}
	${DAKOTA} --compile --project l3.project l3.dk
	${DAKOTA} --compile --project l3.project share-l3.dk
	${DAKOTA} --shared  --project l3.project --output l3${lib_suffix}
	${DAKOTA} --compile --project project exe.dk
	${DAKOTA}           --project project --output exe

l1: | l1.project
	${DAKOTA} --compile --project l1.project l1.dk
	${DAKOTA} --compile --project l1.project share-l1.dk
	${DAKOTA} --shared  --project l1.project --output l1${lib_suffix}

l1-2: | l1.project
	${DAKOTA} --compile --project l1.project l1.dk share-l1.dk
	${DAKOTA} --shared  --project l1.project --output l1${lib_suffix}

ls:
	find build | grep \\.o | sort
