#!/usr/bin/perl

use strict;
use warnings;

undef $/;

my $filestr = <STDIN>;

print $filestr;
&rewrite_catch_all(\$filestr);
print $filestr;

sub rewrite_catch_all {
  my ($filestr_ref) = @_;
  $$filestr_ref =~ s/(\s*)else\s*\{\s*throw\s*;\s*\}\s*\}\s*catch\s*\(\s*\.\.\.\s*\)\s*(\{\s*.+?throw\s*;\s*\})/$1else $2 }/gmsx;
}
