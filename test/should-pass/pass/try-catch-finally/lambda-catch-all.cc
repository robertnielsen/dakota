# line 1 "lambda-catch-all.dk"
// -*- mode: C++; c-basic-offset: 2; tab-width: 2; indent-tabs-mode: nil -*-

# include <cstdio>

# define FUNC auto

FUNC main() -> int {
# line push
  auto _ca_ = [&]() {
    printf("catch-all\n");
  };
# line pop
  try {
    printf("try\n");
  }
  catch (...) {
    _ca_();
  }
  return 0;
}
