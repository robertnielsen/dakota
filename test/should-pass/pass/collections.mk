project_source_dir := ../..

collections := sets counted-sets tables collection-add-all-first collection-forward-iteration

all: ${collections}
	for dir in $^; do ${MAKE} -C $$dir all; done

check: ${collections}
	for dir in $^; do ${MAKE} -C $$dir check; done

clean: ${collections}
	for dir in $^; do ${MAKE} -C $$dir clean; done
