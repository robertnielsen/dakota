.PHONY: single single-parse single-clean

builddir := $(shell ${source_dir}/bin/dakota-build builddir --build dakota.build)
srcs :=     $(shell ${source_dir}/bin/dakota-build srcs     --build dakota.build)
target :=   $(shell ${source_dir}/bin/dakota-build target   --build dakota.build)

DEFINE-MACROS := --define-macro DK_SRC_UNIQUE_HEADER=${DK_SRC_UNIQUE_HEADER}

single: ${srcs} | project
	${DAKOTA-BASE} --project project ${DEFINE-MACROS} --init
	${DAKOTA-BASE} --project project ${DEFINE-MACROS} --compile --output ${builddir}/exe.cc.o exe.dk
	${DAKOTA-BASE} --project project ${DEFINE-MACROS} --compile --output ${builddir}/exe2.cc.o exe2.dk
	${DAKOTA-BASE} --project project ${DEFINE-MACROS} --compile --output ${builddir}/exe3.cc.o exe3.dk
	${DAKOTA-BASE} --project project ${DEFINE-MACROS} --output ${target} ${builddir}/exe.cc.o ${builddir}/exe2.cc.o ${builddir}/exe3.cc.o

single-clean: clean
	${RM} ${RMFLAGS} *.paths ${target} project ${builddir}-0 ${builddir}-1

opendiff:
	make single-clean
	@rm -rf ${builddir}-0
	DK_SRC_UNIQUE_HEADER=1 make clean single
	find ${builddir} -type f | sort > 0.paths
	@mv ${builddir} ${builddir}-0
	@rm -rf ${builddir}-1
	DK_SRC_UNIQUE_HEADER=0 make clean single
	find ${builddir} -type f | sort > 1.paths
	@mv ${builddir} ${builddir}-1
	opendiff 0.paths 1.paths
