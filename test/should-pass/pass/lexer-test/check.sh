#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace
function finish() {
  if test ${clean:-0} -ne 0; then
    :
  fi
}
trap finish EXIT

exe=$1
$exe input-exposing-bug.txt
