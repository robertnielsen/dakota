#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail

echo "digraph {"
echo "  graph [ rankdir = \"LR\" ];"
echo "  graph [ margin = \"0.25\" ];"
echo "  graph [ page = \"8.5,11\" ];"
echo "  graph [ size = \"8,10.5\" ];"
echo
cat /dev/stdin
echo "}"
