#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace
function finish() {
  if test ${clean:-0} -ne 0; then
    rm -f out.dot
  fi
}
trap finish EXIT

exe=$1
$exe 2>&1 | ./wrap.sh > out.dot
#open out.dot
#make out.dot.ps
