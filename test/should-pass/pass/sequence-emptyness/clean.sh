#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail

files="out out.dot out.dot.ps"
rm -f $files
