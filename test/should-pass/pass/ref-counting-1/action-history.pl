#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-

use strict;
use warnings;

use Data::Dumper;
$Data::Dumper::Terse =     1;
$Data::Dumper::Purity =    1;
$Data::Dumper::Useqq =     1;
$Data::Dumper::Sortkeys =  0;
$Data::Dumper::Indent =    1;  # default = 2

my $nl = "\n";

my $addrs_set = {};
my $addrs = [];
my $data = {};

while (<STDIN>) {
  chomp $_;
  if ($_ =~ m/^([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)(?:\s+([^\s]+)\s+([^\s]+))?$/) {
    my ($addr, $action, $key1, $val1, $key2, $val2) = ($1, $2, $3, $4, $5, $6);
    if (!$$addrs_set{$addr}) {
      $$addrs_set{$addr} = 1;
      push @$addrs, $addr;
    }
    $$data{$addr}{$action}{$key1} = $val1;
    if ($key2) {
      $$data{$addr}{$action}{$key2} = $val2;
    }
  }
}
print &Dumper($data);
#print &Dumper($addrs);
