#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
project_source_dir=$samedir
source_dir=$(cd $project_source_dir/.. && pwd)
source "$source_dir/vars.sh"
project_build_dir=$(current-build-dir-from-current-source-dir $project_source_dir)

function f() {
  echo "# cleaning ..." 1>&2
  ee $dakota_home/bin/dakota-project --clean $project_build_dir --clean-exclude check.mk
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  f "$@"
fi
