.PHONY: single single-parse single-clean

target-base := foo
builddir := $(shell ${source_dir}/bin/dakota-build builddir --build ${target-base}.build)
srcs :=     $(shell ${source_dir}/bin/dakota-build srcs     --build ${target-base}.build)
target :=   $(shell ${source_dir}/bin/dakota-build target   --build ${target-base}.build)

DEFINE-MACROS := --define-macro DK_SRC_UNIQUE_HEADER=${DK_SRC_UNIQUE_HEADER}

single: ${srcs} | ${target-base}.project
	${DAKOTA-BASE} --project ${target-base}.project ${DEFINE-MACROS} --compile --output ${builddir}/foo1.cc.o foo1.dk
	@find dkt -type f | sort > state-foo1.paths
	${DAKOTA-BASE} --project ${target-base}.project ${DEFINE-MACROS} --compile --output ${builddir}/foo2.cc.o foo2.dk
	@find dkt -type f | sort > state-foo2.paths
	${DAKOTA-BASE} --project ${target-base}.project ${DEFINE-MACROS} --compile --output ${builddir}/foo3.cc.o foo3.dk
	@find dkt -type f | sort > state-foo3.paths
	${DAKOTA-BASE} --project ${target-base}.project ${DEFINE-MACROS} --shared --output ${target} ${builddir}/foo1.cc.o ${builddir}/foo2.cc.o ${builddir}/foo3.cc.o
	@find dkt -type f | sort > state-target.paths

single-clean: clean
	${RM} ${RMFLAGS} *.paths ${target} ${target-base}.project ${builddir}-0 ${builddir}-1

opendiff:
	make single-clean
	@rm -rf ${builddir}-0
	DK_SRC_UNIQUE_HEADER=1 make clean single
	find ${builddir} -type f | sort > 0.paths
	@mv ${builddir} ${builddir}-0
	@rm -rf ${builddir}-1
	DK_SRC_UNIQUE_HEADER=0 make clean single
	find ${builddir} -type f | sort > 1.paths
	@mv ${builddir} ${builddir}-1
	opendiff 0.paths 1.paths
