.PHONY:\
 all\
 check\
 clean\

all:
	cat /dev/null > empty.${cc_ext}
	clang++ -std=c++11 --shared -fPIC --output ./${lib_prefix}empty${lib_suffix} empty.${cc_ext}

check: all

clean:
	${RM} ${RMFLAGS} ${lib_prefix}empty${lib_suffix} empty.${cc_ext}
