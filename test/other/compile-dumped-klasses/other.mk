all:
	${prefix}/bin/dakota-catalog ${prefix}/lib/${lib_prefix}dakota.so > libdakota.dk
	${prefix}/bin/dakota --compile --define-macro __darwin__ --output libdakota.${o_ext} libdakota.dk

clean:
	rm -f {libdakota,libdakota}.{dk,${o_ext}}
	rm -fr build

check:
