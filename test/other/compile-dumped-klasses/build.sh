#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }

function ee() { echo $@ 1>&2 && eval $@; }

function build() {
  #export PATH=~/opt-stage/dakota/bin:$PATH
  local bin_dir=~/opt-stage/dakota/bin
  local lib_dir=~/opt-stage/dakota/lib

  ee $bin_dir/dakota-catalog $lib_dir/libdakota.dylib > l1.dk

  local d=bld
  rm -rf $d

  if true; then
    ee $bin_dir/dakota-project --build-dir $d
    ee $bin_dir/dakota-project --build     $d
  else
    local cxx; cxx=$(< $lib_dir/dakota/compiler-darwin-appleclang.txt)
	  cxx=$cxx build_dir=$d ee $bin_dir/dakota --compile --define-macro __darwin__ --output l1.o l1.dk # hackhack: should use o_ext
  fi
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  build "$@"
fi
