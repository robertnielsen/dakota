all: ${target}

${target}: ${prereq}
	clang --all-warnings --debug=3 --output $@ -ldl $^

check:
	./${target} ${prefix}/lib/${lib_prefix}dakota${lib_suffix} ${prefix}/lib/${lib_prefix}dakota${lib_suffix}

clean:
	${RM} ${RMFLAGS} ./${target} *~
