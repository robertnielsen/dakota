.PHONY: all check clean

builddir := dkt
bindir :=   bin

all: | dakota.project
	dakota --project dakota.project --output bin/exe d1/f1.dk d1/d2/f2.dk d1/d2/d3/f3.dk
	if [[ -e ${builddir} ]]; then find ${builddir} -type f | sort; fi
	if [[ -e ${bindir}   ]]; then find ${bindir}   -type f | sort; fi

check:
	${bindir}/exe

clean:
	rm -rf ${builddir} ${bindir} dakota.project
