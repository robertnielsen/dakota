#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace
function finish() {
  if test ${clean:-0} -ne 0; then
    :
  fi
}
trap finish EXIT

exe=$1
set -o verbose
$exe 0
$exe 1
$exe 2
$exe 3
$exe 4
$exe 5
$exe 6
$exe 7
$exe 8
$exe 9
