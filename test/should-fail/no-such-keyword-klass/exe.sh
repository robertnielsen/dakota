#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail

export DK_DUMP_UNHANDLED_EXCEPTION=
$current_build_dir/exe
