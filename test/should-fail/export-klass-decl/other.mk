all: ${target}

${target}: ${prereq} ${lib_prefix}l1${lib_suffix}
	${DAKOTA} ${DAKOTAFLAGS} ${EXTRA_DAKOTAFLAGS} --include-directory=. --output $@ $^

${lib_prefix}l1${lib_suffix}: file-1.dk file-2.dk share-l1.dk
	${DAKOTA} --shared ${DAKOTAFLAGS} ${EXTRA_DAKOTAFLAGS} --include-directory=. --output $@ $^

check:
	./${target}

clean:
	${RM} ${RMFLAGS} build
	${RM} ${RMFLAGS} ./${target} file-{1,2}.o ${lib_prefix}l1${lib_suffix} *~
