# build (17 failure(s))
build-fail-exe-files :=\
 should-fail/absent-required-keyword/exe\
 should-fail/compile-time-vs-run-time-hash/exe\
 should-fail/exception/exe\
 should-fail/export-klass-decl/exe\
 should-fail/illegal-predicate-method-defn/exe\
 should-fail/inner-slots-ptr-klass-defn/exe\
 should-fail/malformed-ka-defn/exe\
 should-fail/method-param-name-klass-compile/exe\
 should-fail/method-param-name-klass-run/exe\
 should-fail/no-such-keyword-klass/exe\
 should-fail/no-such-keyword/exe\
 should-fail/no-such-method-super/exe\
 should-fail/no-such-method/exe\
 should-fail/sig-segv/exe\
 should-fail/slots-struct-w-default-values/exe\
 should-fail/unbalenced-fragment/exe\
 should-fail/unbox-object-instance/exe\

# summary: should-fail/*:      pass/total =   0/ 17 ( 17 +   0 =  17 failure(s))
