# build (7 failure(s))
build-fail-exe-files :=\
 should-fail/compile-time-vs-run-time-hash/exe\
 should-fail/export-klass-decl/exe\
 should-fail/inner-slots-ptr-klass-defn/exe\
 should-fail/method-param-name-klass-run/exe\
 should-fail/no-such-keyword-klass/exe\
 should-fail/unbalenced-fragment/exe\
 should-fail/unbox-object-instance/exe\

# run (5 failure(s))
run-fail-exe-files :=\
 should-fail/absent-required-keyword/exe\
 should-fail/exception/exe\
 should-fail/no-such-keyword/exe\
 should-fail/no-such-method-super/exe\
 should-fail/no-such-method/exe\

# summary: should-fail/*:      pass/total =   5/ 17 (  7 +   5 =  12 failure(s))
