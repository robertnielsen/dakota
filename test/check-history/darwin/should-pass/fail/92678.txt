# build (1 failure(s))
build-fail-exe-files :=\
 should-pass/fail/unbox-illegal-klass-exception-special-case/exe\

# run (11 failure(s))
run-fail-exe-files :=\
 should-pass/fail/illegal-klass-exception/exe\
 should-pass/fail/missing-kw-args-for-init-of-subklass-of-klass/exe\
 should-pass/fail/no-such-method-exception-backward-iterator/exe\
 should-pass/fail/no-such-method-exception/exe\
 should-pass/fail/read-only-klass-ptrs-hashed-set/exe\
 should-pass/fail/read-only-klass-ptrs-null/exe\
 should-pass/fail/read-only-klass-ptrs-object/exe\
 should-pass/fail/read-only-klass-ptrs-string/exe\
 should-pass/fail/read-only-selectors/exe\
 should-pass/fail/sets-add-absent/exe\
 should-pass/fail/unbox-illegal-klass-exception/exe\

# summary: should-pass/fail/*: pass/total =   0/ 12 (  1 +  11 =  12 failure(s))
