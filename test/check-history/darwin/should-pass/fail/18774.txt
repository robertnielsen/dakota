# build (1 failure(s))
build-fail-exe-files :=\
 should-pass/fail/unbox-illegal-klass-exception-special-case/exe\

# run (5 failure(s))
run-fail-exe-files :=\
 should-pass/fail/missing-kw-args-for-init-of-subklass-of-klass/exe\
 should-pass/fail/no-such-method-exception-backward-iterator/exe\
 should-pass/fail/no-such-method-exception/exe\
 should-pass/fail/sets-add-absent/exe\
 should-pass/fail/unbox-illegal-klass-exception/exe\

# summary: should-pass/fail/*: pass/total =   6/ 12 (  1 +   5 =   6 failure(s))
