# build (21 failure(s))
build-fail-exe-files :=\
 should-pass/pass/both-raw+object-method-defns/exe\
 should-pass/pass/box-klass-w-slots+init/exe\
 should-pass/pass/check-super-dispatch/exe\
 should-pass/pass/cxx-and-dk-klasses-with-same-name/exe\
 should-pass/pass/exported-const/exe\
 should-pass/pass/exported-methods/exe\
 should-pass/pass/exported-slots-va-methods/exe\
 should-pass/pass/interposing-stacking/exe\
 should-pass/pass/interposing/exe\
 should-pass/pass/klass-w-exported-method-depends/exe\
 should-pass/pass/klass-w-exported-slots-depends/exe\
 should-pass/pass/kw-args-method-concat/exe\
 should-pass/pass/literal-boole/exe\
 should-pass/pass/literal-mulitbyte-char/exe\
 should-pass/pass/method-alias-super/exe\
 should-pass/pass/set-at/exe\
 should-pass/pass/slots-w-local-enum/exe\
 should-pass/pass/table-methods/exe\
 should-pass/pass/use-of-exported-const-in-other-klasses-slots/exe\
 should-pass/pass/write-slots-literal-table/exe\
 should-pass/pass/xjmp-buf/exe\

# run (6 failure(s))
run-fail-exe-files :=\
 should-pass/pass/counted-sets/exe\
 should-pass/pass/exported-enum-slots/exe\
 should-pass/pass/lexer-test/exe\
 should-pass/pass/literal-int-file-scope/exe\
 should-pass/pass/literal-str-file-scope/exe\
 should-pass/pass/signal-to-exception-1/exe\

# summary: should-pass/pass/*: pass/total = 215/242 ( 21 +   6 =  27 failure(s))
