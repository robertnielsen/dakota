#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
project_source_dir=$samedir
source_dir=$(cd $project_source_dir/.. && pwd)
source "$source_dir/lib/util.sh"
build_dir=$(default-build-dir-from-source-dir $source_dir)
project_build_dir=$(current-build-dir-from-current-source-dir $project_source_dir)

source "$project_source_dir/project-generate.sh"
source "$project_source_dir/project-build.sh"
source "$project_source_dir/project-check.sh"

function rebuild-hard() {
  cd $project_source_dir
  rm -fr $project_build_dir
  git clean -dfx --quiet
  project-generate "$@"
  project-build "$@"
  project-check "$@"
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  rebuild-hard "$@"
fi
