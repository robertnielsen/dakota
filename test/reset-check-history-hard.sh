#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
project_source_dir=$samedir

function start() {
  cd $project_source_dir
  git reset HEAD check-history
  git checkout   check-history
  git clean -dfx check-history
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  start "$@"
fi
