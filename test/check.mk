# -*- mode: makefile -*-

samedir := $(abspath $(dir $(realpath $(firstword $(MAKEFILE_LIST)))))
project_source_dir ?= ${samedir}
source_dir ?= $(abspath ${project_source_dir}/..)
project_intmd_dir := $(shell ${dakota_home}/lib/util.sh current-intmd-dir-from-current-source-dir ${project_source_dir})

intmd_dir := $(shell ${dakota_home}/lib/util.sh intmd-dir)

.PHONY: \
 all\
 clean\
 report\
#

all:
	${project_source_dir}/bin/check-recipe.sh

clean:
	find ${project_intmd_dir} -name check.pass -exec rm -f {} \;
	find ${project_intmd_dir} -name check.fail -exec rm -f {} \;

report:
	source_dir=${source_dir} project_source_dir=${project_source_dir} ${project_source_dir}/bin/check-report.sh should-fail should-pass/fail should-pass/pass
