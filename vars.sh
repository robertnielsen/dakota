if ! test ${source_dir:-}; then echo "missing env var 'source_dir'" 1>&2 && exit 1; fi
export source_dir

if test ${__vars_sh__:-0} -ne 0; then return; fi
__vars_sh__=1

#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'

source "$source_dir/lib/util.sh"

export build_dir=${build_dir:-$(default-build-dir-from-source-dir $source_dir)}

export stage_prefix_dir=${stage_prefix_dir:-$(default-stage-prefix-dir-from-source-dir $source_dir)}
export dakota_home=${dakota_home:-$stage_prefix_dir}

export stage_bin_dir=$stage_prefix_dir/bin
export stage_lib_dir=$stage_prefix_dir/lib
export stage_include_dir=$stage_prefix_dir/include

export PATH=$dakota_home/bin:$PATH

intmd_dir=$(intmd-dir)

if test -e "$intmd_dir/lib/dakota/platform.sh"; then
  source   "$intmd_dir/lib/dakota/platform.sh"
else
  __vars_sh__=0
fi

make_opts=(
  '--no-builtin-rules'
  '--no-builtin-variables'
  '--warn-undefined-variables'
)
jobs=${jobs:-$(builder-jobs)}
