// -*- mode: c++; c-basic-offset: 2; tab-width: 2; indent-tabs-mode: nil -*-

// Copyright (C) 2007 - 2022 Robert Nielsen <robert@dakota.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

# include <cstdio>  // fprintf(), stdout, stderr
# include <cstdlib> // EXIT_SUCCESS, EXIT_FAILURE, getenv()
# include <sys/param.h> // MAXPATHLEN

# include "dakota-dso.h" // dso_find_library()

typealias char_t = char;
typealias int_t =  int;

FUNC main(int_t argc, const str_t argv[]) -> int_t {
  //const char_t* ld_library_path = getenv("LD_LIBRARY_PATH");
  //if (ld_library_path == nullptr) ld_library_path = "";
  //fprintf(stderr, "dakota-find-library: LD_LIBRARY_PATH=%s\n", ld_library_path);
  str_t progname = argv[0];
  int_t exit_value = EXIT_SUCCESS;
  for (int_t i = 1; i < argc; i++) {
    str_t orig_arg = argv[i];
    char_t buf[MAXPATHLEN + 1]  = "";
    str_t result = dso_find_library(orig_arg, buf, sizeof(buf) - 1);
    if (result)
      fprintf(stdout, "%s\n", result);
    else {
      fprintf(stderr, "%s: error: cannot find %s\n", progname, orig_arg);
      exit_value = EXIT_FAILURE;
    }
  }
  return exit_value;
}
