#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
source_dir=$(cd $samedir/.. && pwd)
source "$source_dir/vars.sh"

cat $build_dir/dakota.io

if test -d $build_dir; then
    {
        find $build_dir  -name "*.h"
        find $build_dir  -name "*.cc"
        find $build_dir  -name "*.inc"
    } | sort
fi
if test -d CMakeFiles; then
    find CMakeFiles -name "*.o"  | sort
fi
if test -d cm; then
    find cm -name "*.o"  | sort
fi

{
    find . -name "*.so"
    find . -name "*.dylib"
} | sort
