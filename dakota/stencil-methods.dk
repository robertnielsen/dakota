// -*- mode: c++; mode: dakota; c-basic-offset: 2; tab-width: 2; indent-tabs-mode: nil -*-

// Copyright (C) 2007 - 2022 Robert Nielsen <robert@dakota.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

share dakota;

klass
  ascii-number,
  str-buffer,
  token,
  {
  method compare(object-t self, object-t other) -> cmp-t {
    assert(other != nullptr);
    cmp-t result = 0;
    if (self != other) {
      if (klass-of(other) == _klass_ || $instance-of?(other, _klass_))
        result = compare(unbox(self), unbox(other));
      else
        result = $compare(super, other);
    }
    return result;
  }
}
klass
  float32,
  float64,
  float128,
  floatmax,
  tokenid,
  {
  method init(object-t self, slots-t value:) -> object-t {
    self = $init(super);
    mutable-unbox(self) = value;
    if (klass-of(self) == _klass_)
      self = dk-intern(self);
    return self;
  }
  method compare(const slots-t s, const slots-t other-s) -> cmp-t {
    cmp-t result = DK-CMP(s, other-s);
    return result;
  }
  method compare(object-t self, object-t other) -> cmp-t {
    assert(other != nullptr);
    cmp-t result = 0;
    if (self != other) {
      if (klass-of(other) == _klass_ || $instance-of?(other, _klass_))
        result = compare(unbox(self), unbox(other));
      else
        result = $compare(super, other);
    }
    return result;
  }
}
