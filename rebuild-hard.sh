#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
source_dir=$samedir
source "$source_dir/lib/util.sh"
build_dir=$(default-build-dir-from-source-dir $source_dir)

source "$source_dir/configure"
source "$source_dir/project-generate.sh"
source "$source_dir/project-build.sh"

function rebuild-hard() {
  cd $source_dir
  rm -fr $build_dir
  rm -fr $stage_prefix_dir
  git clean -dfx --quiet
  configure "$@"
  project-generate "$@"
  project-build "$@"
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  rebuild-hard "$@"
fi
