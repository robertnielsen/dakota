#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
source_dir=$samedir
source "$source_dir/lib/util.sh"
build_dir=$(default-build-dir-from-source-dir $source_dir)

source "$source_dir/vars.sh"

function stage-pre-built() {
  local prefix_dir=${1:-$(default-stage-prefix-dir-from-source-dir $source_dir)}
  mkdir -p $prefix_dir/{bin,include}
  mkdir -p $prefix_dir/lib/dakota/opts

  install -m 0755 \
    $source_dir/bin/dakota \
    $source_dir/bin/dakota-project \
  $prefix_dir/bin/

  install -m 0755 \
    $source_dir/lib/util.sh \
  $prefix_dir/lib/

  install -m 0644 \
    $source_dir/include/dakota-dso.h \
    $source_dir/include/dakota-finally.h \
    $source_dir/include/dakota-log.h \
    $source_dir/include/dakota-object-defn.inc \
    $source_dir/include/dakota-object.inc \
    $source_dir/include/dakota-of.inc \
    $source_dir/include/dakota-os.h \
    $source_dir/include/dakota-other.inc \
    $source_dir/include/dakota-trace.inc \
    $source_dir/include/dakota-weak-object-defn.inc \
    $source_dir/include/dakota-weak-object.inc \
    $source_dir/include/dakota.h \
  $prefix_dir/include/

  install -m 0644 \
    $source_dir/lib/dakota/dakota.pm \
    $source_dir/lib/dakota/generate.pm \
    $source_dir/lib/dakota/parse.pm \
    $source_dir/lib/dakota/rewrite.pm \
    $source_dir/lib/dakota/sst.pm \
    $source_dir/lib/dakota/sst_match.pm \
    $source_dir/lib/dakota/util.pm \
    $source_dir/lib/dakota/extra.json \
    $source_dir/lib/dakota/macro-lang-dakota.json \
    $source_dir/lib/dakota/used.json \
  $prefix_dir/lib/dakota/

  install -m 0644 \
    $source_dir/lib/dakota/compiler-darwin-appleclang.txt \
    $source_dir/lib/dakota/compiler-darwin-clang.txt \
    $source_dir/lib/dakota/compiler-darwin-gnu.txt \
    $source_dir/lib/dakota/compiler-linux-clang.txt \
    $source_dir/lib/dakota/compiler-linux-gnu.txt \
  $prefix_dir/lib/dakota/

  install -m 0644 \
    $source_dir/lib/dakota/opts/compiler-executable-darwin-appleclang.opts \
    $source_dir/lib/dakota/opts/compiler-executable-darwin-clang.opts \
    $source_dir/lib/dakota/opts/compiler-executable-darwin-gnu.opts \
    $source_dir/lib/dakota/opts/compiler-executable-extra-darwin-appleclang.opts \
    $source_dir/lib/dakota/opts/compiler-executable-extra-darwin-clang.opts \
    $source_dir/lib/dakota/opts/compiler-executable-extra-darwin-gnu.opts \
    $source_dir/lib/dakota/opts/compiler-executable-extra-linux-clang.opts \
    $source_dir/lib/dakota/opts/compiler-executable-extra-linux-gnu.opts \
    $source_dir/lib/dakota/opts/compiler-executable-linux-clang.opts \
    $source_dir/lib/dakota/opts/compiler-executable-linux-gnu.opts \
    $source_dir/lib/dakota/opts/compiler-shared-library-darwin-appleclang.opts \
    $source_dir/lib/dakota/opts/compiler-shared-library-darwin-clang.opts \
    $source_dir/lib/dakota/opts/compiler-shared-library-darwin-gnu.opts \
    $source_dir/lib/dakota/opts/compiler-shared-library-extra-darwin-appleclang.opts \
    $source_dir/lib/dakota/opts/compiler-shared-library-extra-darwin-clang.opts \
    $source_dir/lib/dakota/opts/compiler-shared-library-extra-darwin-gnu.opts \
    $source_dir/lib/dakota/opts/compiler-shared-library-extra-linux-clang.opts \
    $source_dir/lib/dakota/opts/compiler-shared-library-extra-linux-gnu.opts \
    $source_dir/lib/dakota/opts/compiler-shared-library-linux-clang.opts \
    $source_dir/lib/dakota/opts/compiler-shared-library-linux-gnu.opts \
    $source_dir/lib/dakota/opts/linker-executable-darwin-appleclang.opts \
    $source_dir/lib/dakota/opts/linker-executable-darwin-clang.opts \
    $source_dir/lib/dakota/opts/linker-executable-darwin-gnu.opts \
    $source_dir/lib/dakota/opts/linker-executable-extra-darwin-appleclang.opts \
    $source_dir/lib/dakota/opts/linker-executable-extra-darwin-clang.opts \
    $source_dir/lib/dakota/opts/linker-executable-extra-darwin-gnu.opts \
    $source_dir/lib/dakota/opts/linker-executable-extra-linux-clang.opts \
    $source_dir/lib/dakota/opts/linker-executable-extra-linux-gnu.opts \
    $source_dir/lib/dakota/opts/linker-executable-linux-clang.opts \
    $source_dir/lib/dakota/opts/linker-executable-linux-gnu.opts \
    $source_dir/lib/dakota/opts/linker-shared-library-darwin-appleclang.opts \
    $source_dir/lib/dakota/opts/linker-shared-library-darwin-clang.opts \
    $source_dir/lib/dakota/opts/linker-shared-library-darwin-gnu.opts \
    $source_dir/lib/dakota/opts/linker-shared-library-extra-darwin-appleclang.opts \
    $source_dir/lib/dakota/opts/linker-shared-library-extra-darwin-clang.opts \
    $source_dir/lib/dakota/opts/linker-shared-library-extra-darwin-gnu.opts \
    $source_dir/lib/dakota/opts/linker-shared-library-extra-linux-clang.opts \
    $source_dir/lib/dakota/opts/linker-shared-library-extra-linux-gnu.opts \
    $source_dir/lib/dakota/opts/linker-shared-library-linux-clang.opts \
    $source_dir/lib/dakota/opts/linker-shared-library-linux-gnu.opts \
  $prefix_dir/lib/dakota/opts/
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  stage-pre-built "$@"
fi
