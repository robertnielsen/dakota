#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
source_dir=$samedir
source "$source_dir/vars.sh"
source "$source_dir/stage-pre-built.sh"

function project-generate() {
  # install everthing first and then use this partial installation to build
  # the remaining two exes and three libs
  stage-pre-built $stage_prefix_dir
  ee $dakota_home/bin/dakota-project --source-dir $source_dir --build-dir $build_dir
  echo "elapsed: $(($SECONDS / 60))m$(($SECONDS % 60))s" 1>&2
} # project-generate()

if test "$0" == "${BASH_SOURCE:-$0}"; then
  project-generate "$@"
fi
