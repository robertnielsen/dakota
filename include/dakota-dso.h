// -*- mode: c++; c-basic-offset: 2; tab-width: 2; indent-tabs-mode: nil -*-

// Copyright (C) 2007 - 2022 Robert Nielsen <robert@dakota.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

# pragma once

# define FUNC      auto
# define typealias using

# if defined _WIN64
  # define so_import ms::dllimport
  # define so_export ms::dllexport
# else
  # include <dlfcn.h> // RTLD_LAZY, RTLD_GLOBAL, RTLD_DEFAULT
  # define so_import
  # define so_export gnu::visibility("default")
# endif

typealias int_t = int;
typealias str_t = const char*;
typealias ptr_t = void*;

extern "C" {
  [[so_export]] FUNC dso_dlclose(ptr_t handle) -> int_t;
  [[so_export]] FUNC dso_dlopen(str_t filename, int_t flags) -> ptr_t;
  [[so_export]] FUNC dso_dlsym(ptr_t handle, str_t sym) -> ptr_t;
  [[so_export]] FUNC dso_sym_name_for_addr(ptr_t addr) -> str_t;
  [[so_export]] FUNC dso_path_for_handle(ptr_t handle) -> str_t;
  [[so_export]] FUNC dso_path_for_lib_name(str_t lib_name) -> str_t;
  [[so_export]] FUNC dso_find_library(str_t lib_name, char* buf, size_t buflen) -> str_t;
  [[so_export]] FUNC dso_header_of_image_containing_addr(ptr_t addr) -> ptr_t;
  [[so_export]] FUNC dso_path_of_image_containing_addr(ptr_t addr) -> str_t;
  [[so_export]] FUNC dso_file_type_of_image_containing_addr(ptr_t addr) -> str_t;
}
