// -*- mode: c++; c-basic-offset: 2; tab-width: 2; indent-tabs-mode: nil -*-

// Copyright (C) 2007 - 2022 Robert Nielsen <robert@dakota.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

# pragma once

# include <dakota.h>
# include <csignal>
# include <cerrno>

# include "dakota-platform.h"
# include "dakota-config.h"

# if defined HAVE_ELF64_EHDR
  # define DKT_READ_ONLY ".dkt_read_only"
# elif defined HAVE_MACH_HEADER_64
  // mach-o section specifier requires a segment and section separated by a comma
  # define DKT_READ_ONLY "__DKT_READ_ONLY,__dkt_read_only"
# else
  # error
# endif

# if ! defined HAVE_GETPROGNAME && defined HAVE_PROGRAM_INVOCATION_SHORT_NAME
  inline FUNC getprogname() -> str_t {
    return program_invocation_short_name;
  }
# endif
