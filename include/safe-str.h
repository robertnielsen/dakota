// -*- mode: c++; c-basic-offset: 2; tab-width: 2; indent-tabs-mode: nil -*-

// Copyright (C) 2007 - 2022 Robert Nielsen <robert@dakota.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

# pragma once

# include <cstdlib>
# include <cstring>

inline FUNC dkt_normalize_compare_result(intmax_t n) -> cmp_t { return (n < 0) ? -1 : (n > 0) ? 1 : 0; }

inline FUNC safe_strcmp(str_t s1, str_t s2) -> int_t {
  int_t value = 0;

  if (s1 == nullptr || s2 == nullptr) {
    if (s1 == nullptr && s2 == nullptr)
      value = 0;
    else if (s1 == nullptr)
      value = -1;
    else if (s2 == nullptr)
      value = 1;
  } else {
    value = dkt_normalize_compare_result(strcmp(s1, s2));
  }
  return value;
}
inline FUNC safe_strncmp(str_t s1, str_t s2, size_t n) -> int_t {
  int_t value = 0;

  if (s1 == nullptr || s2 == nullptr) {
    if (s1 == nullptr && s2 == nullptr)
      value = 0;
    else if (s1 == nullptr)
      value = -1;
    else if (s2 == nullptr)
      value = 1;
  } else {
    value = dkt_normalize_compare_result(strncmp(s1, s2, n));
  }
  return value;
}
inline FUNC safe_strlen(str_t str) -> size_t {
  size_t len;

  if (str == nullptr)
    len = 0;
  else
    len = strlen(str);
  return len;
}
static inline thread_local char_t val2str_buf[31 + (1)];

inline FUNC val2str(_hhi_t i) -> str_t { snprintf(val2str_buf, sizeof(val2str_buf), "%hhi", i); return val2str_buf; }
inline FUNC val2str(_hhu_t i) -> str_t { snprintf(val2str_buf, sizeof(val2str_buf), "%hhu", i); return val2str_buf; }
inline FUNC val2str(_hi_t  i) -> str_t { snprintf(val2str_buf, sizeof(val2str_buf), "%hi",  i); return val2str_buf; }
inline FUNC val2str(_hu_t  i) -> str_t { snprintf(val2str_buf, sizeof(val2str_buf), "%hu",  i); return val2str_buf; }
inline FUNC val2str(_i_t   i) -> str_t { snprintf(val2str_buf, sizeof(val2str_buf), "%i",   i); return val2str_buf; }
inline FUNC val2str(_u_t   i) -> str_t { snprintf(val2str_buf, sizeof(val2str_buf), "%u",   i); return val2str_buf; }
inline FUNC val2str(_li_t  i) -> str_t { snprintf(val2str_buf, sizeof(val2str_buf), "%li",  i); return val2str_buf; }
inline FUNC val2str(_lu_t  i) -> str_t { snprintf(val2str_buf, sizeof(val2str_buf), "%lu",  i); return val2str_buf; }
inline FUNC val2str(_lli_t i) -> str_t { snprintf(val2str_buf, sizeof(val2str_buf), "%lli", i); return val2str_buf; }
inline FUNC val2str(_llu_t i) -> str_t { snprintf(val2str_buf, sizeof(val2str_buf), "%llu", i); return val2str_buf; }
