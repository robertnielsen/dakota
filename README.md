## to build
./configure
./project-generate.sh
./project-build.sh

# shortcut
./rebuild-hard.sh

## to test
test/project-generate.sh
test/project-build.sh
test/project-check.sh

# shortcut
test/rebuild-hard.sh
